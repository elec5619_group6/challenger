package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.userchallenge")
public class UserChallenge implements Serializable {
	
	
	@Id
	@GeneratedValue
	@Column(name="userchallengeId")
	private long userChallengeId;
	
	@Column(name="user_id")
	private long userId;
	
	@Column(name="challenge_id")
	private long challengeId;
	
	@Column(name="time_start")
	private long timeStart;
	
	@Column(name="time_end")
	private long timeEnd;
	
	@Column(name="status")
	private int status;
	
	@Column(name="progress")
	private int progress;
	
	public UserChallenge()
	{}
	
	public UserChallenge(long userChallengeId, long timeEnd,
			int progress, int status) {
		super();
		this.userChallengeId = userChallengeId;
		this.timeEnd = timeEnd;
		this.status = status;
		this.progress = progress;
	}
	
	public UserChallenge(long userId,int status)
	{
		this.userId = userId;
		this.status = status;
	}

	public long getUserChallengeId() {
		return userChallengeId;
	}
	public void setUserChallengeId(long userChallengeId) {
		this.userChallengeId = userChallengeId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getChallengeId() {
		return challengeId;
	}
	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}
	public long getTimeStart() {
		return timeStart;
	}
	
	public Date getStartDay() {
		Calendar cal = Calendar.getInstance();
		
        cal.setTime(new Date(timeStart));
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
	}
	public void setTimeStart(long timeStart) {
		this.timeStart = timeStart;
	}
	public long getTimeEnd() {
		return timeEnd;
	}
	
	public Date getFinishDay() {
		Calendar cal = Calendar.getInstance();
		
        cal.setTime(new Date(timeEnd));
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
	}
	
	public void setTimeEnd(long timeEnd) {
		this.timeEnd = timeEnd;
	}
	
	/**
	 * 1: Challenge finished, 0: unfinished */
	public int isStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

//	public int getLike() {
//		return like;
//	}
//
//	public void setLike(int like) {
//		this.like = like;
//	}
	
	
	//
}
