package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class TagChallengeTest extends BaseConfiguration {

	@Test
	public void persistTagChallenge() {
		
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		Query query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		challenge = (Challenge) query.list().get(0);
		
		Tag tag = new Tag(TAGNAME);
		this.getSession().save(tag);
		query = this.getSession().createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		tag = (Tag) query.list().get(0);
		
		TagChallenge tagChallenge = new TagChallenge(challenge.getChaId(), tag.getId());
		this.getSession().save(tagChallenge);
		query = this.getSession().createQuery("FROM " + TagChallenge.class.getName() + " WHERE tag_id = :tag_id");
		query.setParameter("tag_id", tag.getId());
		if (query.list().isEmpty()) {
			assertNotNull("tag challenge list is empty", null);
			return;
		}
		tagChallenge = (TagChallenge) query.list().get(0);
		assertEquals(challenge.getChaId(), tagChallenge.getChallengeId());
		assertEquals(tag.getId(), tagChallenge.getTagId());
		
		query = this.getSession().createQuery("DELETE FROM " + TagChallenge.class.getName() + " WHERE tag_id = :tag_id");
		query.setParameter("tag_id", tag.getId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + TagChallenge.class.getName() + " WHERE tag_id = :tag_id");
		query.setParameter("tag_id", tag.getId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		query.executeUpdate();
	}
}
