<%@ include file="/WEB-INF/views/head.jsp"%>

<html lang="en">
<head>
<title>Challenge Creation</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.css" />
<script src="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.js"></script>

<style type="text/css">
.bootstrap-tagsinput {
	width: 100%;
}

.label {
	line-height: 2 !important;
}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Challenges</li>
							<li><a href="./challenges">Back to my challenges</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="jumbotron">
					<form action="addchallenge" method="post" class="form-horizontal">

						<div class="form-group">
							<label class="col-sm-2 control-label">Title:</label>
							<div class="col-sm-5">
								<input type="text" name="title" class="form-control" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Description:</label>
							<div class="col-sm-5">
								<input type="text" name="description" class="form-control" required/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Quantity:</label>
							<div class="col-sm-5">
								<input type="number" name="duration" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Rating:</label>
							<div class="col-sm-5">
								<input type="number" name="rating" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Difficulty:</label>
							<div class="col-sm-5">
								<input type="number" name="difficulty" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tags:</label>
							<div class="col-sm-5">
								<input type="text" name="tags" class="col-sm-5" data-role="tagsinput" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Icon Link:</label>
							<div class="col-sm-5">
								<input type="text" name="iconpath" class="col-sm-5" data-role="tagsinput" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-5 col-sm-offset-2">
								<button type="submit" class="btn btn-default">Create</button>
							</div>
						</div>

					</form>
				</div>

			</div>
			<!--/span-->

		</div>


	</div>
	<!-- /.container -->

</body>
</html>