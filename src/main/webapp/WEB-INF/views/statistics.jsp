<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>User Statistics</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
				<div class="jumbotron">
					<div>
						<h3>User Statistics</h3>
						<br />
						<table class="table">
							<tr>
								<td align="right">User ID:</td>
								<td>${statistics.userId}</td>
							</tr>
							<tr>
								<td align="right">Total Challenge:</td>
								<td>${statistics.totalChallenge}</td>
							</tr>
							<tr>
								<td align="right">Last Login Time:</td>
								<td>${statistics.lastLoginTime}</td>
							</tr>
							<tr>
								<td align="right">Total Login Number:</td>
								<td>${statistics.totalLoginNum}</td>
							</tr>
							<tr>
								<td align="right">Completed Challenge Number:</td>
								<td>${statistics.cmpChg}</td>
							</tr>
							<tr>
								<td align="right">Ongoing Challenge Number:</td>
								<td>${statistics.incmpChg}</td>
							</tr>
							<tr>
								<td align="right">Challenge Complete Rate:</td>
								<td>${statistics.completeRate}%</td>
							</tr>
						</table>
						<br />
					</div>
					<a href="../user_home" type="submit" class="btn btn-default">
						<i class="glyphicon glyphicon-circle-arrow-left"></i>Back
					</a>
				</div>

			</div>
			<!--/span-->

		</div>
		<!--/row-->
	</div>
</body>
</html>