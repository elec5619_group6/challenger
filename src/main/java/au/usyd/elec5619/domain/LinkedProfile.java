package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Basic user class
 * @author John
 */
@Entity
@Table(name="challenger.user")
public class LinkedProfile implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="user_id")
	private long userId;
	
	@Column(name="linked_profile_id")
    private String linkedProfileId;

	/* Functions */
	@Override
	public boolean equals(Object obj) {
		return userId == ((LinkedProfile)obj).getUserId();
	}

	/* Constructor */
	public LinkedProfile() {}
	
	public LinkedProfile(long userId, String linkedProfileId) {
		super();
		this.userId = userId;
		this.linkedProfileId = linkedProfileId;
	}
	
	/* Getter & Setter */
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getLinkedProfileId() {
		return linkedProfileId;
	}

	public void setLinkedProfileId(String linkedProfileId) {
		this.linkedProfileId = linkedProfileId;
	}

	
}
