package au.usyd.elec5619.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.ChallengeStatistics;
import au.usyd.elec5619.domain.UserChallenge;

@Repository
@Transactional
public class GraphManagerImpl implements GraphManager {
	
	private static final Logger logger = LoggerFactory.getLogger(GraphManagerImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private List<UserChallenge> userChlgList;
	private LinkedList<ChallengeStatistics> displayList;

	@Override
	public List<UserChallenge> getUserChallengeRecord(long id) {
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM " + UserChallenge.class.getName() + " WHERE user_id = :id ORDER BY time_end");
		query.setParameter("id", id);

		userChlgList = query.list();
		return userChlgList;
	}
	
	public void mergeRecord() {
		displayList.clear();
		for (UserChallenge uch : userChlgList) {
			if (0 == uch.isStatus()) {
				continue;
			}
			boolean newitem = true;
			for (ChallengeStatistics chStat : displayList) {
				if (chStat.isSameFinishDay(new Date(uch.getTimeEnd()))) {
					logger.info("same day " + new Date(uch.getTimeEnd()) + " and " + chStat.getFinishTime());
					chStat.increaseCount();
					newitem = false;
					break;
				}
			}
			if (!newitem)
				continue;
			/* new item in the list */
			ChallengeStatistics chStat = new ChallengeStatistics(uch.getStartDay(), uch.getFinishDay());
			chStat.increaseCount();
			displayList.add(chStat);
		}
		logger.info("Merging records, total: " + displayList.size());
	}

	public GraphManagerImpl() {
		super();
		displayList = new LinkedList<ChallengeStatistics>();
	}

	public List<UserChallenge> getUserChlgList() {
		return userChlgList;
	}

	public LinkedList<ChallengeStatistics> getDisplayList() {
		return displayList;
	}

}
