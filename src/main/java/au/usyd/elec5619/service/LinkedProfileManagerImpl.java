//package au.usyd.elec5619.service;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//import javax.inject.Inject;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import au.usyd.elec5619.dao.LinkedProfileDAO;
//import au.usyd.elec5619.domain.LinkedProfile;
//
//@Service("LinkedProfileManager")
//@Transactional
//public class LinkedProfileManagerImpl implements LinkedProfileManager {
//	
//	@Autowired
//	LinkedProfileDAO LinkedProfileDAO;
//	
//	protected final Log logger = LogFactory.getLog(getClass());
//
//	@Resource(name="profileManager")
//	ProfileManager profileManager;
//	
//	@Override
//	public void addLinkedProfile(LinkedProfile LinkedProfile) {
//		LinkedProfileDAO.addLinkedProfile(LinkedProfile);
//	}
//	
//	@Override
//	public void addLinkedProfile(String email, String password) {
//		LinkedProfileDAO.addLinkedProfile(email, password);
//	}
//	
//	@Override
//	public LinkedProfile getLinkedProfile(long id) {
//		return LinkedProfileDAO.getLinkedProfile(id);
//	}
//
//	@Override
//	public LinkedProfile getLinkedProfile(String email) {
//		System.out.println("in LinkedProfilemanager");
//		return LinkedProfileDAO.getLinkedProfile(email);
//	}
//
//	@Override
//	public void updateLinkedProfile(LinkedProfile LinkedProfile) throws Exception {
//		LinkedProfileDAO.updateLinkedProfile(LinkedProfile);
//	}
//
//	@Override
//	public List<LinkedProfile> getLinkedProfiles() {
//		return LinkedProfileDAO.getLinkedProfiles();
//	}
//
//	@Override
//	public LinkedProfile getLinkedProfile(String email, String password) {
//		LinkedProfile u = getLinkedProfile(email);
//		if (u != null && u.getPassword() != null) {
//			if (u.getPassword().equals(password)) {
//				return u;
//			}
//		}
//		return null;
//	}
//
//	@Override
//	public boolean hasLinkedProfile(String email) {
//		return (getLinkedProfile(email) != null);
//	}
//
//	@Override
//	public LinkedProfile registerLinkedProfile(Registration registration) {
//		
//		if (!hasLinkedProfile(registration.getEmail())) {
//			
//			LinkedProfile LinkedProfile = new LinkedProfile();
//			LinkedProfile.setEmail(registration.getEmail());
//			LinkedProfile.setPassword(registration.getPassword());
//			LinkedProfile.setLinkedProfileName(registration.getLinkedProfileName());
//			LinkedProfile.setFirstName(registration.getFirstName());
//			LinkedProfile.setLastName(registration.getLastName());
//			LinkedProfile.setDob(registration.getDob());
//			LinkedProfile.setGender(registration.isGender());
//			LinkedProfile.setPhone(registration.getPhone());
//			LinkedProfile.setCity(registration.getCity());
//			LinkedProfile.setCountry(registration.getCountry());
//			
//			addLinkedProfile(LinkedProfile);
//			
//			/* Create achievement list */
//			profileManager.createLinkedProfileAchievementList(LinkedProfile);
//			
//			return getLinkedProfile(registration.getEmail());
//			
//		} else {
//			return null;
//		}
//	}
//
//	@Override
//	public void deleteLinkedProfile(String email) throws Exception {
//		LinkedProfileDAO.deleteLinkedProfile(email);
//	}
//
//	@Override
//	public void setLinkedProfileDAO(LinkedProfileDAO LinkedProfileDAO) {
//		this.LinkedProfileDAO = LinkedProfileDAO;
//	}
//
//	@Override
//	public Registration getUpdateObjectFromLinkedProfile(LinkedProfile LinkedProfile) {
//		Registration LinkedProfileUpdate = new Registration();
//		
//		LinkedProfileUpdate.setLinkedProfileId(LinkedProfile.getLinkedProfileId());
//		LinkedProfileUpdate.setEmail(LinkedProfile.getEmail());
//		LinkedProfileUpdate.setPassword(LinkedProfile.getPassword());
//		LinkedProfileUpdate.setConfirmPassword(LinkedProfile.getPassword());
//		LinkedProfileUpdate.setLinkedProfileName(LinkedProfile.getLinkedProfileName());
//		LinkedProfileUpdate.setFirstName(LinkedProfile.getFirstName());
//		LinkedProfileUpdate.setLastName(LinkedProfile.getLastName());
//		LinkedProfileUpdate.setDob(LinkedProfile.getDob());
//		LinkedProfileUpdate.setGender(LinkedProfile.isGender());
//		LinkedProfileUpdate.setPhone(LinkedProfile.getPhone());
//		LinkedProfileUpdate.setCity(LinkedProfile.getCity());
//		LinkedProfileUpdate.setCountry(LinkedProfile.getCountry());
//		
//		return LinkedProfileUpdate;
//		
//	}
//
//	@Override
//	public LinkedProfile updateLinkedProfile(Registration LinkedProfileUpdate) throws Exception {
//		
//		if (hasLinkedProfile(LinkedProfileUpdate.getLinkedProfileId())) {
//			
//			LinkedProfile LinkedProfile = new LinkedProfile();
//			LinkedProfile.setEmail(LinkedProfileUpdate.getEmail());
//			LinkedProfile.setPassword(LinkedProfileUpdate.getPassword());
//			LinkedProfile.setLinkedProfileName(LinkedProfileUpdate.getLinkedProfileName());
//			LinkedProfile.setFirstName(LinkedProfileUpdate.getFirstName());
//			LinkedProfile.setLastName(LinkedProfileUpdate.getLastName());
//			LinkedProfile.setDob(LinkedProfileUpdate.getDob());
//			LinkedProfile.setGender(LinkedProfileUpdate.isGender());
//			LinkedProfile.setPhone(LinkedProfileUpdate.getPhone());
//			LinkedProfile.setCity(LinkedProfileUpdate.getCity());
//			LinkedProfile.setCountry(LinkedProfileUpdate.getCountry());
//			
//			updateLinkedProfile(LinkedProfile);
//			
//			return getLinkedProfile(LinkedProfileUpdate.getEmail());
//			
//		} else {
//			return null;
//		}
//	}
//
//	@Override
//	public boolean hasLinkedProfile(long LinkedProfileId) {
//		return (getLinkedProfile(LinkedProfileId) != null);
//	}
//	
//}
