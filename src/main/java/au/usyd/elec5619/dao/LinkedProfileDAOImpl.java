package au.usyd.elec5619.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.LinkedProfile;
import au.usyd.elec5619.domain.User;

@Repository
public class LinkedProfileDAOImpl implements LinkedProfileDAO {

	@Autowired
	private SessionFactory sessionFactory;

	protected final Log logger = LogFactory.getLog(getClass());

	@Override
	@Transactional
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	@Transactional
	public LinkedProfile getLinkedProfile(long userId, String linkedProfileId) {
		List<LinkedProfile> userList = new ArrayList<LinkedProfile>();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM LinkedProfile u WHERE u.user_id = :userId AND u.linked_profile_id = :linkedProfileId");
        query.setParameter("userId", userId);
        query.setParameter("linkedProfileId", linkedProfileId);
        logger.info("executing query " + query.toString());
        userList = query.list();
        if (userList.size() > 0)
            return userList.get(0);
        else
            return null; 

	}

	@Override
	@Transactional
	public LinkedProfile getLinkedProfile(String linkedProfileId) {
		List<LinkedProfile> userList = new ArrayList<LinkedProfile>();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM LinkedProfile u WHERE u.linked_profile_id = :linkedProfileId");
        query.setParameter("linkedProfileId", linkedProfileId);
        logger.info("executing query " + query.toString());
        userList = query.list();
        if (userList.size() > 0)
            return userList.get(0);
        else
            return null;
	}
	
	@Override
	@Transactional
	public List<LinkedProfile> getlinkedProfiles(long userId) {
		return this.sessionFactory.getCurrentSession().createQuery("FROM LinkedProfile").list();
	}

	@Override
	@Transactional
	public void addLinkedProfile(long userId, String linkedProfileId) {
		LinkedProfile linkedProfile = new LinkedProfile();
		linkedProfile.setLinkedProfileId(linkedProfileId);
		linkedProfile.setUserId(userId);

		this.sessionFactory.getCurrentSession().save(linkedProfile);
	}

	@Override
	@Transactional
	public void addLinkedProfile(LinkedProfile linkedProfile) {
		this.sessionFactory.getCurrentSession().save(linkedProfile);
	}

	@Override
	@Transactional
	public void deleteLinkedProfile(String linkedProfileId)
			throws Exception {
		LinkedProfile linkedProfile = getLinkedProfile(linkedProfileId);
		if (linkedProfile != null) {
			this.sessionFactory.getCurrentSession().delete(linkedProfile);
		} else {
			throw new Exception("User: " + linkedProfile + " does not exist");
		}
	}

	@Override
	@Transactional
	public void deleteLinkedProfile(LinkedProfile linkedProfile)
			throws Exception {
		if (linkedProfile != null && !linkedProfile.getLinkedProfileId().equals(null)) { 
			deleteLinkedProfile(linkedProfile.getLinkedProfileId());
		} else {
			throw new Exception("User: " + linkedProfile + " does not exist");
		}
	}


}
