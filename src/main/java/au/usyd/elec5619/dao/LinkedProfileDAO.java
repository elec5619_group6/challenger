package au.usyd.elec5619.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.LinkedProfile;
import au.usyd.elec5619.domain.User;

public interface LinkedProfileDAO {

	public LinkedProfile getLinkedProfile(long userId, String linkedProfileId);
		
	public LinkedProfile getLinkedProfile(String linkedProfileId);
	
	public List<LinkedProfile> getlinkedProfiles(long userId);

	public void addLinkedProfile(long userId, String linkedProfileId);
	
	public void addLinkedProfile(LinkedProfile linkedProfile);
	
	public void deleteLinkedProfile(String linkedProfileId) throws Exception;

	public void deleteLinkedProfile(LinkedProfile linkedProfile) throws Exception;
	
	public void setSessionFactory(SessionFactory sessionFactory);

}
