package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Relationship;



@Repository
public class FriendManager {

	private ArrayList<Long> friendList;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public FriendManager()
	{
		friendList = new ArrayList<Long>();
	}
	
	@Transactional
	public ArrayList<Long> getFriends(long id)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM Relationship where user_id =:id");
		query.setLong("id", id);
		 List<Relationship> relationList = (List)query.list();
		 ArrayList<Long> users = new ArrayList<Long>();
		 for(Relationship r : relationList)
		 {
			 users.add(r.getFriendId());
		 }
		//
		 return users;
	}
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public void addFriend(Relationship friend)
	{
		this.sessionFactory.getCurrentSession().save(friend);
	}
	
	@Transactional
	public void deleteFriend(long id,long friendId) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM Relationship where user_id =:id and friend_id =:fid");
		query.setLong("id", id);
		query.setLong("fid", friendId);
		
		Relationship friend = (Relationship)query.uniqueResult();
		currentSession.delete(friend);
	}
}
