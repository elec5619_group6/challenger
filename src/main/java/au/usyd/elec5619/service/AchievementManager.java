package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Achievement;
import au.usyd.elec5619.domain.User;

/**
 * AchievementManager
 * @author John
 *
 */
@Repository
public class AchievementManager {

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<Achievement> achievementList;
	
	@Transactional
	public void findAll() {
		Session session = sessionFactory.getCurrentSession();
	    List<Achievement> achievementList = session.createQuery("FROM Achievement").list();
	    for (Achievement achievement : achievementList) {
			System.out.println("Achievement " + achievement.getAchId());
			this.achievementList.add(achievement);
		}
	}
}
