package au.usyd.elec5619.domain;

import java.io.Serializable;

public class FeedbackForDisplay implements Serializable 
{
	
	private long feedbackId;
	private String challengeName;
	private String userName;
	private String review;
	private String comment;
	private String videoLink;
	private int interestScore;
	private int difficultyScore;
	public FeedbackForDisplay(Feedback feedback,Challenge challenge,User user)
	{
		challengeName = challenge.getTitle();
		//user = challenge.getCreator();
		review = feedback.getDescription();
		feedbackId = feedback.getFdId();
		interestScore = feedback.getInterest_score();
		difficultyScore = feedback.getDifficulty_score();
		videoLink = feedback.getVideoLink();
		userName = user.getUserName();
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public long getFeedbackId() {
		return feedbackId;
	}
	public void setFeedbackId(long feedbackId) {
		this.feedbackId = feedbackId;
	}
	public String getChallengeName() {
		return challengeName;
	}
	public void setChallengeName(String challengeName) {
		this.challengeName = challengeName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getInterestScore() {
		return interestScore;
	}
	public void setInterestScore(int interestScore) {
		this.interestScore = interestScore;
	}
	public int getDifficultyScore() {
		return difficultyScore;
	}
	public void setDifficultyScore(int difficultyScore) {
		this.difficultyScore = difficultyScore;
	}
	
}