package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.UserDAO;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.Registration;

@Service("userManager")
@Transactional
public interface UserManager {
	
	public User getUser(long id);
	
	public User getUser(String email);
	
	public User getUser(String email, String password);

	public void addUser(User user);
	
	public void addUser(String email, String password);
	
	public void updateUser(User user) throws Exception;

	public List<User> getUsers();

	public boolean hasUser(long userId);
	
	public boolean hasUser(String email);

	public User registerUser(Registration registration);

	public void deleteUser(String email) throws Exception;

	public void setUserDAO(UserDAO userDAO);

	public Registration getUpdateObjectFromUser(User self);

	public User updateUser(Registration userUpdate) throws Exception;

	void setProfileManager(ProfileManager profileManager);

}
