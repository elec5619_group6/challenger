package au.usyd.elec5619.service;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.Relationship;
import au.usyd.elec5619.domain.User;
import junit.framework.TestCase;

public class FriendManagerTest extends BaseConfiguration{

	private FriendManager friendManager;
	private ArrayList<Long> friendList;
	private Relationship rela1;
	private Relationship rela2;
	private int origSize;
	
	
	private static long USER_ID = 1;
	private static long FRIEND1_ID = 2;
	private static long FRIEND2_ID = 3;
	
	
	
	
	private static int FRIEND_COUNT = 2;
	
	
	@Before
	public void setUp() throws Exception
	{
		friendManager = new FriendManager();
		friendManager.setSessionFactory(this.getSessionFactory());
		friendList = friendManager.getFriends(USER_ID);
		rela1 = new Relationship(USER_ID,FRIEND1_ID);
		rela2 = new Relationship(USER_ID,FRIEND2_ID);
		origSize = friendList.size();
	}
	
	@Test
	public void testFriendManager()
	{
		friendManager.addFriend(rela1);
		friendManager.addFriend(rela2);
		assertEquals(origSize+FRIEND_COUNT,friendManager.getFriends(USER_ID).size());
		
		friendList = friendManager.getFriends(USER_ID);
		assertEquals(FRIEND_COUNT,friendList.size()-origSize);
		
		friendManager.deleteFriend(USER_ID, FRIEND1_ID);
		friendManager.deleteFriend(USER_ID, FRIEND2_ID);
		assertEquals(friendManager.getFriends(USER_ID).size(),origSize);
	}
	
}
