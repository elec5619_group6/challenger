package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.tag_user")
public class TagUser {
	
	@Id
	@GeneratedValue
	@Column(name="idtag_user")
	private long id;
	
	@Column(name="user_id")
	private long userId;
	
	@Column(name="tag_id")
	private long tagId;
	
	@Column(name="challenge_completed")
	private int completeChallenge;

	public TagUser() {
		super();
	}

	public TagUser(long userId, long tagId) {
		super();
		this.userId = userId;
		this.tagId = tagId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(long tagId) {
		this.tagId = tagId;
	}

	public int getCompleteChallenge() {
		return completeChallenge;
	}

	public void setCompleteChallenge(int completeChallenge) {
		this.completeChallenge = completeChallenge;
	}
	
	public void increaseCompleteChallenge() {
		this.completeChallenge++;
	}
}
