<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
<head>
<title>User Profile</title>
<link
	href="${pageContext.request.contextPath}/resources/css/dashboard.css"
	rel="stylesheet">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.css" />
<script
	src="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.js"></script>

<style type="text/css">
.bootstrap-tagsinput {
	width: 100%;
}

.error {
	color: red;
}

</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div
				class="col-xs-5 col-xs-offset-7 col-sm-3 col-sm-offset-9 col-md-2 sidebar col-md-offset-10">
				<ul class="nav nav-sidebar">
					<div class="list-group">
						<div class="media">
							<a class="pull-left" href="#"> <img class="media-object"
								src="" alt="">
							</a>
							<div class="media-body">
								<h4 class="media-heading">
									<span class="glyphicon glyphicon-user"></span>
									<c:out value='${user.userName }' />
								</h4>
								<a href="${pageContext.request.contextPath}/profile">View Profile</a>
							</div>
						</div>
					</div>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="list-group">
							<a href="<c:out value='${pageContext.request.contextPath}/user_home'/>"
								class="list-group-item"> <span
								class="glyphicon glyphicon-home"> </span> Activity
							</a> <a href="<c:out value="${pageContext.request.contextPath}/challenges"/>"
								class="list-group-item"> <span
								class="glyphicon glyphicon-send"> </span> Challenges
							</a><a href="<c:out value="${pageContext.request.contextPath}/statistics"/>"
								class="list-group-item"> <span
								class="glyphicon glyphicon-list"></span> Statistics
							</a> <a href="<c:out value="${pageContext.request.contextPath}/barchart"/>"
								class="list-group-item"> <span
								class="glyphicon glyphicon-signal"></span> Graphs
							</a> <a href="<c:out value="${pageContext.request.contextPath}/achievements"/>"
								class="list-group-item"> <span
								class="glyphicon glyphicon-thumbs-up"></span> Achievements
							</a>
						</div></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Friends</h3>
							</div>
							<ul class="list-group">
								<c:forEach items="${friends}" var="friend">
								<li class="list-group-item">
									<a href="${friend.userId}" class="list-group-item"> 
										<span>
											<p class="list-group-item-heading">${friend.userName}</p>
										</span>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div></li>
				</ul>
			</div>
			<div class="col-xs-7 col-sm-9 col-md-10 main">
				<div class="container">
					<div class="col-xs-7 col-sm-9 col-md-10">
						<div class="panel panel-primary">
							<div class="panel-heading">

								<h1 class="panel-title">Edit Profile</h1>

							</div>

							<form:form action="edit" commandName="userUpdate"
								method="post" class="form-horizontal">
								<form:hidden path="userId" name="userId"/>
								<div class="form-group">
									<label class="col-sm-3 control-label">Email:</label>
									<div class="col-sm-6">
										<form:input type="email" path="email" class="form-control"
											required="true" autofocus="true" />
										<form:errors path="email" cssClass="error" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Password:</label>
									<div class="col-sm-6">
										<form:input type="password" path="password"
											class="form-control" required="true" />
										<form:errors path="password" cssClass="error" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Confirm Password:</label>
									<div class="col-sm-6">
										<form:input type="password" path="confirmPassword"
											class="form-control" required="true" />
										<form:errors path="password" cssClass="error" />
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">First Name:</label>
									<div class="col-sm-6">
										<form:input type="text" path="firstName" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Last Name:</label>
									<div class="col-sm-6">
										<form:input type="text" path="lastName" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">User Name:</label>
									<div class="col-sm-6">
										<form:input type="text" path="userName" class="form-control"
											required="true" />
										<form:errors path="userName" cssClass="error" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Phone:</label>
									<div class="col-sm-6">
										<form:input type="text" path="phone" class="form-control" />
										<form:errors path="phone" cssClass="error" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Gender:</label>
									<div class="col-sm-6">
										<label> <form:radiobutton path="gender" value="true"
												checked="true" /> Male
										</label> <label> <form:radiobutton path="gender" value="false" />
											Female
										</label>
										<form:errors path="gender" cssClass="error" />
									</div>
								</div>
<!-- 								<div class="form-group"> -->
<!-- 									<label class="col-sm-3 control-label">Date of Birth:</label> -->

<!-- 									<div class='col-sm-6'> -->
<!-- 										<div class='input-group date' id='datetimepicker5'> -->
<%-- 											<form:input path="${userRegister.dob}" type='text' --%>
<%-- 												class="form-control" data-date-format="YYYY/MM/DD" /> --%>
<!-- 											<span class="input-group-addon"> <span -->
<!-- 												class="glyphicon glyphicon-calendar"></span> -->
<!-- 											</span> -->
<!-- 										</div> -->
<!-- 										<script type="text/javascript"> -->
<!-- // 											$(function() { -->
<!-- // 												$('#sandbox-container input') -->
<!-- // 														.datepicker({}); -->
<!-- // 											}); -->
<!-- 										</script> -->
<!-- 									</div> -->
									<!-- 				<div class="col-sm-9"> -->
									<!-- 					<select name="year" id="year"> -->
									<!-- 					</select> <select name="month" id="month"> -->
									<!-- 						<option value=01>JAN</option> -->
									<!-- 						<option value=02>FEB</option> -->
									<!-- 						<option value=03>MAR</option> -->
									<!-- 						<option value=04>APR</option> -->
									<!-- 						<option value=05>MAY</option> -->
									<!-- 						<option value=06>JUN</option> -->
									<!-- 						<option value=07>JUL</option> -->
									<!-- 						<option value=08>AUG</option> -->
									<!-- 						<option value=09>SEP</option> -->
									<!-- 						<option value=10>OCT</option> -->
									<!-- 						<option value=11>NOV</option> -->
									<!-- 						<option value=12>DEC</option> -->
									<!-- 					</select> <select name="day" id="day"> -->
									<!-- 					</select> -->
									<!-- 				</div> -->
<!-- 								</div> -->
								<div class="form-group">
									<label class="col-sm-3 control-label">Country:</label>
									<div class="col-sm-6">
										<form:input type="text" path="country" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">City:</label>
									<div class="col-sm-6">
										<form:input type="text" path="city" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
							</form:form>
							
						</div>
					</div>
				</div>
				<!-- /.container -->
			</div>
		</div>
	</div>
	<div class="fb-like" data-share="true" data-width="450"
		data-show-faces="true"></div>
	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../assets/js/docs.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>