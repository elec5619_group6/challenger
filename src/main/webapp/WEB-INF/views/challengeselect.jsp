<%@ include file="/WEB-INF/views/head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Challenge selection</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Challenges</li>
							<li><a href="./challenges">Back to my challenges</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h4>Challenges</h4>
					</div>

					<div class="panel-body">
						<c:if test="${not empty chgtitle}">
							<div class="alert alert-info alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>${chgtitle}</strong>
							</div>
						</c:if>
						<c:if test="${not empty msg}">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>${msg}</strong>
							</div>
						</c:if>
						<div class="list-group">
							<c:forEach items="${challengelist}" var="display">

								<a href="./challengedetail/${display.chaId}"
									class="list-group-item clearfix"> <span>
									
									<table width="100%" cellpadding="0" bordercolorlight="#999999" bordercolordark="#FFFFFF"
cellspacing="0" align="center"><tr><td width="30%">
										<img src="${display.icon}" width="128" height="128"></td>

										<td>
										<h4 class="list-group-item-heading"  align="lift">${display.title}</h4>
										<p class="list-group-item-text" align="lift">${display.description}</p>										
								</span> <span class="pull-right" > <input type="button" align="right"
										class="btn btn-md btn-primary" value="Detail" />
								</span></td></tr></table>
								</a>

							</c:forEach>
						</div>

						<!-- <div class="panel-footer">Not used.</div> -->
					</div>
				</div>

			</div>
			<!--/row-->
		</div>
	</div>

</body>
</html>