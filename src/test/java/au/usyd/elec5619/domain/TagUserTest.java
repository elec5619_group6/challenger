package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class TagUserTest extends BaseConfiguration {

	@Test
	public void persistTagUser() {
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Tag tag = new Tag(TAGNAME);
		this.getSession().save(tag);
		query = this.getSession().createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		tag = (Tag) query.list().get(0);
		
		TagUser tagUser = new TagUser(user.getUserId(), tag.getId());
		this.getSession().save(tagUser);
		query = this.getSession().createQuery("FROM " + TagUser.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		if (query.list().isEmpty()) {
			assertNotNull("tag user list is empty", null);
			return;
		}
		tagUser = (TagUser) query.list().get(0);
		assertEquals(user.getUserId(), tagUser.getUserId());
		assertEquals(tag.getId(), tagUser.getTagId());
		
		query = this.getSession().createQuery("DELETE FROM " + TagUser.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + TagUser.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		query.executeUpdate();
	}
}
