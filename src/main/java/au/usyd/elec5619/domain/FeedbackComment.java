package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.feedback_comment")
public class FeedbackComment implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="Feedback_Comment_id")
	private long FCId;
	
	
	@Column(name="Comment")
    private String comment;
	
	@Column(name="User_id")
    private long user_id;
	
	@Column(name="Feedback_id")
	private long feedback_id;
	
	@Column(name="datetime")
	private Date date;
	
	public FeedbackComment(long userId,long fbId,String com)
	{
		this.user_id = userId;
		this.setComment(com);
		this.setFeedback_id(fbId);
	}
	
	public FeedbackComment()
	{}
	public long getFCId() {
		return FCId;
	}

	public void setFCId(long fCId) {
		FCId = fCId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getFeedback_id() {
		return feedback_id;
	}

	public void setFeedback_id(long feedback_id) {
		this.feedback_id = feedback_id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
