package au.usyd.elec5619.domain;


import junit.framework.TestCase;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.domain.Feedback;


public class BasicFeedbackTest extends TestCase {
	
	private Feedback feedback;

	protected void setUp()
	{
		feedback=new Feedback();
	}
	public void testSetAndGetDescription()
	{
		String testDescription = "aDescription";
		assertNull(feedback.getDescription());
		feedback.setDescription("aDescription");
		assertEquals(testDescription,feedback.getDescription());
	}
	public void testSetAndGetInterestScore()
	{
		int testInterestScore = 5;
		assertEquals(0,0);
		feedback.setInterest_score(5);
		assertEquals(testInterestScore,feedback.getInterest_score());
	}
	public void testSetAndGetDifficultyScore()
	{
		int testInterestScore = 5;
		assertEquals(0,0);
		feedback.setDifficulty_score(5);
		assertEquals(testInterestScore,feedback.getDifficulty_score());
	}
	public void testSetAndGetFeedbackId()
	{
		long testFeedbackId = 12345;
		assertEquals(0,0);
		feedback.setFdId(12345);
		assertEquals(testFeedbackId,feedback.getFdId());
	}
	public void testSetAndGetChallengeId()
	{
		long testChallengeId = 12345;
		assertEquals(0,0);
		feedback.setChallenge_id(12345);
		assertEquals(testChallengeId,feedback.getChallenge_id());
	}
	public void testSetAndGetUserId()
	{
		long testUserId = 12345;
		assertEquals(0,0);
		feedback.setUser_id(12345);
		assertEquals(testUserId,feedback.getUser_id());
	}
}
