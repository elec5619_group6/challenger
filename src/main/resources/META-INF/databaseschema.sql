CREATE DATABASE  IF NOT EXISTS `challenger` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `challenger`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: challenger
-- ------------------------------------------------------
-- Server version	5.6.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achievement`
--

DROP TABLE IF EXISTS `achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement` (
  `achievement_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`achievement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Achievement template for all users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement`
--

LOCK TABLES `achievement` WRITE;
/*!40000 ALTER TABLE `achievement` DISABLE KEYS */;
INSERT INTO `achievement` VALUES (43,'First one done!','Complete a challenge',1,0),(44,'3 in a row!','Complete 3 challenges',3,0),(45,'Want more!','Complete 5 challenges',5,0),(46,'Can\'t stop!','Complete 10 challenges',10,0),(47,'First time','First time login',1,1),(48,'Coming back','Login 3 times',3,1),(49,'All week','Login 7 times',7,1),(50,'Heavy user','Login 14 times',14,1),(51,'Decide on my own','Create a challenge',1,2),(52,'Challenge master','Create 5 challenges',5,2);
/*!40000 ALTER TABLE `achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `achievement_attempt`
--

DROP TABLE IF EXISTS `achievement_attempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_attempt` (
  `attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `achievement_id` int(11) NOT NULL,
  `completed_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` int(11) DEFAULT '0',
  `progress` int(11) DEFAULT '0',
  `percentage` double DEFAULT '0',
  PRIMARY KEY (`attempt_id`),
  UNIQUE KEY `unique_user_ach` (`user_id`,`achievement_id`) COMMENT 'One user cannot have two same achievements. ',
  KEY `user_id_idx` (`user_id`),
  KEY `achievement_id_idx` (`achievement_id`),
  CONSTRAINT `achievement_id` FOREIGN KEY (`achievement_id`) REFERENCES `achievement` (`achievement_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='Achievement log for each achievement owned by each user.  ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement_attempt`
--

LOCK TABLES `achievement_attempt` WRITE;
/*!40000 ALTER TABLE `achievement_attempt` DISABLE KEYS */;
INSERT INTO `achievement_attempt` VALUES (52,1,1,'2014-09-21 23:03:54',0,1,1),(53,1,2,'2014-09-21 23:03:54',0,3,0.4286);
/*!40000 ALTER TABLE `achievement_attempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `challenge`
--

DROP TABLE IF EXISTS `challenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `challenge` (
  `challenge_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `post_time` datetime DEFAULT NULL,
  `rating` double DEFAULT '0',
  `difficulty` double DEFAULT '0',
  `picture_id` int(11) DEFAULT '1',
  `video_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`challenge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `challenge`
--

LOCK TABLES `challenge` WRITE;
/*!40000 ALTER TABLE `challenge` DISABLE KEYS */;
INSERT INTO `challenge` VALUES (1,'Run half hour','Run half hour','wang',2,'2014-10-19 00:00:00',2,2,1,null);
/*!40000 ALTER TABLE `challenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `challenge_attempt`
--

DROP TABLE IF EXISTS `challenge_attempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `challenge_attempt` (
  `attempt_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `challenge_id` int(11) DEFAULT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `challenge_attempt`
--

LOCK TABLES `challenge_attempt` WRITE;
/*!40000 ALTER TABLE `challenge_attempt` DISABLE KEYS */;
/*!40000 ALTER TABLE `challenge_attempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `userchallenge_id` int(11) NOT NULL,
  `commenter_id` int(11) NOT NULL,
  `content` varchar(300) NOT NULL,
  `comment_time` bigint(11) NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,2,3,'test',12334),(2,2,1,'2',1411626622556),(3,3,1,'3',1411626625436),(4,2,1,'5',1411626720056),(5,2,1,'6',1411626722326),(6,3,1,'e',1411626725774),(7,3,1,'adsfasdf',1411626730100),(8,1,1,'43545',1411626791095);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(500) DEFAULT NULL,
  `interest_score` int(11) DEFAULT NULL,
  `difficulty_score` int(11) DEFAULT NULL,
  `photo_path` varchar(50) DEFAULT NULL,
  `video_link` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `challenge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `user_FK` (`user_id`),
  KEY `challenge_FK` (`challenge_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`challenge_id`) REFERENCES `challenge` (`challenge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_comment`
--

DROP TABLE IF EXISTS `feedback_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback_comment` (
  `feedback_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feedback_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`feedback_comment_id`),
  KEY `user_FK` (`user_id`),
  KEY `feedback_FK` (`feedback_id`),
  CONSTRAINT `feedback_comment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `feedback_comment_ibfk_2` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_comment`
--

LOCK TABLES `feedback_comment` WRITE;
/*!40000 ALTER TABLE `feedback_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likechallenge`
--

DROP TABLE IF EXISTS `likechallenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likechallenge` (
  `like_id` int(11) NOT NULL AUTO_INCREMENT,
  `userchallenge_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`like_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likechallenge`
--

LOCK TABLES `likechallenge` WRITE;
/*!40000 ALTER TABLE `likechallenge` DISABLE KEYS */;
/*!40000 ALTER TABLE `likechallenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship`
--

DROP TABLE IF EXISTS `relationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship` (
  `relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`relationship_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship`
--

LOCK TABLES `relationship` WRITE;
/*!40000 ALTER TABLE `relationship` DISABLE KEYS */;
INSERT INTO `relationship` VALUES (9,1,3),(11,1,4),(12,1,2);
/*!40000 ALTER TABLE `relationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(45) NOT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'Building website'),(2,'22'),(3,'222');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_challenge`
--

DROP TABLE IF EXISTS `tag_challenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_challenge` (
  `idtag_challenge` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  PRIMARY KEY (`idtag_challenge`),
  KEY `tag_id_fk_chg_idx` (`tag_id`),
  KEY `chg_id_fk_chg_idx` (`challenge_id`),
  CONSTRAINT `chg_id_fk_chg` FOREIGN KEY (`challenge_id`) REFERENCES `challenge` (`challenge_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tag_id_fk_chg` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tags for challenges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_challenge`
--

LOCK TABLES `tag_challenge` WRITE;
/*!40000 ALTER TABLE `tag_challenge` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_challenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_user`
--

DROP TABLE IF EXISTS `tag_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_user` (
  `idtag_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `challenge_completed` int(11) DEFAULT '0',
  PRIMARY KEY (`idtag_user`),
  KEY `user_id_idx` (`user_id`),
  KEY `tag_id_idx` (`tag_id`),
  CONSTRAINT `tag_id_fk_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id_fk_tag` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='tags user has';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_user`
--

LOCK TABLES `tag_user` WRITE;
/*!40000 ALTER TABLE `tag_user` DISABLE KEYS */;
INSERT INTO `tag_user` VALUES (3,23,1,0),(4,23,3,0);
/*!40000 ALTER TABLE `tag_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL COMMENT '0: female',
  `date_of_birth` datetime DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'123@123.com','123456','John','Yi','Lu','12345678',1,'1990-04-29 00:00:00','Australia','Sydney'),(2,'wangwei@gmail.com','wang','wang','wang','wei','045199',1,'2014-09-17 00:00:00','china','hefei'),(3,'swin@swin.com','oooo','kevin','kevin','kevin','987123',1,'2014-09-17 00:00:00','china','lanzhou'),(4,'louis@163.com','12345','Louis','Louis','Louis','12345',1,'2014-09-22 00:00:00','china','nanjing');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_statistics`
--

DROP TABLE IF EXISTS `user_statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_statistics` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `completed_challenges` int(11) DEFAULT '0',
  `incompleted_challenges` int(11) DEFAULT '0',
  `total_challenge` int(11) DEFAULT '0',
  `complete_rate` double DEFAULT '0',
  `last_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `total_login_num` int(11) DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='All statistics for user activities. ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_statistics`
--

LOCK TABLES `user_statistics` WRITE;
/*!40000 ALTER TABLE `user_statistics` DISABLE KEYS */;

/*!40000 ALTER TABLE `user_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userchallenge`
--

DROP TABLE IF EXISTS `userchallenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userchallenge` (
  `userchallengeId` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `time_start` bigint(20) NOT NULL,
  `time_end` bigint(20) DEFAULT NULL,
  `progress` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT '1:complete',
  PRIMARY KEY (`userchallengeId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userchallenge`
--

LOCK TABLES `userchallenge` WRITE;
/*!40000 ALTER TABLE `userchallenge` DISABLE KEYS */;
INSERT INTO `userchallenge` VALUES (1,2,1,2147483647,2147483647,0,1),(2,3,1,2147483647,2147483647,0,1),(3,4,1,2147483647,2147483647,0,1),(4,1,1,1411661391526,1413338102162,0,1),(5,1,1,1411661391526,1413338102162,0,1),(6,1,1,2147483647,1413448112162,0,1),(7,1,1,1411661391526,1413558112162,0,0),(23,23,1,1413789091012,1413894072711,1,0),(24,23,1,1413847899928,0,0,0),(25,23,1,1413848186413,0,0,0),(26,23,1,1413848266613,0,0,0);
/*!40000 ALTER TABLE `userchallenge` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-29 13:33:40
