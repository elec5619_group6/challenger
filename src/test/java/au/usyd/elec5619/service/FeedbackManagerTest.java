package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;
import au.usyd.elec5619.domain.Like;

public class FeedbackManagerTest extends BaseConfiguration {
	
	
	private FeedbackManager feedbackManager;
	private String FEEDBACK_COMMENT = "test";
	private int INTEREST_SCORE = 3;
	private int DIFFCUITY_SCORE = 4;
	private int USER_ID = 1;
	private int CHALLENGE_ID = 1;
	private List<Feedback> list;
	@Test
	public void setUp()
	{
		feedbackManager = new FeedbackManager();
		feedbackManager.setSessionFactory(this.getSessionFactory());
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		challenge = (Challenge)query.list().get(0);
		Feedback feedback = new Feedback(CHG_TITLE,INTEREST_SCORE,DIFFCUITY_SCORE,user.getUserId(),challenge.getChaId());
		this.getSession().save(feedback);
		query = this.getSession().createQuery("FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", CHG_TITLE);
		feedback = (Feedback)query.list().get(0);
		
		
		/**
		 * transaction test
		 */
		assertEquals(feedback.getDescription(),CHG_TITLE);
		assertEquals(feedback.getChallenge_id(),challenge.getChaId());
		assertEquals(feedback.getUser_id(),user.getUserId());
		assertEquals(feedback.getInterest_score(),INTEREST_SCORE);
		assertEquals(feedback.getDifficulty_score(),DIFFCUITY_SCORE);
		
		/**
		 * Test GetRelatedFeedback function
		 */
		list = feedbackManager.getRelatedFeedback(challenge.getChaId());
		assertEquals(list.get(0).getDescription(),feedback.getDescription());
		/**
		 * Test FindAll function
		 */
		System.out.println(feedbackManager.findAll().size()+" row found ~~~~~~~~~~~~~~~~~~~~~~");
		
		query = this.getSession().createQuery("DELETE FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment",CHG_TITLE);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();

	}
	

	
}
