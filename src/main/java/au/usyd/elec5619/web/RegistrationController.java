package au.usyd.elec5619.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.Login;
import au.usyd.elec5619.service.ProfileManager;
import au.usyd.elec5619.service.Registration;
import au.usyd.elec5619.service.TagService;
import au.usyd.elec5619.service.UserManager;

@Controller
@RequestMapping(value="/register")
public class RegistrationController {

	protected final Log logger = LogFactory.getLog(getClass());
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@Resource(name="tagService")
	TagService tagService;
	
	@Resource(name="profileManager")
	ProfileManager profileManager;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(
	            dateFormat, false));
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String showRegisterForm(Model model, HttpSession session, HttpServletRequest httpServletRequest) {
		User currentUser = (User) session.getAttribute("user");
		if (null != currentUser)
			return "redirect:/home";
		Registration newUser = new Registration();
		newUser.setGender(true);
		model.addAttribute("userRegister", newUser);
		model.addAttribute("date", Calendar.getInstance(httpServletRequest.getLocale()));
		return "register";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(@Valid @ModelAttribute("userRegister") Registration registration, 
			BindingResult bindingResult, 
			HttpSession session,
			HttpServletRequest httpServletRequest, 
			Model uiModel) {
		User currentUser = (User) session.getAttribute("user");
		if (null != currentUser)
			return "redirect:/home";
		
		if (!bindingResult.hasFieldErrors("email") && userManager.getUser(registration.getEmail()) != null) {
			bindingResult.rejectValue("email", "EmailNotExist.register.email");
		}
		
		if (bindingResult.hasErrors()) {
			return "register";
		}
		
		logger.info("Registering user " + registration.getEmail() + " with password " + registration.getPassword());

		currentUser = userManager.registerUser(registration);
		
		// Moved to UserManagerImpl
//		/* Create achievement list */
//		currentUser = userManager.getUser(currentUser.getEmail());
//		profileManager.createUserAchievementList(currentUser);
//		
		// Moved to Profile Edit
		/* create tags */
//		String tagString = httpServletRequest.getParameter("interests");
//		while (true) {
//			int pos = tagString.indexOf(',');
//			if (pos < 0) {
//				tagService.addTagForUser(tagString, currentUser.getUserId());
//				break;
//			}
//			tagService.addTagForUser(tagString.substring(0, pos), currentUser.getUserId());
//			tagString = tagString.substring(pos + 1);
//		}
//		
		// Not needed
//		uiModel.addAttribute("user", currentUser);
		
		return "redirect:/login";
		
		
	}
	

}
