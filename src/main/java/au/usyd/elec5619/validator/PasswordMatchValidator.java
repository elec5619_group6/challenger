package au.usyd.elec5619.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import au.usyd.elec5619.service.Registration;

public final class PasswordMatchValidator implements ConstraintValidator<PasswordMatch, Registration>
{
	private String firstFieldName;
	private String secondFieldName;
	private String errorMessage;
	
	public void initialize(final PasswordMatch constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
		errorMessage = constraintAnnotation.message(); 
		//System.out.println("firstFieldName = "+firstFieldName+"   secondFieldName = "+secondFieldName);
	}

	public boolean isValid(final Registration value, final ConstraintValidatorContext cvc) {
		final String first = value.getPassword();
		final String second = value.getConfirmPassword();
		if (first.equals(second)) {
			return true;
		} else {
			cvc.disableDefaultConstraintViolation();
	        //In the initialiaze method you get the errorMessage: constraintAnnotation.message();
	        cvc.buildConstraintViolationWithTemplate(errorMessage).addNode(firstFieldName).addConstraintViolation();
			return false;
		}
	}
}