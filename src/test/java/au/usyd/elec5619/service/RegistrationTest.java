package au.usyd.elec5619.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.dao.UserDAO;
import au.usyd.elec5619.dao.UserDAOImpl;
import au.usyd.elec5619.domain.User;

public class RegistrationTest extends BaseConfiguration {

	private Registration register;
	
	private User user;
	private UserDAO userDAO;
	private UserManager userManager;
	private ProfileManager profileManager;
	
	private static final String TEST_EMAIL = "mary.email@test.com";
	private static final String TEST_PASSWORD = "pass123";
	private static final String TEST_USERNAME = "whatACoolUserName";
	private static final String TEST_FIRSTNAME = "Mary";
	private static final String TEST_PASSWORD_1 = "pass2";
	private static final String TEST_USERNAME_1 = "anotherCoolUserName";

	private static final long TEST_USER_ID = 1000;
	
	
	@Before
	public void setUp() throws Exception {
		register = new Registration();
		register.setEmail(TEST_EMAIL);
		register.setPassword(TEST_PASSWORD);
		user = new User(TEST_EMAIL, TEST_PASSWORD, TEST_USERNAME);
		userDAO = new UserDAOImpl();
		userDAO.setSessionFactory(this.getSessionFactory());
		userManager = new UserManagerImpl();
		userManager.setUserDAO(userDAO);
		profileManager = new ProfileManager();
		profileManager.setSessionFactory(this.getSessionFactory());
		userManager.setProfileManager(profileManager);
	}

	@After
	public void tearDown() throws Exception {
		if (userManager.hasUser(TEST_EMAIL)) {
			try {
				userManager.deleteUser(TEST_EMAIL);
			} catch (Exception e) { 
				throw new Exception("UserManagerTest teardown exception: " + e.getMessage());
			}
		}
	}


	@Test
	public void testRegisterUserSuccess() throws Exception {
		// Delete existing test user
		if (userManager.hasUser(TEST_EMAIL)) {

			userManager.deleteUser(TEST_EMAIL);

		}
		assertFalse("User already exists and can't be deleted", userManager.hasUser(TEST_EMAIL));
		assertNull(userManager.getUser(TEST_EMAIL));

		// Create registration
		Registration register = new Registration();
		register.setEmail(TEST_EMAIL);
		register.setPassword(TEST_PASSWORD);
		register.setUserName(TEST_USERNAME);
		register.setFirstName(TEST_FIRSTNAME);
		
		// Persist User
		userManager.registerUser(register);;


		// Test user is persisted
		assertTrue("User was not registered", userManager.hasUser(TEST_EMAIL));
		User registeredUser = userManager.getUser(TEST_EMAIL);

		// Test added user is the correct one
		assertEquals("Registered user was different from original user", TEST_EMAIL, registeredUser.getEmail());
		assertEquals("Registered user was different from original user", TEST_PASSWORD, registeredUser.getPassword());
		assertEquals("Registered user was different from original user", TEST_USERNAME, registeredUser.getUserName());
		assertEquals("Registered user was different from original user", TEST_FIRSTNAME, registeredUser.getFirstName());
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
	}

	@Test
	public void testRegisterUserFailEmailExist() throws Exception {
		// Delete existing test user
		if (!userManager.hasUser(TEST_EMAIL)) {

			// Create registration
			Registration register = new Registration();
			register.setEmail(TEST_EMAIL);
			register.setPassword(TEST_PASSWORD);
			register.setUserName(TEST_USERNAME);
			register.setFirstName(TEST_FIRSTNAME);
			
			// Persist User
			userManager.registerUser(register);

		}

		// Test user is persisted
		assertTrue("Original user was not registered", userManager.hasUser(TEST_EMAIL));
		User registeredUser = userManager.getUser(TEST_EMAIL);

		// Create registration
		Registration registerDup = new Registration();
		registerDup.setEmail(TEST_EMAIL);
		registerDup.setPassword(TEST_PASSWORD_1);
		registerDup.setUserName(TEST_USERNAME_1);
		registerDup.setFirstName(TEST_FIRSTNAME);
		
		// Persist User
		try {
			User changedUser = userManager.registerUser(registerDup);
			assertNull("Invalid register went through", changedUser);
		} catch (Exception e) {
			// Test added user is the correct one
			assertEquals("Invalid register went through", TEST_EMAIL, registeredUser.getEmail());
			assertEquals("Invalid register went through", TEST_PASSWORD_1, registeredUser.getPassword());
			assertEquals("Invalid register went through", TEST_USERNAME_1, registeredUser.getUserName());
			assertEquals("Invalid register went through", TEST_FIRSTNAME, registeredUser.getFirstName());
			assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
		}
	}
}
