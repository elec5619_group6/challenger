package au.usyd.elec5619.service;

import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;

public class CurrentUserTest extends BaseConfiguration{
	
	private CurrentUser currentUser;
	private User user;
	private long CHALLENGE_ID;
	
	@Before
	public void setUp() throws Exception
	{
		currentUser = new CurrentUser();
		user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		currentUser.setSessionFactory(this.getSessionFactory());
	}
	
	@Test
	public void testCurrentUser()
	{
		User testUser = currentUser.getUserById(user.getUserId());
		assertEquals(user,testUser);
		
		Query query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() +" where user_id =:id");
		query.setParameter("id", user.getUserId());
		int userChallengeSize = query.list().size();
		
		int testUserChallengeSize = currentUser.getMyChallenge(user.getUserId()).size();
		assertEquals(userChallengeSize,testUserChallengeSize);
		
		currentUser.takeChallenge(CHALLENGE_ID, user.getUserId());
		query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() +" where user_id =:id");
		query.setParameter("id", user.getUserId());
		int afterUserChallengeSize = query.list().size();
		assertEquals(userChallengeSize,afterUserChallengeSize);
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
	}
}
