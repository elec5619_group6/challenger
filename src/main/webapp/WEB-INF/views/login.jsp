<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
<head>
<title>Login</title>
<link href="${pageContext.request.contextPath}/resources/css/signin.css" rel="stylesheet">

<style>
.error {
	color: red;
}
</style>
</head>
<body>

	<div class="container">
		<div class="col-sm-9 col-md-6 col-md-offset-3 jumbotron">
			<form:form class="form-signin" role="form" action="login"
				commandName="userLogin" method="post">
				<h2 class="form-signin-heading">Please sign in</h2>
				<form:errors path="" cssClass="error" />
				<form:input type="email" path="email" class="form-control"
					placeholder="Email address" name="email" required="true"
					autofocus="true" />
				<form:errors path="email" cssClass="error" />
				<form:input type="password" path="password" class="form-control"
					placeholder="Password" name="password" required="true" />
				<form:errors path="password" cssClass="error" />
				<!-- 			<label class="checkbox"> <input type="checkbox" path="rememberMe" -->
				<!-- 					value="remember-me" /> Remember me -->
				<!-- 			</label> -->
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				
				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
					in</button>
				<br>
					Don't have an acount? 
					<a class="btn btn-lg btn-success btn-block" href="register">Register</a>
			</form:form>
			
		</div>

	</div>
	<!-- /container -->
	<div class="container">
		<c:choose>
			<c:when test="${param.error != null}">
				<p>Invalid username and password.
				<p>
			</c:when>
		</c:choose>
	</div>
	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>