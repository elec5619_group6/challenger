package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="challenger.feedback")
public class Feedback implements Serializable{

	
	@Id
	@GeneratedValue
	@Column(name="Feedback_Id")
	private long fdId;
	
	

	@Column(name="Datetime")
    private Date datetime;
	
	@Column(name="Comment")
    private String description;
	
	@Column(name="Interest_Score")
    private int interest_score;
	
	@Column(name="Difficulty_Score")
    private int difficulty_score;
	
	@Column(name="Challenge_id")
	private long challenge_id;
	
	@Column(name="User_id")
	private long user_id;
	
	@Column(name="Video_link")
	private String VideoLink;
	/* Override functions */


	/* Constructor */
	public Feedback(){	
	}
	public Feedback(String comment,int i,int d,long uid,long cid){
		super();
		this.description = comment;
		this.interest_score = i;
		this.difficulty_score = d;
		this.user_id = uid;
		this.challenge_id = cid;
	}
	
	public String getVideoLink() {
		return VideoLink;
	}

	public void setVideoLink(String videoLink) {
		VideoLink = videoLink;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getChallenge_id() {
		return challenge_id;
	}

	public void setChallenge_id(long challenge_id) {
		this.challenge_id = challenge_id;
	}

	public long getFdId() {
		return fdId;
	}

	public void setFdId(long fdId) {
		this.fdId = fdId;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getInterest_score() {
		return interest_score;
	}

	public void setInterest_score(int interest_score) {
		this.interest_score = interest_score;
	}

	public int getDifficulty_score() {
		return difficulty_score;
	}

	public void setDifficulty_score(int difficulty_score) {
		this.difficulty_score = difficulty_score;
	}
	
	
//XML test version
//	private String description;
//	private String user;
////	private int interest;
////	private int difficulty;
//	
//	
//	public String getUser() {
//		return user;
//	}
//
//	public void setUser(String user) {
//		this.user = user;
//	}
//
////	public double getInterest() {
////		return interest;
////	}
////
////	public void setInterest(int interest) {
////		this.interest = interest;
////	}
////
////	public double getDifficulty() {
////		return difficulty;
////	}
////
////	public void setDifficulty(int difficulty) {
////		this.difficulty = difficulty;
////	}
//
//	public String getDescription() {
//		return description;
//	}
//	
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	
//
//	
//	public String toString(){
//		StringBuffer buffer = new StringBuffer();
//		buffer.append("Description: "+description+";");
//		buffer.append("User: "+user+";");
////		buffer.append("interest: "+ interest+";");
////		buffer.append("difficulty"+difficulty);
//		return buffer.toString();
//	}
}


