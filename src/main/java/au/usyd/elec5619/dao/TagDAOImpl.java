package au.usyd.elec5619.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.TagChallenge;
import au.usyd.elec5619.domain.TagUser;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserStatistics;

@Repository
public class TagDAOImpl implements TagDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(TagDAOImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public ArrayList<Tag> getTagsByUserId(long userId) {
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM " + TagUser.class.getName() + " WHERE user_id = :id");
		query.setParameter("id", userId);
		
		ArrayList<Tag> tags = new ArrayList<Tag>();
		if (query.list().isEmpty()) {
			logger.info("User has no tags at all. ");
		} else {
			List<TagUser> tagIds = query.list();
			for (TagUser tuser : tagIds) {
				query = session.createQuery("FROM " + Tag.class.getName() + " WHERE tag_id = :id");
				query.setParameter("id", tuser.getTagId());
				
				logger.info("Getting user tags: " + tuser.getTagId() + " " + ((Tag) query.list().get(0)).getTagName());
				if (1 == query.list().size()) {
					logger.info("adding success.");
					tags.add((Tag) query.list().get(0));
				}
			}
		}
		return tags;
	}
	
	@Override
	@Transactional
	public Tag addTag(String tagName) {
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :name");
		query.setParameter("name", tagName);
		logger.info("Searching for tag: " + tagName);
		if (query.list().isEmpty()) {
			Tag tag = new Tag(tagName);
			session.save(tag);
			return tag;
		} else {
			return (Tag) query.list().get(0);
		}
	}
	
	@Override
	@Transactional
	public Tag getTagById(long tagId) {
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM " + Tag.class.getName() + " WHERE tag_id = :id");
		query.setParameter("id", tagId);
		logger.info("Searching for tagid: " + tagId);
		if (!query.list().isEmpty())
			return (Tag) query.list().get(0);
		else
			return null;
	}
	
	@Override
	@Transactional
	public void addTagForUser(String tagName, long userId) {
		Session session = sessionFactory.getCurrentSession();
		Connection conn = session.connection();
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("SELECT COUNT(*) "
					+ "  FROM challenger.tag NATURAL JOIN challenger.tag_user WHERE user_id = ? AND tag_name = ?");
			stmt.setLong(1, userId);
			stmt.setString(2, tagName);
			logger.info("Searching for tag: " + tagName + " for user: " + userId);
			ResultSet resultSet = stmt.executeQuery();
			
			if (resultSet.next() && (0 == resultSet.getInt(1))) {
				Tag tag = addTag(tagName);
				logger.info("Tag id: " + tag.getId());
				session.save(new TagUser(userId, tag.getId()));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	@Transactional
	public void addTagForChallenge(String tagName, long challengeId) {
		Session session = sessionFactory.getCurrentSession();
		Connection conn = session.connection();
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("SELECT COUNT(*) "
					+ "  FROM challenger.tag NATURAL JOIN challenger.tag_challenge WHERE challenge_id = ? AND tag_name = ?");
			stmt.setLong(1, challengeId);
			stmt.setString(2, tagName);
			ResultSet resultSet = stmt.executeQuery();
			logger.info("Searching for tag: " + tagName + " for challenge: " + challengeId);
			if (0 == resultSet.getInt(1)) {
				Tag tag = addTag(tagName);
				logger.info("Tag id: " + tag.getId());
				session.save(new TagChallenge(challengeId, tag.getId()));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	@Transactional
	public void persistUser(User user) {
		sessionFactory.getCurrentSession().persist(user);
	}
	
	@Override
	@Transactional
	public User findUserById(String id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	@Transactional
	public void removeAllTagsForUser(long userId) {
		Query query = sessionFactory.getCurrentSession().createQuery("FROM " + TagUser.class.getName() + " tu WHERE tu.userId = :userId");
        query.setParameter("userId", userId);
        logger.info("executing query " + query.toString());
        List<TagUser> tagUserList = query.list();
        for (TagUser tu : tagUserList) {
        	sessionFactory.getCurrentSession().delete(tu);
        }
	}
}
