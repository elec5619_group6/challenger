package au.usyd.elec5619.service;

import java.util.ArrayList;

import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;

public interface TagService {

	public void persistUser(User user);
	
	public User findUserById(String id);

	ArrayList<Tag> getTagsByUserId(long userId);

	void addTag(String tagName);

	Tag getTagById(long tagId);

	void addTagForUser(String tagName, long userId);

	void addTagForChallenge(String tagName, long challengeId);

	ArrayList<Tag> getUserTags();

	void setUserTags(ArrayList<Tag> userTags);

	public Object getTagsStringByUserId(long userId);

	public void addTagsForUser(String tags, long userId);

	public void removeAllTagsForUser(long userId);

}
