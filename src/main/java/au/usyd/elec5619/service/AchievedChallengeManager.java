package au.usyd.elec5619.service;

import java.util.*;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Like;
import au.usyd.elec5619.domain.Relationship;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;

@Repository
public class AchievedChallengeManager 
{

	@Autowired
	private SessionFactory sessionFactory;
	private ArrayList<UserChallenge> friendAchievedChallenge;
	//
	
	
	
	@Transactional
	public ArrayList<UserChallenge> findAll(long id) 
	{
		Session currentSession = sessionFactory.getCurrentSession();
		friendAchievedChallenge = new ArrayList<UserChallenge>();
		System.out.println("--------------1------------------");
		Query query = currentSession.createQuery("FROM UserChallenge where user_id =:id");//FROM userchallenge where user_id =: id
		query.setLong("id", id);
		ArrayList<UserChallenge> friendChallenge = (ArrayList)query.list();
		System.out.println("--------------2------------------");
		for (UserChallenge challenge : friendChallenge) 
	    {
			if(challenge.isStatus()==1)
			this.friendAchievedChallenge.add(challenge);
		}
	    
	    return friendAchievedChallenge;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public ArrayList<Like> getLikeList()
	{
		Session currentSession = sessionFactory.getCurrentSession();
		return (ArrayList<Like>)currentSession.createQuery("FROM Like").list();
	}
	
	@Transactional
	public void increaseLike(Like like)
	{
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.save(like);
	}
	
	@Transactional
	public void disLike(long cid,long uid)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM Like where user_id =:id and userchallenge_id =:cid");
		query.setLong("id", uid);
		query.setLong("cid", cid);
		
		Like like = (Like)query.uniqueResult();
		currentSession.delete(like);
	}
	

	
	public AchievedChallengeManager()
	{
		super();
	}
	
}
