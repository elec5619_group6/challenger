package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class UserStatisticsTest extends BaseConfiguration {

	@Test
	public void persistUserStatistics() {
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		UserStatistics userStat = new UserStatistics();
		userStat.setUserId(user.getUserId());
		this.getSession().save(userStat);
		query = this.getSession().createQuery("FROM " + UserStatistics.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		if (query.list().isEmpty()) {
			assertNotNull("user statistics list is empty", null);
			return;
		}
		userStat = (UserStatistics) query.list().get(0);
		assertEquals(user.getUserId(), userStat.getUserId());
		
		query = this.getSession().createQuery("DELETE FROM " + UserStatistics.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + UserStatistics.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
	}
}
