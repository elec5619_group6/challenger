<%@ include file="/WEB-INF/views/head.jsp" %>

<html>
  <head><title><fmt:message key="title"/></title></head>
  <body>
    <h1>
    	Challenger
    </h1>
    <p>
    </p>
    <h3>Current User: ${User.userName}</h3>
    

    <h4>Friends' Challenge</h4>
    <c:forEach items="${Challenges}" var="challenge">
    	
    	Challenge ID: <c:out value="${challenge.challengeId}"/>
    	<i> Challenge Title: <c:out value="${challenge.title}"/></i>
    	 friend ID: <c:out value="${challenge.userId}"/>
    	<br>
    	<c:forEach items = "${Comment}" var = "comment">
    		<c:choose>
   				<c:when test="${comment.userChallengeId eq challenge.userChallengeId}">
   					Comment for <c:out value="${comment.userChallengeId}"/>: <c:out value="${comment.content}"/><br>
   				</c:when>
			</c:choose>    		
    	</c:forEach>
    	<br>
    	<form action="addcomment/${challenge.userChallengeId}&${User.userId}" method="post">
			Comment content: <input type="text" name="content"/>
			<input type="submit" value="Comment"/>
		</form>
		${challenge.like} people like this Challenge
		<c:choose>
   				<c:when test="${challenge.liked}">
   					<a href="dislike/${challenge.userChallengeId}&${User.userId}">Dislike</a>
   				</c:when>
   				<c:otherwise>
   					<a href="like/${challenge.userChallengeId}&${User.userId}">Like</a>
   				</c:otherwise>
		</c:choose>
		
		<c:choose>
   				<c:when test="${challenge.attemped eq false}">
   					<a href="attempt/${challenge.challengeId}&${User.userId}">Attempt</a>
   				</c:when>
		</c:choose>    	
		
    	<br>
    </c:forEach>
    
    <h4>Friends</h4>
    
    <c:forEach items="${Friends}" var="friend">
    	
    	Friend's ID: <c:out value="${friend.userId}"/>
    	<i> Friend's name: <c:out value="${friend.userName}"/></i>
    	<a href="delete/${User.userId}&${friend.userId }">delete</a>
    	<br>
    	<br>
    </c:forEach>
    <h4>Add Friend by ID:</h4>
    <br>
    <form action="add/${User.userId}" method="post">
			Friend ID: <input type="text" name="friendId"/>
			<input type="submit" value="Add"/>
	</form>
    
  </body>
</html>
