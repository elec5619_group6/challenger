<%@ include file="/WEB-INF/views/head.jsp"%>

<html lang="en">
<head>
<title>User Home</title>
<link
	href="${pageContext.request.contextPath}/resources/css/dashboard.css"
	rel="stylesheet">

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-5 col-xs-offset-7 col-sm-3 col-sm-offset-9 col-md-2 sidebar col-md-offset-10">
				<ul class="nav nav-sidebar">
					<div class="list-group">
						<div class="media">
							<a class="pull-left" href="#"> <img class="media-object"
								src="" alt="">
							</a>
							<div class="media-body">
								<h4 class="media-heading">
									<span class="glyphicon glyphicon-user"></span>
									<c:out value='${user.userName }' />
								</h4>
								<a href="${pageContext.request.contextPath}/profile/${user.userId }">View Profile</a>
							</div>
						</div>
					</div>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="list-group">
							<a href="<c:out value='${pageContext.request.contextPath}/user_home'/>" class="list-group-item">
								<span class="glyphicon glyphicon-home"> </span> Activity
							</a> <a href="<c:out value="${pageContext.request.contextPath}/challenges"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-send"> </span> Challenges
							</a><a href="<c:out value="${pageContext.request.contextPath}/statistics"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-list"></span> Statistics
							</a> <a href="<c:out value="${pageContext.request.contextPath}/barchart"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-signal"></span> Graphs
							</a> <a href="<c:out value="${pageContext.request.contextPath}/achievements"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-thumbs-up"></span> Achievements
							</a>
						</div></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title"><span class="glyphicon glyphicon-user"></span>My Friends</h3> 
								<a href="${pageContext.request.contextPath}/social/">See Friends Activity</a>
							</div>
							<ul class="list-group">
							<c:forEach items="${friends}" var="friend">
								<li class="list-group-item">
									<a href="${friend.userId}" class="list-group-item"> 
										<span>
											<p class="list-group-item-heading">${friend.userName}</p>
										</span>
									</a>
								</li>
							</c:forEach>
							</ul>
						</div></li>
				</ul>
			</div>
			<div class="col-xs-7 col-sm-9 col-md-10 main">
				<c:if test="${target.userId == user.userId }"><h1 class="page-header"><span class="glyphicon glyphicon-home"> </span>My Activity</h1></c:if>
				<c:if test="${target.userId != user.userId }">
				<h1 class="page-header"><c:out value="${target.userName }" />'s Activity
				<a class="btn btn-sm btn-info" href="${pageContext.request.contextPath}/profile/${target.userId}">View Profile</a>
				</h1> 
				
				</c:if>
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="panel panel-info" align="center">
							<div class="panel-heading">
								<h3 class="panel-title"><span class="glyphicon glyphicon-send"> </span>Recent Challenges</h3>
							</div>
							<div class="panel-body">
							
								<div class="list-group">
									<c:forEach items="${challengelist}" var="challenge">

										<a href="${pageContext.request.contextPath}/challengedetail/${challenge.chaId}"
											class="list-group-item clearfix"> 
											<span>
												<h4 class="list-group-item-heading">${challenge.title}</h4>
												<p class="list-group-item-text">${challenge.description}</p>
											</span> 
											<span class="pull-right"> 
												<input type="button"
													class="btn btn-md btn-primary" value="Detail" />
											</span>
										</a>

									</c:forEach>
								</div>

							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 placeholder">
						<div class="panel panel-danger" align="center">
							<div class="panel-heading">
								<h3 class="panel-title"><span class="glyphicon glyphicon-thumbs-up"> </span>Recent Achievements</h3>
							</div>
							<div class="panel-body">
							
								<div class="list-group">
									<c:forEach items="${achieves}" var="achieve">

										<div class="list-group">
											<h4 class="list-group-item-heading">${achieve.title}</h4>
											<p class="list-group-item-text">${achieve.description}</p>
											<div class="progress">
												<div class="progress-bar" role="progressbar"
													aria-valuenow="${achieve.progress}" aria-valuemin="0"
													aria-valuemax="${achieve.quantity}"
													style="width: ${achieve.percentage}%;">
													${achieve.progress}/${achieve.quantity}</div>
											</div>
										</div>

									</c:forEach>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-12 col-sm-6 placeholder">
					<div class="panel panel-danger" align="center">
						<div class="panel-heading">
							<h3 class="panel-title">Friends</h3>
						</div>
						<div class="panel-body">

							<div class="list-group">
								<c:forEach items="${targetFriends}" var="tFriend">

									<a
										href="${pageContext.request.contextPath}/user_home/${tFriend.userId}"
										class="list-group-item clearfix"> <span>
											<h4 class="list-group-item-heading">${tFriend.userName}</h4>
									</span> <span class="pull-right"> <input type="button"
											class="btn btn-md btn-primary" value="View Profile" />
									</span>
									</a>

								</c:forEach>
							</div>

						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 placeholder">
					<div class="panel panel-success" align="center">
						<div class="panel-heading">
							<h3 class="panel-title"><span class="glyphicon glyphicon-list"> </span>Statistics</h3>
						</div>
						<div class="panel-body">

							<div class="list-group">
								<table class="table">
									<tr>
										<td align="right">Total Challenge:</td>
										<td>${statistics.totalChallenge}</td>
									</tr>
									<tr>
										<td align="right">Last Login Time:</td>
										<td>${statistics.lastLoginTime}</td>
									</tr>
									<tr>
										<td align="right">Total Login Number:</td>
										<td>${statistics.totalLoginNum}</td>
									</tr>
									<tr>
										<td align="right">Completed Challenge Number:</td>
										<td>${statistics.cmpChg}</td>
									</tr>
									<tr>
										<td align="right">Ongoing Challenge Number:</td>
										<td>${statistics.incmpChg}</td>
									</tr>
									<tr>
										<td align="right">Challenge Complete Rate:</td>
										<td>${statistics.completeRate}%</td>
									</tr>
								</table>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>