package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User statistics
 * @author John
 * 
 */
@Entity
@Table(name="challenger.user_statistics")
public class UserStatistics implements Serializable {

	@Id
	@Column(name="user_id")
	private long userId;
	
	@Column(name="completed_challenges")
	private int cmpChg;
	
	@Column(name="incompleted_challenges")
	private int incmpChg;
	
	@Column(name="total_challenge")
	private int totalChallenge = 0;
	
	@Column(name="complete_rate")
	private double completeRate;
	
	@Column(name="last_login_time")
	private Date lastLoginTime;
	
	@Column(name="total_login_num")
	private int totalLoginNum = 0;
	
	/* Constructor */
	public UserStatistics() {}

	public UserStatistics(long userId, Date lastLoginTime) {
		super();
		this.userId = userId;
		this.lastLoginTime = lastLoginTime;
	}

	/* Getter & setter */
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getTotalChallenge() {
		return totalChallenge;
	}

	public void setTotalChallenge(int totalChallenge) {
		this.totalChallenge = totalChallenge;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getTotalLoginNum() {
		return totalLoginNum;
	}

	public void setTotalLoginNum(int totalLoginNum) {
		this.totalLoginNum = totalLoginNum;
	}
	
	public void incTotalLoginNum() {
		this.totalLoginNum++;
	}

	public int getCmpChg() {
		return cmpChg;
	}

	public void setCmpChg(int cmpChg) {
		this.cmpChg = cmpChg;
		this.incmpChg = (incmpChg > cmpChg) ? incmpChg - cmpChg : 0;
		this.totalChallenge = this.cmpChg + this.incmpChg;
		this.completeRate = (0 == this.totalChallenge) ? 0 : ((double)cmpChg / (double) this.totalChallenge) * 100;
	}
	
	public void incCmpChg() {
		this.cmpChg++;
		this.incmpChg--;
		this.completeRate = (0 == this.totalChallenge) ? 0 : ((double)cmpChg / (double) this.totalChallenge) * 100;
	}

	public int getIncmpChg() {
		return incmpChg;
	}

	public void setIncmpChg(int incmpChg) {
		this.incmpChg = incmpChg;
		this.totalChallenge = incmpChg + cmpChg;
		this.completeRate = (0 == this.totalChallenge) ? 0 : ((double)cmpChg / (double) this.totalChallenge) * 100;
	}
	
	public void incIncmpChg() {
		this.incmpChg++;
		this.totalChallenge++;
		this.completeRate = (0 == this.totalChallenge) ? 0 : ((double)cmpChg / (double) this.totalChallenge) * 100;
	}

	public double getCompleteRate() {
		return completeRate;
	}

	public void setCompleteRate(double completeRate) {
		this.completeRate = completeRate * 100;
	}
}
