package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class CommentTest extends BaseConfiguration{
	
	private static long COMMENT_TIME = 10;

	@Test
	public void persistComment()
	{
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		UserChallenge userChallenge = new UserChallenge(USER_CHALLENGE_ID,123,0,0);
		//System.out.println(userChallenge.getUserChallengeId()+"11111111111111111111111111111111111111111222222");
		this.getSession().save(userChallenge);
		query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() + " WHERE time_end =:userChallengeId");
		query.setParameter("userChallengeId", 123);
		userChallenge = (UserChallenge) query.list().get(0);
		
		Comment comment = new Comment(userChallenge.getUserChallengeId(),user.getUserId(),"test",COMMENT_TIME);
		this.getSession().save(comment);
		query = this.getSession().createQuery("FROM " + Comment.class.getName() + " WHERE comment_time =:comment_time");
		query.setParameter("comment_time", COMMENT_TIME);
		if(query.list().isEmpty())
		{
			assertNotNull("comment list is empty",null);
			return;
		}
		comment = (Comment) query.list().get(0);
		assertEquals(user.getUserId(),comment.getCommenterId());
		assertEquals(userChallenge.getUserChallengeId(),comment.getUserChallengeId());
		
		query = this.getSession().createQuery("DELETE FROM " + Comment.class.getName() + " WHERE comment_time =:comment_time");
		query.setParameter("comment_time", COMMENT_TIME);
		query.executeUpdate();
		
		//assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + UserChallenge.class.getName() + " WHERE userChallengeId = :userChallengeId");
		query.setParameter("userChallengeId", userChallenge.getUserChallengeId());
		query.executeUpdate();
	}
}
