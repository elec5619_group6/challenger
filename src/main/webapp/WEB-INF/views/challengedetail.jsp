<%@ include file="/WEB-INF/views/head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Challenge detail</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Challenges</li>
							<li><a href="../challenges">Back to my challenges</a></li>
							<li><a href="../challengelist">Challenge list</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="jumbotron">
					<div>
						<h3>Challenge Details</h3>
						<br />
						<c:if test="${challenge.icon!=''}">
    					<img src="${challenge.icon}" width="128" height="128">
    					</c:if><br>
						<h4>Title: ${challenge.title}</h4>
						<h4>Description: ${challenge.description}</h4>
						<h4>Creator: ${challenge.creator}</h4>
						<h4>Created Time: ${challenge.postTime}</h4>
						<h4>Rating: ${challenge.rating}</h4>
						<h4>Difficulty: ${challenge.difficulty}</h4>
					
						<br />
					</div>
					<span> <input type="button"
						onClick="parent.location='../selectchallenge/${challenge.chaId}'"
						class="btn btn-lg btn-primary" value="Start" />
					</span>
					<span> <input type="button" type="submit"
						onClick="parent.location='../leavefeedback/${challenge.chaId}'"
						class="btn btn-lg btn-primary" value="Leave Feedback" />
					</span>
				</div>

			</div></div></div>
			
		<c:forEach items="${feedbacks}" var="feedback">
    	<div class="container">
    	<div class="panel panel-primary">
    	<div class="panel-heading">
    	<h4>By:&nbsp<c:out value="${feedback.userName}"/></h4>
    	<p>Interest: ${feedback.interestScore} &nbsp&nbsp Difficulty:${feedback.difficultyScore}</p>
    	</div>
    	<div class="panel-body">
    	<c:out value="${feedback.review}"/><br><br>
    	<c:if test="${feedback.videoLink!=''}">
    	<img src="${feedback.videoLink}" width="128" height="128">
    	</c:if>
    	</div>
    	<div class="panel-footer">	
    			<c:forEach items="${comment}" var="comm">
    			<style type="text/css">
   					 .x-fieldset{border:2px solid #B5B8C8;padding:10px;margin-bottom:10px;display:block;background :#FFEC8B;
 	margin:auto;
	}
  				</style>   			 				
    				<c:if test="${feedback.feedbackId==comm.feedback_id}">
    				<fieldset class="x-fieldset">
    				<legend>${comm.date} by ${comm.userName} </legend>
					<label><c:out value="${comm.comment}"/></label>			
					</fieldset>
					</c:if>				
				</c:forEach>
				<form action="showfeedback/${feedback.feedbackId}&${User.userId}&${challenge.chaId}" method="post">					
				Comment content: <input type="text" name="comment"/>
				<input type="submit" value="Comment"/>
				</form>				
		</div>
    	</div>
    	</div>
    	<br>
    	<br>
    </c:forEach>
			<!--/span-->

		</div>
		<!--/row-->
	</div>
</body>
</html>