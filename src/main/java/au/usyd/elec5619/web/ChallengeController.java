package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.ChallengeDisplay;
import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.FeedbackForDisplay;
import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;
import au.usyd.elec5619.service.AchievementAttemptManager;
import au.usyd.elec5619.service.ChallengeManager;
import au.usyd.elec5619.service.CurrentUser;
import au.usyd.elec5619.service.FeedbackCommentMananger;
import au.usyd.elec5619.service.FeedbackManager;

/**
 * Challenge controller
 * @author John
 *
 */
@Controller
public class ChallengeController {

	private static final Logger logger = LoggerFactory.getLogger(ChallengeController.class);
	
	@Resource(name="challengeManager")
	private ChallengeManager challengeManager;
	@Resource(name="feedbackManager")
	private FeedbackManager feedbackmanager;
	
	@Resource(name="feedbackCommentManager")
	private FeedbackCommentMananger feedbackcommentmanager;
	
	@Resource(name="currentUser")
	private CurrentUser currentUser;
	
	@Resource(name="achAmptManager")
	private AchievementAttemptManager achAmptManager;
	
	/**
	 * Get all challenges user has*/
	@RequestMapping(value="/challenges", method=RequestMethod.GET)
	public String getUserChallengeList(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		ArrayList<ChallengeDisplay> challengeList = currentUser.getUserChallenges(user.getUserId());
		model.addAttribute("challengelist", challengeList);
		
		return "challenges";
	}
	
	/**
	 * Get single user challenge by userChallengeId*/
	@RequestMapping(value="/challenges/{ucid}", method=RequestMethod.GET)
	public String getUserChallenge(@PathVariable("ucid") long ucid, Model model, HttpSession session,
			HttpServletRequest httpServletRequest) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		ArrayList<ChallengeDisplay> challengeList = currentUser.getChallengeList();
		for (ChallengeDisplay challenge : challengeList) {
			if (ucid == challenge.getUserChallengeId()) {
				model.addAttribute("challenge", challenge);
			}
		}
		return "challengeupdate";
	}
	
	@RequestMapping(value="/challengelist", method=RequestMethod.GET)
	public String getChallengeList(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		ArrayList<Challenge> challengeList = challengeManager.getChallengeList();
		model.addAttribute("challengelist", challengeList);
		
		return "challengeselect";
	}
	
	@RequestMapping(value="/selectchallenge/{cid}")
	public String selectChallenge(@PathVariable("cid") long cid,
			HttpSession session, RedirectAttributes redirectAttrs) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		currentUser.takeChallenge(cid, user.getUserId());
		achAmptManager.startNewChallenge(user.getUserId());
		redirectAttrs.addFlashAttribute("chgtitle", "Challenge has started! ");
		logger.info("selecting challenge " + cid);
		
		return "redirect:/challengelist";
	}
	
	@RequestMapping(value="/updatechallenge/{ucid}")
	public String updateChallenge(@PathVariable("ucid") long ucid, HttpServletRequest httpServletRequest,
			HttpSession session, RedirectAttributes redirectAttrs) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		logger.info("progressout: " + httpServletRequest.getParameter("progressoutput"));
		logger.info("durationout: " + httpServletRequest.getParameter("durationoutput"));
		
		int progress = Integer.parseInt(httpServletRequest.getParameter("progressoutput"));
		int duration = Integer.parseInt(httpServletRequest.getParameter("durationoutput"));

		if (progress >= duration) {
			currentUser.updateUserChallenge(new UserChallenge(ucid, System.currentTimeMillis(), progress, 1));
			if (achAmptManager.finishChallenge(user.getUserId())) {
				redirectAttrs.addFlashAttribute("msg", "New Achievement Unlocked! ");
			}
		} else {
			currentUser.updateUserChallenge(new UserChallenge(ucid, System.currentTimeMillis(), progress, 0));
		}
		
		return "redirect:/challenges";
	}
	
	@RequestMapping(value="/challengedetail/{cid}")
	public String getChallengeDetail(@PathVariable("cid") long cid, Model model,
			HttpSession session, RedirectAttributes redirectAttrs) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";

		List<Feedback> fblist = feedbackmanager.getRelatedFeedback(cid);
		feedbackcommentmanager.getFeedbackCommentList();
		ArrayList<FeedbackForDisplay> feedbackd = new ArrayList<FeedbackForDisplay>(); 
		for(Feedback f : fblist)
		{
			feedbackd.add(new FeedbackForDisplay(f,challengeManager.findRelatedChallenge(f.getChallenge_id()),currentUser.getUserById(f.getUser_id())));
		}
		model.addAttribute("challenge", challengeManager.getChallengeById(cid));
		model.addAttribute("comment",feedbackcommentmanager.getCommDisplay());				
		model.addAttribute("User", user );
     	model.addAttribute("feedbacks",feedbackd);	
		
		return "challengedetail";
	}
	
	@RequestMapping(value="/addchallenge", method = RequestMethod.GET)
	public String getChallengeCreationForm(HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		return "challengecreate";
	}
	
	@RequestMapping(value="/addchallenge", method = RequestMethod.POST)
	public String addChallenge(Model model, HttpSession session,
			RedirectAttributes redirectAttrs, HttpServletRequest httpServletRequest) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";

		Challenge challenge = new Challenge();
		challenge.setTitle(httpServletRequest.getParameter("title"));
		challenge.setDescription(httpServletRequest.getParameter("description"));
		challenge.setCreator(user.getUserName());
		challenge.setDuration(Integer.parseInt(httpServletRequest.getParameter("duration")));
		challenge.setPostTime(new Date());
		challenge.setRating(Double.parseDouble(httpServletRequest.getParameter("rating")));
		challenge.setDifficulty(Double.parseDouble(httpServletRequest.getParameter("difficulty")));
		if(String.valueOf(httpServletRequest.getParameter("iconpath"))=="")
		{
			challenge.setIcon("http://charitable-bonds.allia.org.uk/avatars/challengelogo-full-jpeg.jpg");
		}
			else
		{
		challenge.setIcon(String.valueOf(httpServletRequest.getParameter("iconpath")));
		}
		//challenge.setPictureId(Integer.parseInt(httpServletRequest.getParameter("pictureid")));
		
		/* create tags */
		ArrayList<Tag> tags = new ArrayList<Tag>();
		String tagString = httpServletRequest.getParameter("tags");
		while (true) {
			int pos = tagString.indexOf(',');
			if (pos < 0) {
				tags.add(new Tag(tagString));
				break;
			}
			tags.add(new Tag(tagString.substring(0, pos)));
			tagString = tagString.substring(pos + 1);
		}
		
		challengeManager.addChallenge(challenge, tags);
		challenge =  challengeManager.getChallengeByTitle(httpServletRequest.getParameter("title"));
		if (null == challenge) {
			redirectAttrs.addFlashAttribute("msg", "Challenge creation failed! ");
			return "redirect:/challengelist";
		} else {
			if (achAmptManager.updateChallengeCreationAchievement()) {
				redirectAttrs.addFlashAttribute("msg", "New Achievement Unlocked! ");
			}
			return "redirect:/challengedetail/" + challenge.getChaId();
		}
	}
	

}
