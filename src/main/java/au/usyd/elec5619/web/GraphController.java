package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.GraphManagerImpl;

/**
 * Graph displaying controller
 * @author John
 *
 */
@Controller
public class GraphController {

	private static final Logger logger = LoggerFactory.getLogger(GraphController.class);
	
	@Resource(name="graphManager")
	private GraphManagerImpl graphManager;
	
	@RequestMapping(value="/barchart", method=RequestMethod.GET)
	public String getBarChartStatistics(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		logger.info("Accessing all challenges history for the user " + user.getUserId());
		graphManager.getUserChallengeRecord(user.getUserId());
		graphManager.mergeRecord();
		
		model.addAttribute("display", graphManager.getDisplayList());
		return "barchart";
	}
}
