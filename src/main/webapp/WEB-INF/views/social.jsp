
<%@ include file="/WEB-INF/views/head.jsp"%>

<html>
<head>
<title><fmt:message key="title" /></title>
<script type="text/javascript">
	function checkCommentInput(id)
	{
        var content = document.getElementById(id).value;
		if(content!="")
		{
			document.getElementById("form"+id).submit();
		}
	}
	
	function checkFriendIdNull()
	{
        var content = document.getElementById("addFriend").value;
		if(content!="")
		{
			document.getElementById("addFriendForm").submit();
		}
	}
</script>
</head>
<body onload="printnotice('addFriend','Enter Friend ID')">
	<div class="container">
		<div class="row">
			<div class="col-md-7">

				<c:forEach items="${Challenges}" var="challenge">

					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-heading">
							<c:forEach items="${Friends}" var="friend">
								<c:choose>
									<c:when test="${friend.userId eq challenge.userId}">
								Your Friend: <a href="../user_home/${friend.userId}"><c:out value="${friend.userName} " /></a>
									</c:when>
								</c:choose>
							</c:forEach>

						</div>
						<div class="panel-body">
							<h2>
								<c:out value="${challenge.title}" />
							</h2>
							<p>
								<c:out value="${challenge.description }" />
							</p>
						</div>

						<!-- List group -->
						<ul class="list-group">
							<li class="list-group-item">
								<c:choose>
										<c:when test="${challenge.attemped eq false}">
											<a href="attempt/${challenge.challengeId}&${User.userId}">
												<button type="button" class="glyphicon glyphicon-plus">
												</button>
											</a>
										</c:when>
								</c:choose>
								<c:choose>
									<c:when test="${challenge.liked}">
										<a href="dislike/${challenge.userChallengeId}&${User.userId}">
											<button type="button" class="glyphicon glyphicon-thumbs-down">
											</button>
										</a>
									</c:when>
									<c:otherwise>
										<a href="like/${challenge.userChallengeId}&${User.userId}">
											<button type="button" class="glyphicon glyphicon-thumbs-up">
											</button>
										</a>
									</c:otherwise>
								</c:choose> <span>: ${challenge.like} people liked this Challenge</span></li>



							<c:forEach items="${Comment}" var="comment">
								<c:choose>
									<c:when
										test="${comment.userChallengeId eq challenge.userChallengeId}">
										<li class="list-group-item"><c:choose>
												<c:when test="${!(User.userId eq comment.commenterId)}">
													<c:forEach items="${Friends}" var="friend">
														<c:choose>
															<c:when test="${friend.userId eq comment.commenterId}">
																<c:out value="${friend.userName} " />
															</c:when>
														</c:choose>
													</c:forEach>

												</c:when>
												<c:otherwise>
													<c:out value="${User.userName} " />
												</c:otherwise>

											</c:choose> <span>: </span> <c:out value="${comment.content}" /></li>
									</c:when>
								</c:choose>
							</c:forEach>

							<li class="list-group-item">
								<form
									action="addcomment/${challenge.userChallengeId}&${User.userId}"
									method="post"
									id = "form${challenge.userChallengeId}">
									
									
									
									<input type="text" id="${challenge.userChallengeId}" name="content" placeholder="Write a comment..."/> 
									<button type="button" class="glyphicon glyphicon-comment" value="Comment" onclick="checkCommentInput('${challenge.userChallengeId}')"> </button>
									<span id = "info${challenge.userChallengeId}"></span>
								</form>
							</li>
						</ul>
					</div>
					<br>
				</c:forEach>
			</div>

			<div class="col-md-3" onload="setColor('content')">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header"><h4>Friend List</h4></li>

							<c:forEach items="${Friends}" var="friend">
    							<li>
    							
    							
    							<a href="delete/${User.userId}&${friend.userId }"><c:out value="${friend.userName}" />(ID: <c:out value="${friend.userId}" />)&nbsp;&nbsp;<span class="glyphicon glyphicon-remove"></span></a>
    							
								
								</li>
							</c:forEach>
							<li>
								<form action="add/${User.userId}" method="post" id="addFriendForm">
									&nbsp;&nbsp;&nbsp;<input type="text" name="friendId" size="10" id="addFriend" placeholder="Enter Friend ID"/> 
									<button type="button" class="btn btn-default btn-sm" onclick="checkFriendIdNull()">
										<span class="glyphicon glyphicon-plus-sign"></span>
									</button>
								</form>
							</li>

						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>


		</div>
	</div>





</body>
</html>
