
<%@ include file="/WEB-INF/views/head.jsp"%>

<html>
<head>
<title><fmt:message key="title" /></title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8">

				<c:forEach items="${Challenges}" var="challenge">
    			
				<div class="panel panel-default">
					<!-- Default panel contents -->
					<div class="panel-heading">
					<c:forEach items = "${Friends}" var = "friend">
						<c:choose>
							<c:when test="${friend.userId eq challenge.userId}">
								Your Friend: <c:out value="${friend.userName} "/>
							</c:when>
						</c:choose>
					</c:forEach>
					
					</div>
					<div class="panel-body">
						<h2><c:out value = "${challenge.title}"/></h2>
						<p><c:out value = "${challenge.description }"/></p>
					</div>

					<!-- List group -->
					<ul class="list-group">
						<li class="list-group-item">
							Liked: 
						</li>
						<li class="list-group-item">Dapibus ac facilisis in</li>
						<li class="list-group-item">Morbi leo risus</li>
						<li class="list-group-item">Porta ac consectetur ac</li>
						<li class="list-group-item">Vestibulum at eros</li>
					</ul>
				</div>
					<br>
				</c:forEach>
			</div>
			
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Friend List</li>
							<li><a href="./challengelist">Choose challenge</a></li>
							<li><a href="./addchallenge">Create challenge</a></li>
							<li><a href="#">Link</a></li>
							<li><a href="#">Link</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			
			
			</div>
			</div>
		
		
		
		

</body>
</html>
