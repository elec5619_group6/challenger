package au.usyd.elec5619.domain;

import java.io.Serializable;

public class ChallengeInvite implements Serializable {
//
	private String inviteID;
	private String userID;
	private String inviteeID;
	private String ChallengeID;
	private boolean accepted;
	
	
	public String getInviteID() {
		return inviteID;
	}
	public void setInviteID(String inviteID) {
		this.inviteID = inviteID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getInviteeID() {
		return inviteeID;
	}
	public void setInviteeID(String inviteeID) {
		this.inviteeID = inviteeID;
	}
	public String getChallengeID() {
		return ChallengeID;
	}
	public void setChallengeID(String challengeID) {
		ChallengeID = challengeID;
	}
	public boolean isAccepted() {
		return accepted;
	}
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	
	
}
