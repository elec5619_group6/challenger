package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class TagTest extends BaseConfiguration {

	@Test
	public void persistTag() {
		Tag tag = new Tag(TAGNAME);
		this.getSession().save(tag);
		Query query = this.getSession().createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		if (query.list().isEmpty()) {
			assertNotNull("tag list is empty", null);
			return;
		}
		tag = (Tag) query.list().get(0);
		assertEquals(TAGNAME, tag.getTagName());
		
		query = this.getSession().createQuery("DELETE FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :tag_name");
		query.setParameter("tag_name", TAGNAME);
		
		assertTrue(query.list().isEmpty());
	}
}
