package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.FriendManager;
import au.usyd.elec5619.service.Registration;
import au.usyd.elec5619.service.TagService;
import au.usyd.elec5619.service.UserManager;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ProfileController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@Resource(name="tagService")
	TagService tagService;
	
	@Resource(name="friendManager")
	private FriendManager friendManager;
	
	
	@RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
	public String userProfile(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/login";
		long userId = user.getUserId();
		
		return "redirect:/profile/" + userId;
	}
	
	@RequestMapping(value = "/profile/{uid}", method = RequestMethod.GET)
	public String userProfileWithId(@PathVariable("uid") long userId, Model model, HttpSession session) {
		User self = (User) session.getAttribute("user");
		User target = userManager.getUser(userId);
		
		if (self == null) {
			return "redirect:/login";
		}
		logger.info("user info: " + self.getEmail());
		logger.info("user info: " + self.getUserId());
		model.addAttribute("user", self);
		
		logger.info("user info: " + target.getEmail());
		logger.info("user info: " + target.getUserId());
		model.addAttribute("target", target);
		
		ArrayList<Tag> tags = tagService.getTagsByUserId(userId);
		for (Tag tag : tags)
			logger.info("tag from session " + tag.getTagName());
		model.addAttribute("tags", tagService.getTagsStringByUserId(target.getUserId()));
		
		// Display Friends
		friendManager.getFriends(self.getUserId());
		List<Long> friendsListStrings = friendManager.getFriends(self.getUserId());
		List<User> friendsList = new ArrayList<User>();
		for (Long fid : friendsListStrings) {
			friendsList.add(userManager.getUser(fid));
		}
		model.addAttribute("friends", friendsList);
		
		return "user_profile";
	}
	
	@RequestMapping(value = "/profile/edit", method = RequestMethod.GET)
	public String userProfileEdit(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/login";
		long userId = user.getUserId();
		
		return "redirect:/profile/" + userId + "/edit";
	}
	
	@RequestMapping(value = "/profile/edit_tags", method = RequestMethod.GET)
	public String userTagsEdit(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/login";
		long userId = user.getUserId();
		
		return "redirect:/profile/" + userId + "/edit_tags";
	}
	
	@RequestMapping(value = "/profile/{uid}/edit", method = RequestMethod.GET)
	public String userProfileEditWithId(@PathVariable("uid") long userId, Model model, HttpSession session) {
		User self = (User) session.getAttribute("user");
		if (null == self) {
			return "redirect:/login";
		} else if (self.getUserId() != userId) {
			return "redirect:/profile/" + userId;
		}
		
        logger.info("user has email " + self.getEmail());
		logger.info("user has id " + self.getUserId());
		model.addAttribute("user", self);
		
		model.addAttribute("target", self);
		
		Registration userUpdate = userManager.getUpdateObjectFromUser(self);
		model.addAttribute("userUpdate", userUpdate);

		ArrayList<Tag> tags = tagService.getTagsByUserId(userId);
		for (Tag tag : tags)
			logger.info("tag from session " + tag.getTagName());
		model.addAttribute("tags", tagService.getTagsStringByUserId(userId));
		
		// Display Friends
		friendManager.getFriends(self.getUserId());
		List<Long> friendsListStrings = friendManager.getFriends(self.getUserId());
		List<User> friendsList = new ArrayList<User>();
		for (Long fid : friendsListStrings) {
			friendsList.add(userManager.getUser(fid));
		}
		model.addAttribute("friends", friendsList);
		
		return "edit_profile";
	}
	
	@RequestMapping(value = "/profile/{uid}/edit_tags", method = RequestMethod.GET)
	public String userTagsEditWithId(@PathVariable("uid") long userId, Model model, HttpSession session) {
		User self = (User) session.getAttribute("user");
		if (null == self) {
			return "redirect:/login";
		} else if (self.getUserId() != userId) {
			return "redirect:/profile/" + userId;
		}
		
        logger.info("user has email " + self.getEmail());
		logger.info("user has id " + self.getUserId());
		model.addAttribute("user", self);
		
		model.addAttribute("target", self);
		
		Registration userUpdate = userManager.getUpdateObjectFromUser(self);
		model.addAttribute("userUpdate", userUpdate);

		List<Tag> tagList = tagService.getTagsByUserId(userId);
		model.addAttribute("tags", tagService.getTagsStringByUserId(userId));
		
		// Display Friends
		friendManager.getFriends(self.getUserId());
		List<Long> friendsListStrings = friendManager.getFriends(self.getUserId());
		List<User> friendsList = new ArrayList<User>();
		for (Long fid : friendsListStrings) {
			friendsList.add(userManager.getUser(fid));
		}
		model.addAttribute("friends", friendsList);
		
		return "edit_tags";
	}
	
	@RequestMapping(value = "/profile/{uid}/edit", method = RequestMethod.POST)
	public String saveUserProfileWithId(@PathVariable("uid") long userId, 
			Model model, 
			@Valid @ModelAttribute("userUpdate") Registration userUpdate, 
			BindingResult bindingResult,
			HttpSession session,
			HttpServletRequest httpServletRequest) {
		
		User self = (User) session.getAttribute("user");
		if (null == self) {
			return "redirect:/login";
		} else if (self.getUserId() != userId) {
			return "edit_profile";
		}
		
		if (!bindingResult.hasFieldErrors("email")) {
			User emailDupUser = userManager.getUser(userUpdate.getEmail());
			if (emailDupUser != null && emailDupUser.getUserId() != self.getUserId()) {
				bindingResult.rejectValue("email", "EmailNotExist.profile.email");
				return "edit_profile";
			}
		}
		
		if (bindingResult.hasErrors()) {
			return "edit_profile";
		}
		
		logger.info("Updating user " + userUpdate.getEmail() + " with password " + userUpdate.getPassword());
		
		User currentUser;
		try {
			userUpdate.setUserId(userId);
			currentUser = userManager.updateUser(userUpdate);
		} catch (Exception e) {
			bindingResult.rejectValue("email", "EmailExist.profile.email");
			return "edit_profile";
		}

		// Update model user properties
		currentUser = userManager.getUser(userId);
		model.addAttribute("user", currentUser);
		
		// Update session user properties
		session.setAttribute("user", currentUser);
		
		return "redirect:/profile/" + userId;
	}
	
	@RequestMapping(value = "/profile/{uid}/edit_tags", method = RequestMethod.POST)
	public String saveUserProfileWithId(@PathVariable("uid") long userId, 
			Model model, 
			@ModelAttribute("tags") String tags,
			BindingResult bindingResult,
			HttpSession session,
			HttpServletRequest httpServletRequest) {
		
		User self = (User) session.getAttribute("user");
		if (null == self) {
			return "redirect:/login";
		} else if (self.getUserId() != userId) {
			return "redirect:/profile/" + userId;
		}
		
		/* delete old tags */
		tagService.removeAllTagsForUser(self.getUserId());
		
		/* create tags */
		tagService.addTagsForUser(tags, self.getUserId());	
		
		// Update model user properties
		model.addAttribute("user", self);
		
		model.addAttribute("target", self);	
		
		return "redirect:/profile/" + userId;
	}
	
}
