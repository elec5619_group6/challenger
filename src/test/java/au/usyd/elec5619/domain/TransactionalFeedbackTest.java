package au.usyd.elec5619.domain;

import java.util.Date;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class TransactionalFeedbackTest extends BaseConfiguration {

	@Test
	public void persistFeedback()
	{
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		challenge = (Challenge)query.list().get(0);
		/**
		 * create object, prepare data
		 */
		Feedback feedback = new Feedback();
		feedback.setUser_id(user.getUserId());
		feedback.setChallenge_id(challenge.getChaId());
		feedback.setInterest_score(FB_INTEREST);
		feedback.setDifficulty_score(FB_DIFFICUITY);
		feedback.setDatetime(new Date());		
		feedback.setDescription(FB_COMMMENT);
		/**
		 * 	transaction data to database
		 */
		this.getSession().save(feedback);
		/**
		 * Get data from database
		 */
		query = this.getSession().createQuery("FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FB_COMMMENT);
		if (query.list().isEmpty()) {
			assertNotNull("challenge list is empty", null);
			return;
		}
		feedback = (Feedback) query.list().get(0);
		/**
		 * Test whether data were consistent
		 */
		assertEquals(FB_COMMMENT, feedback.getDescription());
		assertEquals(FB_INTEREST, feedback.getInterest_score());
		assertEquals(FB_DIFFICUITY, feedback.getDifficulty_score());

		/**
		 * Delete test row
		 */
		query = this.getSession().createQuery("DELETE FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FB_COMMMENT);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		
		
		/**
		 * Check whether test row exist in database
		 */
		query = this.getSession().createQuery("FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FB_COMMMENT);
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		
		assertTrue(query.list().isEmpty());

		
	}
}
