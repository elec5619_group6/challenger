package au.usyd.elec5619.domain;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.dao.UserDAO;
import au.usyd.elec5619.dao.UserDAOImpl;
import au.usyd.elec5619.service.UserManager;
import au.usyd.elec5619.service.UserManagerImpl;

public class UserTest extends BaseConfiguration {
	
	private User user;
	
	private static final String TEST_EMAIL = "mary.email@test.com";
	private static final String TEST_PASSWORD = "pass123";
	private static final String TEST_USERNAME = "whatACoolUserName";
	private static final String TEST_FIRSTNAME = "Mary";
	private static final String TEST_PASSWORD_1 = "pass2";
	private static final String TEST_USERNAME_1 = "anotherCoolUserName";
	
	@Before
	public void setUp() throws Exception {
		// Delete user if user exists
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", TEST_EMAIL);
		if (!query.list().isEmpty()) {
			user = (User) query.list().get(0);
			this.getSession().delete(user);
		}
		
		// Create new User
		user = new User();
		user.setEmail(TEST_EMAIL);
		user.setPassword(TEST_PASSWORD);
		user.setUserName(TEST_USERNAME);

	}

	@After
	public void tearDown() throws Exception {
		// Delete user if user exists
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", TEST_EMAIL);
		if (!query.list().isEmpty()) {
			user = (User) query.list().get(0);
			this.getSession().delete(user);
		}
	}
		
	@Test
	public void persistUser() {
		
		User user = new User(TEST_EMAIL, TEST_PASSWORD, TEST_USERNAME);
		
		this.getSession().save(user);
		
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", TEST_EMAIL);
		if (query.list().isEmpty()) {
			assertNotNull("user list is empty", null);
			return;
		}
		user = (User) query.list().get(0);
		assertEquals(TEST_EMAIL, user.getEmail());
		assertEquals(TEST_PASSWORD, user.getPassword());
		assertEquals(TEST_USERNAME, user.getUserName());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", TEST_EMAIL);
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", TEST_EMAIL);
		
		assertTrue(query.list().isEmpty());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUser() {
		List<User> users;
		users = this.getSession().createQuery("FROM " + User.class.getName()).list();
		assertNotNull("user list is null.", users);
	}

	public UserTest() {
		super();
	}

}
