package au.usyd.elec5619.web;

import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.ChallengeManager;
import au.usyd.elec5619.service.TagService;
import au.usyd.elec5619.service.UserManager;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@Resource(name="challengeManager")
	private ChallengeManager challengeManager;

	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = {"/", "/index", "/home"}, method = RequestMethod.GET)
	public String home(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null != user) {
			logger.info("homepage: already logged in: " + user.getEmail());
			logger.info("homepage: already logged in: " + user.getUserId());
			model.addAttribute("user", user);
		}
		
		ArrayList<Challenge> challengeList = challengeManager.getChallengeList();
		model.addAttribute("challengelist", challengeList);
		return "home";
	}

}
