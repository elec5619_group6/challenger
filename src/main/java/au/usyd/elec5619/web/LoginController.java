package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.AchievementAttemptManager;
import au.usyd.elec5619.service.Login;
import au.usyd.elec5619.service.TagService;
import au.usyd.elec5619.service.UserManager;

@Controller

public class LoginController {

	protected final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@Resource(name="tagService")
	TagService tagService;
	
	@Resource(name="achAmptManager")
	private AchievementAttemptManager achAmptManager;
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String showLoginForm(Model model, HttpSession session) {
		User currentUser = (User) session.getAttribute("user");
		if (null != currentUser)
			return "redirect:/home";
		model.addAttribute("userLogin", new Login());
		return "login";
	}
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String onSubmit(@Valid @ModelAttribute("userLogin") Login login, BindingResult bindingResult, HttpSession session,
			RedirectAttributes redirectAttrs) {
		User currentUser = (User) session.getAttribute("user");
		if (null != currentUser)
			return "redirect:/home";
		
		String email = login.getEmail();
		boolean hasUser = userManager.hasUser(email);
		
		// Email is not registered
		if(!bindingResult.hasFieldErrors("email") && !hasUser) {
            bindingResult.rejectValue("email", "EmailExist.login.email");
        }
		
		logger.info("getting user with email " + email);
		User returnedUser = userManager.getUser(email, login.getPassword());
        
		// Email and password do not match
		if (!bindingResult.hasFieldErrors() && returnedUser == null) {
        	bindingResult.rejectValue("email", "LoginMatch.login");
        }
        logger.info("got user with email and matching password " + email);
		
		if (bindingResult.hasErrors()) {
			for (ObjectError err : bindingResult.getAllErrors()) {
				logger.info("login error: Obejct: " + err.getObjectName() + " Message: " + err.getDefaultMessage());
			}
			return "login";
		}
		
		long userId = returnedUser.getUserId();
		logger.info("user has email " + returnedUser.getEmail());
		logger.info("user has id " + userId);
		session.setAttribute("user", returnedUser);
		
		/* update user statistics */
		if (achAmptManager.updateUserLoginRecord(userId)) {
			redirectAttrs.addFlashAttribute("msg", "New Achievement Unlocked! ");
		}
		
		if (null == session.getAttribute("userTags")) {
			logger.info("Adding user tags to the session. ");
			tagService.setUserTags(tagService.getTagsByUserId(userId));
			session.setAttribute("userTags", tagService.getUserTags());
		}
		return "redirect:/home";
		
	}
	
	@RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
	public String logout(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null != user) {
			session.removeAttribute("user");
			session.removeAttribute("userTags");
		}
			
		return "redirect:/";
	}

}
