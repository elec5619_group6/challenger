package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class AchievementAttemptTest extends BaseConfiguration {
	
	@Test
	public void persistAchievementAttempt() {
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Achievement achievement = new Achievement(ACH_TITLE, ACH_DESCRIPTION, ACH_QUANTITY, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		query = this.getSession().createQuery("FROM " + Achievement.class.getName() + " WHERE title = :title");
		query.setParameter("title", ACH_TITLE);
		achievement = (Achievement) query.list().get(0);
		
		AchievementAttempt ach_attempt = new AchievementAttempt(user.getUserId(), achievement.getAchId());
		this.getSession().save(ach_attempt);
		query = this.getSession().createQuery("FROM " + AchievementAttempt.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		if (query.list().isEmpty()) {
			assertNotNull("achievement attempt list is empty", null);
			return;
		}
		ach_attempt = (AchievementAttempt) query.list().get(0);
		assertEquals(user.getUserId(), ach_attempt.getUserId());
		assertEquals(achievement.getAchId(), ach_attempt.getAchId());
		
		query = this.getSession().createQuery("DELETE FROM " + AchievementAttempt.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + AchievementAttempt.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Achievement.class.getName() + " WHERE title = :title");
		query.setParameter("title", ACH_TITLE);
		query.executeUpdate();
	}
	
	/*@Test
	public void addNewAchievement() {
		Achievement achievement = new Achievement("First one done!", "Complete a challenge", 1, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("3 in a row!", "Complete 3 challenges", 3, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("Want more!", "Complete 5 challenges", 5, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("Can't stop!", "Complete 10 challenges", 10, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		
		achievement = new Achievement("First time", "First time login", 1, AchievementType.LOGIN_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("Coming back", "Login 3 times", 3, AchievementType.LOGIN_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("All week", "Login 7 times", 7, AchievementType.LOGIN_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("Heavy user", "Login 14 times", 14, AchievementType.LOGIN_NUMBER);
		this.getSession().save(achievement);
		
		achievement = new Achievement("Decide on my own", "Create a challenge", 1, AchievementType.CREATE_CHG_NUMBER);
		this.getSession().save(achievement);
		achievement = new Achievement("Challenge master", "Create 5 challenges", 5, AchievementType.CREATE_CHG_NUMBER);
		this.getSession().save(achievement);
	}*/
}
