package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Achievement template belongs to no one
 * @author John
 * 
 */
@Entity
@Table(name="challenger.achievement")
public class Achievement implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="achievement_id")
	private long achId;
	
	@Column(name="title")
    private String title;
	
	@Column(name="description")
    private String description;
	
	@Column(name="quantity")
    private int quantity;
	
	@Column(name="type")
    private AchievementType category;
	
	/* Override functions */
	

	/* Constructor */
	public Achievement() {}
	
	public Achievement(String title, String description, int quantity,
			AchievementType category) {
		super();
		this.title = title;
		this.description = description;
		this.quantity = quantity;
		this.category = category;
	}

	/* Getter & Setter */
	public long getAchId() {
		return achId;
	}

	public void setAchId(long achId) {
		this.achId = achId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public AchievementType getCategory() {
		return category;
	}

	public void setCategory(AchievementType category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@Override
	public boolean equals(Object obj) {
		return achId == ((Achievement)obj).getAchId();
	}
}
