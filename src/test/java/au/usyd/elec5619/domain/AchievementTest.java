package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class AchievementTest extends BaseConfiguration {
	
	@Test
	public void persistAchievement() {
		Achievement achievement = new Achievement(ACH_TITLE, ACH_DESCRIPTION, ACH_QUANTITY, AchievementType.CHG_NUMBER);
		this.getSession().save(achievement);
		Query query = this.getSession().createQuery("FROM " + Achievement.class.getName() + " WHERE title = :title");
		query.setParameter("title", ACH_TITLE);
		if (query.list().isEmpty()) {
			assertNotNull("achievement list is empty", null);
			return;
		}
		achievement = (Achievement) query.list().get(0);
		assertEquals(ACH_DESCRIPTION, achievement.getDescription());
		assertEquals(ACH_QUANTITY, achievement.getQuantity());
		
		query = this.getSession().createQuery("DELETE FROM " + Achievement.class.getName() + " WHERE title = :title");
		query.setParameter("title", ACH_TITLE);
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + Achievement.class.getName() + " WHERE title = :title");
		query.setParameter("title", ACH_TITLE);
		
		assertTrue(query.list().isEmpty());
	}
}
