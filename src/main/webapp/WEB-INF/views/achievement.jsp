<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<head>
<title>Achievement</title>
</head>
</head>
<body>
	<div class="container">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-danger">
				<div class="panel-heading" align="center">
					<h4>Achievements</h4>
				</div>
				<div class="panel-body">
					<c:forEach items="${display}" var="display">
						<div class="list-group">
							<h4 class="list-group-item-heading">${display.title}</h4>
							<p class="list-group-item-text">${display.description}</p>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
									aria-valuenow="${display.progress}" aria-valuemin="0"
									aria-valuemax="${display.quantity}"
									style="width: ${display.percentage}%;">
									${display.progress}/${display.quantity}</div>
							</div>
						</div>

						<br />
						<br />
					</c:forEach>
				</div>
				<!-- <div class="panel-footer">Not used.</div> -->
			</div>
		</div>
		<!--/span-->
	</div>
</body>
</html>