package au.usyd.elec5619.service;

import org.hibernate.Query;
import org.junit.Before;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.UserChallenge;

public class ChallengeManagerTest extends BaseConfiguration {

	private ChallengeManager challengeManager;
	private String CHALLENGE_TITLE = "test";
	private String CHALLENGE_CONTENT = "test_content";
	private String CHALLENGE_CREATOR = "test_creator";
	private int CHALLENGE_DURATION = 5;
	private Challenge challenge;
	
	@Before
	public void setUp() throws Exception
	{
		challengeManager = new ChallengeManager();
		challenge = new Challenge(CHALLENGE_TITLE,CHALLENGE_CONTENT,CHALLENGE_CREATOR,CHALLENGE_DURATION);
		Query query = (Query) this.getSession().save(challenge);
	}
}
