//package au.usyd.elec5619.service;
//
//
//import java.util.List;
//
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
//
//import au.usyd.elec5619.domain.Challenge;
//import au.usyd.elec5619.domain.Feedback;
//
///*
// * Challenge Manager
// */
//
//@Repository
//public class CurrentChallenge {
//
//	@Autowired
//	private SessionFactory sessionFactory;
//	
//	@Transactional
//	public Challenge getChallengeById(long challengeId)
//	{
//		Session currentSession = sessionFactory.getCurrentSession();
//		Challenge challenge = (Challenge) currentSession.get(Challenge.class,challengeId);		
//		return challenge;
//	}
//	
//	public CurrentChallenge() {
//		super();
//	}
//	
//}