package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class RelationshipTest extends BaseConfiguration {

	@Test
	public void persistRelationship()
	{
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		User friend = new User(EMAIL_1, PASSWORD_1, USERNAME_1);
		this.getSession().save(friend);
		Query query1 = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query1.setParameter("email", EMAIL_1);
		friend = (User) query1.list().get(0);
		
		Relationship relationship = new Relationship(user.getUserId(),friend.getUserId());
		this.getSession().save(relationship);
		query = this.getSession().createQuery("FROM " + Relationship.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		if (query.list().isEmpty()) {
			assertNotNull("Relationship list is empty", null);
			return;
		}
		relationship = (Relationship) query.list().get(0);
		assertEquals(user.getUserId(), relationship.getUserId());
		assertEquals(friend.getUserId(), relationship.getFriendId());
		
		query = this.getSession().createQuery("DELETE FROM " + Relationship.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + Relationship.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query1 = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query1.setParameter("email", EMAIL_1);
		query1.executeUpdate();
		
	}
}
