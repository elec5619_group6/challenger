package au.usyd.elec5619.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Comment;
import au.usyd.elec5619.domain.FeedbackComment;
import au.usyd.elec5619.domain.FeedbackCommentForDisplay;

@Repository
public class FeedbackCommentMananger
{
	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<FeedbackCommentForDisplay> commDisplay;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public void getFeedbackCommentList() {
		Session session = sessionFactory.getCurrentSession();
		Connection conn = session.connection();
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("SELECT * FROM challenger.feedback_comment NATURAL JOIN challenger.user");
			ResultSet resultSet = stmt.executeQuery();
			commDisplay.clear();
			
			while (resultSet.next()) {	
				System.out.println(resultSet.getString("user_name")
						+ " - " + resultSet.getString("comment")+"fffffffffffffffffffffffffffffffffffffffff");
				commDisplay.add(new FeedbackCommentForDisplay(
						resultSet.getString("user_name"),
						resultSet.getString("comment"),
						resultSet.getLong("feedback_id"),
						resultSet.getDate("datetime")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	@Transactional
	public ArrayList<Comment> getComment()
	{
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM Feedback_Comment");
		ArrayList<Comment> comment = (ArrayList<Comment>)query.list();
		return comment;
	}
	@Transactional
	public void addComment(FeedbackComment comment)
	{
		this.sessionFactory.getCurrentSession().save(comment);
	}
	public FeedbackCommentMananger()
	{
		this.commDisplay = new ArrayList<FeedbackCommentForDisplay>();
	}
	public ArrayList<FeedbackCommentForDisplay> getCommDisplay() {
		return commDisplay;
	}
}