<%@ include file="/WEB-INF/views/head.jsp"%>

<html lang="en">
<head>
<title>User Profile</title>
<link
	href="${pageContext.request.contextPath}/resources/css/dashboard.css"
	rel="stylesheet">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.css" />
<script
	src="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.js"></script>
	
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-5 col-xs-offset-7 col-sm-3 col-sm-offset-9 col-md-2 sidebar col-md-offset-10">
				<ul class="nav nav-sidebar">
					<div class="list-group">
						<div class="media">
							<a class="pull-left" href="#"> <img class="media-object"
								src="" alt="">
							</a>
							<div class="media-body">
								<h4 class="media-heading">
									<span class="glyphicon glyphicon-user"></span>
									<c:out value='${user.userName }' />
								</h4>
								<a href="${pageContext.request.contextPath}/profile">View Profile</a>
							</div>
						</div>
					</div>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="list-group">
							<a href="<c:out value='${pageContext.request.contextPath}/user_home'/>" class="list-group-item">
								<span class="glyphicon glyphicon-home"> </span> Activity
							</a> <a href="<c:out value="${pageContext.request.contextPath}/challenges"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-send"> </span> Challenges
							</a><a href="<c:out value="${pageContext.request.contextPath}/statistics"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-list"></span> Statistics
							</a> <a href="<c:out value="${pageContext.request.contextPath}/barchart"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-signal"></span> Graphs
							</a> <a href="<c:out value="${pageContext.request.contextPath}/achievements"/>" class="list-group-item"> <span
								class="glyphicon glyphicon-thumbs-up"></span> Achievements
							</a>
						</div></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Friends</h3>
							</div>
							<ul class="list-group">
								<c:forEach items="${friends}" var="friend">
								<li class="list-group-item">
									<a href="${friend.userId}" class="list-group-item"> 
										<span>
											<p class="list-group-item-heading">${friend.userName}</p>
										</span>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div></li>
				</ul>
			</div>
			<div class="col-xs-7 col-sm-9 col-md-10 main">
				<div class="container">
					<div class="col-xs-7 col-sm-9 col-md-10">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<c:if test="${user != null && user.userId == target.userId }">
								<a href="${pageContext.request.contextPath}/profile/edit"><button type="button"
										class="btn btn-default btn-xs pull-right">
										<span class="glyphicon glyphicon-edit"></span> Edit
									</button></a></c:if>
								<h1 class="panel-title">${target.userName}'s Profile</h1>
							</div>

							<!-- Table of User Data -->
							<table class="table">
								<tr>
									<td align="right">Email:</td>
									<td>${user.email}</td>
								</tr>
								<c:if test="${user != null && target.userId == user.userId }" >
								<tr>
									<td align="right">Password:</td>
									<td>********</td>
								</tr>
								</c:if>
								<tr>
									<td align="right">Username:</td>
									<td>${target.userName}</td>
								</tr>
								<tr>
									<td align="right">First Name:</td>
									<td>${target.firstName}</td>
								</tr>
								<tr>
									<td align="right">Last Name:</td>
									<td>${target.lastName}</td>
								</tr>
								<tr>
									<td align="right">Phone:</td>
									<td>${target.phone}</td>
								</tr>
								<tr>
									<td align="right">Gender:</td>
									<td><c:choose>
									<c:when test="${target.gender}" >Male</c:when>
									<c:otherwise>Female</c:otherwise></c:choose></td>
								</tr>
								<tr>
									<td align="right">Date of Birth:</td>
									<td>${target.dob}</td>
								</tr>
							</table>
						</div>
						
						<div class="panel panel-primary">
							<div class="panel-heading">
								<c:if test="${user != null && user.userId == target.userId }">
								<a href="${pageContext.request.contextPath}/profile/edit_tags"><button type="button"
										class="btn btn-default btn-xs pull-right">
										<span class="glyphicon glyphicon-edit"></span> Edit
									</button></a></c:if>
								<h1 class="panel-title">${target.userName}'s Interests</h1>

							</div>

							<!-- Table of Tag Data -->
							<table class="table">
								<tr>
									<td align="right">Interests:</td>
									<td>
										<input type="text" name="tags" class="col-sm-5 form-control"
											value="${tags}" data-role="tagsinput" />
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<!-- /.container -->
			</div>
		</div>
	</div>
	<div class="fb-like" data-share="true" data-width="450"
		data-show-faces="true"></div>
	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../assets/js/docs.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>