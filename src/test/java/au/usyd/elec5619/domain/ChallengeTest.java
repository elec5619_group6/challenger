package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class ChallengeTest extends BaseConfiguration {

	@Test
	public void persistChallenge() {
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		Query query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		if (query.list().isEmpty()) {
			assertNotNull("challenge list is empty", null);
			return;
		}
		challenge = (Challenge) query.list().get(0);
		assertEquals(CHG_TITLE, challenge.getTitle());
		assertEquals(CHG_DESCRIPTION, challenge.getDescription());
		assertEquals(USERNAME, challenge.getCreator());
		assertEquals(CHG_DURATION, challenge.getDuration());
		assertEquals(CHG_POSTTIME, challenge.getPostTime());
		
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		
		assertTrue(query.list().isEmpty());
	}
}
