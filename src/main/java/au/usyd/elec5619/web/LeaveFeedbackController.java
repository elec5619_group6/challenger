package au.usyd.elec5619.web;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;

import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.FeedbackForDisplay;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.*;

@Controller
public class LeaveFeedbackController{
	
	@Resource(name="currentUser")
	private CurrentUser currentUser;
	
	@Resource(name="feedbackManager")
	private FeedbackManager feedbackmanager;
	
	@Resource(name="challengeManager")
	private ChallengeManager challengemanager;
	
	@RequestMapping(value = "/leavefeedback/{cid}", method = RequestMethod.GET)
	public String home(@PathVariable("cid") long cid,Locale locale, Model model, HttpSession session) {
		
		User user = (User) session.getAttribute("user");//currentUser.getUserById(6);
		if (null == user)
			return "redirect:/";
		Challenge challenge = challengemanager.getChallengeById(cid);
		model.addAttribute("user", user );
		model.addAttribute("challenge",challenge);
     	return "LeaveFeedback";
	}
	
	@RequestMapping(value="leavefeedback/{challengeid}&{userid}", method=RequestMethod.POST)
	public String addFeedback(@PathVariable("challengeid") long challengeId,
			@PathVariable("userid") long userId, HttpServletRequest httpServletRequest) {
		//User user = (User) httpServletRequest.getAttribute("user");
		//if (null == user)
			//return "redirect:/";
		
		Feedback feedback = new Feedback();
		feedback.setUser_id(userId);
		feedback.setChallenge_id(challengeId);
		//here waiting for rating JavaScript
		//feedback.setInterest_score(3);
		//feedback.setDifficulty_score(5);
		feedback.setDatetime(new Date());
		feedback.setInterest_score(Integer.valueOf(httpServletRequest.getParameter("a")));
		feedback.setDifficulty_score(Integer.valueOf(httpServletRequest.getParameter("b")));
		feedback.setDescription(String.valueOf(httpServletRequest.getParameter("textarea")));
		feedback.setVideoLink(String.valueOf(httpServletRequest.getParameter("photopath")));
		
		System.out.println(System.currentTimeMillis()+"-------------------------------");
	//	comment.setCommentTime(Long.valueOf(System.currentTimeMillis()));
		//this.commentManager.addComment(comment);
		this.feedbackmanager.addFeedback(feedback);
		return "LeaveFeedbackSuccessful";
	}
}