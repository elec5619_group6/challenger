package au.usyd.elec5619.domain;

import java.util.Date;

public class AchievementDisplay {

	private long achievement_id;
	private String title;
	private String description;
	
	private double percentage;
	private int progress;
	private int quantity;
	private int type;
	private int state;
	private Date completed_time;
	
	private long attempt_id;
	private long user_id;
	
	@Override
	public boolean equals(Object obj) {
		return this.achievement_id == ((AchievementDisplay)obj).getAchievement_id();
	}


	public AchievementDisplay(long achievement_id, String title,
			String description, double percentage, int progress, int quantity,
			int type, int state, Date completed_time) {
		super();
		this.achievement_id = achievement_id;
		this.title = title;
		this.description = description;
		this.percentage = percentage;
		this.progress = progress;
		this.quantity = quantity;
		this.type = type;
		this.state = state;
		this.completed_time = completed_time;
	}
	
	
	public long getAchievement_id() {
		return achievement_id;
	}
	public void setAchievement_id(long achievement_id) {
		this.achievement_id = achievement_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Date getCompleted_time() {
		return completed_time;
	}
	public void setCompleted_time(Date completed_time) {
		this.completed_time = completed_time;
	}
	public long getAttempId() {
		return attempt_id;
	}
	public void setAttempId(long attempId) {
		this.attempt_id = attempId;
	}
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	
}
