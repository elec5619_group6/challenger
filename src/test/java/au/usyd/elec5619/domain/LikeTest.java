package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class LikeTest extends BaseConfiguration {

	@Test
	public void persistLike()
	{
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		UserChallenge userChallenge = new UserChallenge(USER_CHALLENGE_ID,123,0,0);
		//System.out.println(userChallenge.getUserChallengeId()+"11111111111111111111111111111111111111111222222");
		this.getSession().save(userChallenge);
		query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() + " WHERE time_end =:userChallengeId");
		query.setParameter("userChallengeId", 123);
		userChallenge = (UserChallenge) query.list().get(0);
		
		Like like = new Like(userChallenge.getUserChallengeId(),user.getUserId());
		this.getSession().save(like);
		query = this.getSession().createQuery("FROM " + Like.class.getName() + " WHERE userchallenge_id = :userChallengeId");
		query.setParameter("userChallengeId", userChallenge.getUserChallengeId());
		if(query.list().isEmpty())
		{
			assertNotNull("Like list is empty",null);
			return;
		}
		like = (Like) query.list().get(0);
		assertEquals(userChallenge.getUserChallengeId(),like.getUserChallengeId());
		assertEquals(user.getUserId(),like.getUserId());
		
		query = this.getSession().createQuery("DELETE FROM " + Like.class.getName() + " WHERE userchallenge_id = :userchallenge_id");
		query.setParameter("userchallenge_id", userChallenge.getUserChallengeId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + Like.class.getName() + " WHERE userchallenge_id = :userchallenge_id");
		query.setParameter("userchallenge_id", userChallenge.getUserChallengeId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + UserChallenge.class.getName() + " WHERE userchallengeId = :userChallengeId");
		query.setParameter("userChallengeId", USER_CHALLENGE_ID);
		query.executeUpdate();
		
	}
}
