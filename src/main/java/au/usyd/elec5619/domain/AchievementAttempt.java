package au.usyd.elec5619.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Log for each achievement in each user
 * @author John
 *
 */
@Entity
@Table(name="challenger.achievement_attempt")
public class AchievementAttempt {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="attempt_id", nullable=false)
	private long attempId;
	
	@Column(name="user_id")
    private long userId;
	
	@Column(name="achievement_id")
	private long achId;
	
	@Column(name="state")
    private TaskState state = TaskState.INACTIVE;
	
	@Column(name="completed_time")
    private Date completedTime = null;
	
	@Column(name="progress")
    private int progress = 0;
	
	@Column(name="percentage")
    private double percentage = 0;

	/* Functions */
	@Override
	public boolean equals(Object obj) {
		return attempId == ((AchievementAttempt)obj).getAttempId();
	}

	/* Constructor */
	public AchievementAttempt() {}
	
	public AchievementAttempt(long userId, long achId) {
		super();
		this.userId = userId;
		this.achId = achId;
		this.setCompletedTime(new Date());
	}
	
	/* Getter & setter */
	public long getAttempId() {
		return attempId;
	}

	public void setAttempId(long attempId) {
		this.attempId = attempId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getAchId() {
		return achId;
	}

	public void setAchId(long achId) {
		this.achId = achId;
	}

	public TaskState getState() {
		return state;
	}

	public void setState(TaskState state) {
		this.state = state;
	}

	public Date getCompletedTime() {
		return completedTime;
	}

	public void setCompletedTime(Date completedTime) {
		this.completedTime = completedTime;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}
	
	public void incProgress() {
		this.progress++;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	

}
