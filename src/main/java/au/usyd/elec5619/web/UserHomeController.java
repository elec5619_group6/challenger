package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.AchievementDisplay;
import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.ChallengeDisplay;
import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.AchievementAttemptManager;
import au.usyd.elec5619.service.ChallengeManager;
import au.usyd.elec5619.service.CurrentUser;
import au.usyd.elec5619.service.FriendManager;
import au.usyd.elec5619.service.ProfileManager;
import au.usyd.elec5619.service.TagService;
import au.usyd.elec5619.service.UserManager;

@Controller
public class UserHomeController {

	
	private static final Logger logger = LoggerFactory.getLogger(UserHomeController.class);
	
	@Resource(name="userManager")
	UserManager userManager;
	
	@Resource(name="tagService")
	TagService tagService;

	@Resource(name="currentUser")
	CurrentUser currentUser;
	
	@Resource(name="profileManager")
	ProfileManager profileManager;
	
	@Resource(name="achAmptManager")
	private AchievementAttemptManager achAmptManager;
	
	@Resource(name="friendManager")
	private FriendManager friendManager;
	
	@RequestMapping(value = {"/user_home"}, method = RequestMethod.GET)
	public String homeUser(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		long userId = user.getUserId();
		
		return "redirect:/user_home/" + userId;
	}

	
	@RequestMapping(value = {"/user_home/{uid}"}, method = RequestMethod.GET)
	public String homeUserWithId(@PathVariable("uid") long userId, Model model, HttpSession session) {
		User self = (User) session.getAttribute("user");
		if (null == self) {
			return "redirect:/login";
//		} else if (self.getUserId() != userId) {
//			return "redirect:/user_home/" + userId;
		}
		User target = userManager.getUser(userId);
		
		logger.info("user info: " + self.getEmail());
		logger.info("user info: " + self.getUserId());
		model.addAttribute("user", self);
		
		logger.info("user info: " + target.getEmail());
		logger.info("user info: " + target.getUserId());
		model.addAttribute("target", target);
		
		// Display Challenges
		List<ChallengeDisplay> challengeList = currentUser.getUserChallenges(target.getUserId());
		model.addAttribute("challengelist", challengeList.subList(0, 5 > challengeList.size() ? challengeList.size() : 5));
		
		// Display Achievements
		profileManager.createUserAchievementList(target);
		achAmptManager.getAchievementList(target.getUserId());
		List<AchievementDisplay> achieves = achAmptManager.getAchDisplay();
		model.addAttribute("achieves", achieves.subList(0, 5 > achieves.size() ? achieves.size() : 5));
		
		// Display Statistics
		achAmptManager.getUserStatistics(target.getUserId());	
		model.addAttribute("statistics", achAmptManager.getUserStat());
		
		// Display Logged User's Friends
		friendManager.getFriends(self.getUserId());
		List<Long> friendsListStrings = friendManager.getFriends(self.getUserId());
		List<User> friendsList = new ArrayList<User>();
		for (Long fid : friendsListStrings) {
			friendsList.add(userManager.getUser(fid));
		}
		model.addAttribute("friends", friendsList);
		
		// Display Target User Friends
		friendManager.getFriends(target.getUserId());
		List<Long> targetFriendsListStrings = friendManager.getFriends(target.getUserId());
		List<User> targetFriendsList = new ArrayList<User>();
		for (Long fid : targetFriendsListStrings) {
			targetFriendsList.add(userManager.getUser(fid));
		}
		model.addAttribute("targetFriends", targetFriendsList);
		
		return "user_home";
	}
}
