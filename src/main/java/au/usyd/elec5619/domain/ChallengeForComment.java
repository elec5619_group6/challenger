package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

/*
 * 
 */

public class ChallengeForComment implements Serializable{


	private long userId;
	private long challengeId;
	private String title;
	private String description;
	private String creator;
	private String duration;
	private long post_time;
	private double rating;
	private double difficulty;
	private int picture_id;
	private long userChallengeId;
	private int like;
	private boolean liked;
	private boolean attemped;
	
	
	public int getLike() {
		return like;
	}

	public void setLike(int like) {
		this.like = like;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public long getPost_time() {
		return post_time;
	}

	public void setPost_time(long post_time) {
		this.post_time = post_time;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public double getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(double difficulty) {
		this.difficulty = difficulty;
	}

	public int getPicture_id() {
		return picture_id;
	}

	public void setPicture_id(int picture_id) {
		this.picture_id = picture_id;
	}
	
	public ChallengeForComment() {}
	
	public ChallengeForComment(Challenge challenge,long userId,long userChallengeId,int like,boolean liked,boolean attemped) 
	{
		this.userId = userId;
		this.like = like;
		this.setUserChallengeId(userChallengeId);
		challengeId = challenge.getChaId();
		title = challenge.getTitle();
		description = challenge.getDescription();
		creator = challenge.getCreator();
		rating = challenge.getRating();
		difficulty = challenge.getDifficulty();
		this.setLiked(liked);
		this.attemped = attemped;
	}

	public long getUserChallengeId() {
		return userChallengeId;
	}

	public void setUserChallengeId(long userChallengeId) {
		this.userChallengeId = userChallengeId;
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	public boolean isAttemped() {
		return attemped;
	}

	public void setAttemped(boolean attemped) {
		this.attemped = attemped;
	}
}