package au.usyd.elec5619.base;

import java.util.Date;
import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(defaultRollback = true)
@Transactional
@ContextConfiguration(locations = { "classpath:persistence-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class})
public class BaseConfiguration extends TestCase {
	
	/* add common test data here */
	protected static final long TIME = 1;
	
	protected static final String EMAIL = "abcd1234@uni.sydney.edu.au";
	protected static final String PASSWORD = "password";
	protected static final String USERNAME = "John";
	
	protected static final String EMAIL_1 = "123@123234.com";
	protected static final String PASSWORD_1 = "password";
	protected static final String USERNAME_1 = "Wang";
	
	protected static final String CHG_TITLE = "Cook a meal";
	protected static final String CHG_DESCRIPTION = "Cood a meal by yourself.";
	protected static final int CHG_DURATION = 3;
	protected static final Date CHG_POSTTIME = new Date(TIME);
	protected static final double CHG_RATING = 1.1;
	protected static final double CHG_DIFFICULTY = 2.3;
	protected static final long CHG_PICID = 1;

	protected static final String ACH_TITLE = "Finish 10 challenges";
	protected static final String ACH_DESCRIPTION = "finish 10 challenges in total";
	protected static final int ACH_QUANTITY = 10;
	protected static final Date ACH_CPLTTIME = new Date(TIME);
	
	protected static final String TAGNAME = "Sports";
	
	protected static final Date USER_LOGIN = new Date(TIME);
	
	protected static final long USER_CHALLENGE_ID = 9;
	protected static final long COMMENT_ID = 9;
	
	protected static final String FB_COMMMENT = "test feedback";
	protected static final int FB_INTEREST = 2;
	protected static final int FB_DIFFICUITY = 2;
	
	
	
	@Autowired
	private SessionFactory sessionFactory = buildSessionFactory();;
	
	private Session session;
	
	
	private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return new Configuration().configure().buildSessionFactory();
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    @Before
    public void setup() {
    	try {
			session = sessionFactory.openSession();
		}
		catch(Exception e) {
			//Logging
		}
    }
    
    @After
    public void closeSession() {
    	if(session !=null && session.isOpen()) {
			session.close();
			session=null;
		}
    }

	public Session getSession() {
		return session;
	}
	
}
