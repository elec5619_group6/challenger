package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




import au.usyd.elec5619.domain.Comment;

@Repository
public class CommentManager {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public ArrayList<Comment> getCommentByChallengeId(long userChallengeId)
	{
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM Comment where userchallenge_id =:id");
		query.setLong("id", userChallengeId);
		ArrayList<Comment> comment = (ArrayList<Comment>)query.list();
		return comment;
	}
	
	@Transactional
	public void addComment(Comment comment)
	{
		this.sessionFactory.getCurrentSession().save(comment);
	}
	
}
