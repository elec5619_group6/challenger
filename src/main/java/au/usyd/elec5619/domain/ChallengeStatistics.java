package au.usyd.elec5619.domain;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChallengeStatistics {

	private int count;
	private Date startTime;
	private Date finishTime;
	
	public ChallengeStatistics() {
		super();
	}
	
	public ChallengeStatistics(Date startTime, Date finishTime) {
		super();
		this.count = 0;
		this.startTime = startTime;
		this.finishTime = finishTime;
	}

	public Date getStartTime() {
		return startTime;
	}
	
	public Date getStartDay() {
		Calendar cal = Calendar.getInstance();
		
        cal.setTime(startTime);
        cal.set(Calendar.HOUR_OF_DAY, 10);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}
	
	public Date getFinishDay() {
		Calendar cal = Calendar.getInstance();
		
        cal.setTime(finishTime);
        cal.set(Calendar.HOUR_OF_DAY, 10);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        
        return cal.getTime();
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	
	public boolean isSameStartDate(Date start) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		return fmt.format(this.startTime).equals(fmt.format(start));
	}
	
	public boolean isSameFinishDay(Date finish) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		return fmt.format(this.finishTime).equals(fmt.format(finish));
	}

	public int getCount() {
		return count;
	}
	
	public void increaseCount() {
		this.count++;
	}
}
