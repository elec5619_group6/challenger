package au.usyd.elec5619.web;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;

import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.FeedbackComment;
import au.usyd.elec5619.domain.FeedbackForDisplay;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.*;

@Controller
public class ShowFeedbackController{
	
	@Resource(name="currentUser")
	private CurrentUser currentUser;
	@Resource(name="feedbackManager")
	private FeedbackManager feedbackmanager;
	@Resource(name="challengeManager")
	private ChallengeManager challengemanager;
	@Resource(name="feedbackCommentManager")
	private FeedbackCommentMananger feedbackcommentmanager;
	
	@RequestMapping(value = "/showfeedback", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		List<Feedback> fblist = feedbackmanager.findAll();
		feedbackcommentmanager.getFeedbackCommentList();
		ArrayList<FeedbackForDisplay> feedbackd = new ArrayList<FeedbackForDisplay>(); 
		for(Feedback f : fblist)
		{
			feedbackd.add(new FeedbackForDisplay(f,challengemanager.findRelatedChallenge(f.getChallenge_id()),currentUser.getUserById(f.getUser_id())));
		}
		
		model.addAttribute("comment",feedbackcommentmanager.getCommDisplay());		
		
		model.addAttribute("User", user );
     	model.addAttribute("feedbacks",feedbackd);	
   

     	return "ShowFeedback";
	}
	@RequestMapping(value="/showfeedback/{feedbackid}&{userid}", method=RequestMethod.POST)
	public String addComment(@PathVariable("feedbackid") long feedbackId,
			@PathVariable("userid") long userId, HttpServletRequest httpServletRequest) {
	
		
		FeedbackComment feedbackcomment = new FeedbackComment();
		feedbackcomment.setFeedback_id(feedbackId);
		feedbackcomment.setUser_id(userId);
		feedbackcomment.setComment(String.valueOf(httpServletRequest.getParameter("comment")));
		feedbackcomment.setDate(new Date());
		
		this.feedbackcommentmanager.addComment(feedbackcomment);
		
		return "redirect:../showfeedback";
	}
	@RequestMapping(value="/challengedetail/showfeedback/{feedbackid}&{userid}&{chid}", method=RequestMethod.POST)
	public String addComment2(@PathVariable("feedbackid") long feedbackId,
			@PathVariable("userid") long userId,@PathVariable("chid") long challengeId, HttpServletRequest httpServletRequest) {
	
		
		FeedbackComment feedbackcomment = new FeedbackComment();
		feedbackcomment.setFeedback_id(feedbackId);
		feedbackcomment.setUser_id(userId);
		feedbackcomment.setComment(String.valueOf(httpServletRequest.getParameter("comment")));
		feedbackcomment.setDate(new Date());
		
		this.feedbackcommentmanager.addComment(feedbackcomment);
		
		//return "redirect:/challengedetail/" + challenge.getChaId();
		return "redirect:/challengedetail/"+challengeId;
	}
//******************************************************************************
//public class ShowFeedbackController implements Controller {
//protected final Log logger = LogFactory.getLog(getClass());
//private FeedbackManager feedbackmanager;
//
//@Override
//public ModelAndView handleRequest(HttpServletRequest arg0,
//HttpServletResponse arg1) throws Exception {
//String now = (new Date()).toString();
//logger.info("Returning hello view with " + now);
//
//try {
//Map<String,Object> myModel = new HashMap<String,Object>();
//myModel.put("now",now);
//myModel.put("products",this.feedbackmanager.getFeedbacks() );
//}
//catch (Exception e) {e.printStackTrace(System.err);}
//
//
////return new ModelAndView("ShowFeedback", "model", myModel);
//return new ModelAndView("ShowFeedback", "now", now);
//}
//public void setFeedbackManager(FeedbackManager fb){
//	this.feedbackmanager=fb;
//}
}