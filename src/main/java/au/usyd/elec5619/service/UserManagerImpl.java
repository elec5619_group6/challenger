package au.usyd.elec5619.service;

import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.UserDAO;
import au.usyd.elec5619.domain.User;

@Service("userManager")
@Transactional
public class UserManagerImpl implements UserManager {
	
	@Autowired
	UserDAO userDAO;
	
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name="profileManager")
	ProfileManager profileManager;
	
	@Override
	public void addUser(User user) {
		userDAO.addUser(user);
	}
	
	@Override
	public void addUser(String email, String password) {
		userDAO.addUser(email, password);
	}
	
	@Override
	public User getUser(long id) {
		return userDAO.getUser(id);
	}

	@Override
	public User getUser(String email) {
		System.out.println("in usermanager");
		return userDAO.getUser(email);
	}

	@Override
	public void updateUser(User user) throws Exception {
		userDAO.updateUser(user);
	}

	@Override
	public List<User> getUsers() {
		return userDAO.getUsers();
	}

	@Override
	public User getUser(String email, String password) {
		User u = getUser(email);
		if (u != null && u.getPassword() != null) {
			if (u.getPassword().equals(password)) {
				return u;
			}
		}
		return null;
	}

	@Override
	public boolean hasUser(String email) {
		return (getUser(email) != null);
	}

	@Override
	public User registerUser(Registration registration) {
		
		if (!hasUser(registration.getEmail())) {
			
			User user = new User();
			user.setEmail(registration.getEmail());
			user.setPassword(registration.getPassword());
			user.setUserName(registration.getUserName());
			user.setFirstName(registration.getFirstName());
			user.setLastName(registration.getLastName());
			user.setDob(registration.getDob());
			user.setGender(registration.isGender());
			user.setPhone(registration.getPhone());
			user.setCity(registration.getCity());
			user.setCountry(registration.getCountry());
			
			addUser(user);
			
			/* Create achievement list */
			profileManager.createUserAchievementList(user);
			
			return getUser(registration.getEmail());
			
		} else {
			return null;
		}
	}

	@Override
	public void deleteUser(String email) throws Exception {
		userDAO.deleteUser(email);
	}

	@Override
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@Override
	public void setProfileManager(ProfileManager profileManager) {
		this.profileManager = profileManager;
	}

	@Override
	public Registration getUpdateObjectFromUser(User user) {
		Registration userUpdate = new Registration();
		
		userUpdate.setUserId(user.getUserId());
		userUpdate.setEmail(user.getEmail());
		userUpdate.setPassword(user.getPassword());
		userUpdate.setConfirmPassword(user.getPassword());
		userUpdate.setUserName(user.getUserName());
		userUpdate.setFirstName(user.getFirstName());
		userUpdate.setLastName(user.getLastName());
		userUpdate.setDob(user.getDob());
		userUpdate.setGender(user.isGender());
		userUpdate.setPhone(user.getPhone());
		userUpdate.setCity(user.getCity());
		userUpdate.setCountry(user.getCountry());
		
		return userUpdate;
		
	}

	@Override
	public User updateUser(Registration userUpdate) throws Exception {
		
		if (hasUser(userUpdate.getUserId())) {
			User user = new User();
			user.setEmail(userUpdate.getEmail());
			user.setPassword(userUpdate.getPassword());
			user.setUserName(userUpdate.getUserName());
			user.setFirstName(userUpdate.getFirstName());
			user.setLastName(userUpdate.getLastName());
			user.setDob(userUpdate.getDob());
			user.setGender(userUpdate.isGender());
			user.setPhone(userUpdate.getPhone());
			user.setCity(userUpdate.getCity());
			user.setCountry(userUpdate.getCountry());
			
			updateUser(user);
			
			return getUser(userUpdate.getEmail());
			
		} else {
			return null;
		}
	}

	@Override
	public boolean hasUser(long userId) {
		return (getUser(userId) != null);
	}
	
}
