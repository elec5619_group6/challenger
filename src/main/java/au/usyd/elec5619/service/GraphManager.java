package au.usyd.elec5619.service;

import java.util.List;

import au.usyd.elec5619.domain.UserChallenge;

public interface GraphManager {

	public List<UserChallenge> getUserChallengeRecord(long id);
	
}
