package au.usyd.elec5619.service;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.*;

public class Login implements Serializable {

	@NotBlank(message = "Email must not be empty")
	@Email(message = "Must be a valid email")
	private String email;
	
	@NotBlank(message = "Password must not be empty")
    private String password;

//	@NotNull
//	private boolean rememberMe;

	public Login() {}
	
	public Login(String email, String password, boolean rememberMe) {
		super();
		this.email = email;
		this.password = password;
//		this.rememberMe = rememberMe;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
//	public boolean isRememberMe() {
//		return rememberMe;
//	}
//
//	public void setRememberMe(boolean rememberMe) {
//		this.rememberMe = rememberMe;
//	}

}
