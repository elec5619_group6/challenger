package au.usyd.elec5619.web;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;

import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.ChallengeForComment;
import au.usyd.elec5619.domain.Comment;
import au.usyd.elec5619.domain.Like;
import au.usyd.elec5619.domain.Relationship;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;
import au.usyd.elec5619.service.AchievedChallengeManager;
import au.usyd.elec5619.service.ChallengeManager;
import au.usyd.elec5619.service.CommentManager;
import au.usyd.elec5619.service.CurrentUser;
import au.usyd.elec5619.service.FriendManager;

@Controller
@RequestMapping(value = "/social")
public class UserController{

	@Resource(name="currentUser")
	private CurrentUser currentUser;
	
	@Resource(name="friendManager")
	private FriendManager friendManager;
	
	@Resource(name="achievedChallengeManager")
	private AchievedChallengeManager achievedChallengeManager;
	
	@Resource(name="challengeManager")
	private ChallengeManager challengeManager;
	
	@Resource(name="commentManager")
	private CommentManager commentManager;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		
		User user = (User) session.getAttribute("user");	//User user = currentUser.getUserById(1);
		if (null == user)
			return "redirect:/";
		
		ArrayList<Long> friendsId = friendManager.getFriends(user.getUserId());
		ArrayList<User> friends = new ArrayList<User>();
		ArrayList<UserChallenge> userChallenge = new ArrayList<UserChallenge>();
		ArrayList<ChallengeForComment> challenge = new ArrayList<ChallengeForComment>();
		ArrayList<Comment> comment = new ArrayList<Comment>();
		ArrayList<Like> likes = achievedChallengeManager.getLikeList();
		ArrayList<Long> myListId = currentUser.getMyChallenge(user.getUserId());
		for(long f : friendsId)
		{
			friends.add(currentUser.getUserById(f));
			userChallenge.addAll(achievedChallengeManager.findAll(f));
		}
		
		for(UserChallenge u : userChallenge)
		{
			int numOfLike = 0;
			boolean liked = false;
			for(Like l : likes)
			{
				if(l.getUserChallengeId() == u.getUserChallengeId())
					numOfLike++;
				if(l.getUserId() == user.getUserId()&&l.getUserChallengeId() == u.getUserChallengeId())
					liked = true;
			}
			if(u.getUserId() != user.getUserId())
			{
				boolean attemped = false;
				if(myListId.contains(u.getChallengeId()))
				{
					attemped = true;
				}
				System.out.println("-------------1111111111------------");
				challenge.add(challengeManager.findChallengeContent(u.getChallengeId(),u.getUserId(),u.getUserChallengeId(),numOfLike,liked,attemped));
				System.out.println("---cccccccccc---------------"+u.getChallengeId());
				comment.addAll(commentManager.getCommentByChallengeId(u.getUserChallengeId()));
			}
		}
		
		model.addAttribute("User", user );
		model.addAttribute("Friends",friends);
		model.addAttribute("Challenges", challenge);
		model.addAttribute("Comment",comment);
		model.addAttribute("Like",likes);
		
		
	
		return "social";
	}
	
	@RequestMapping(value="/delete/{id}&{fid}",method=RequestMethod.GET)
	public String deleteFriend(@PathVariable("id") long id,@PathVariable("fid") long fid, HttpSession session){
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		friendManager.deleteFriend(id,fid);
		
		return "redirect:/social/";
	}
	
	
	@RequestMapping(value="/add/{id}", method=RequestMethod.POST)
	public String addFriend(@PathVariable("id") long id, HttpServletRequest httpServletRequest, HttpSession session){
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		Relationship relation = new Relationship();
		relation.setUserId(id);
		relation.setFriendId(Long.valueOf(httpServletRequest.getParameter("friendId")));
		this.friendManager.addFriend(relation);
		
		return "redirect:/social/";
	}
	
	@RequestMapping(value="/addcomment/{challengeid}&{commenterid}", method=RequestMethod.POST)
	public String addComment(@PathVariable("challengeid") long challengeId,
			@PathVariable("commenterid") long commenterId, HttpServletRequest httpServletRequest, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		Comment comment = new Comment();
		comment.setUserChallengeId(challengeId);
		comment.setCommenterId(commenterId);
		comment.setContent(String.valueOf(httpServletRequest.getParameter("content")));
		System.out.println(System.currentTimeMillis()+"-------------------------------");
		comment.setCommentTime(Long.valueOf(System.currentTimeMillis()));
		this.commentManager.addComment(comment);
		
		return "redirect:/social/";
	}
	
	@RequestMapping(value="/like/{id}&{uid}", method=RequestMethod.GET)
	public String likeChallenge(@PathVariable("id") long id, @PathVariable("uid") long uid, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		//System.out.println(challenge.getChallengeId()+"------1--------------------------"+challenge.getLike());
		Like like = new Like();
		like.setUserChallengeId(id);
		like.setUserId(uid);
		this.achievedChallengeManager.increaseLike(like);
		return "redirect:/social/";
	}
	
	
	@RequestMapping(value="/dislike/{id}&{uid}", method=RequestMethod.GET)
	public String dislikeChallenge(@PathVariable("id") long id, @PathVariable("uid") long uid, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		this.achievedChallengeManager.disLike(id,uid);
		return "redirect:/social/";
	}
	
	@RequestMapping(value="/attempt/{cid}&{uid}", method=RequestMethod.GET)
	public String attempChallenge(@PathVariable("cid") long id,@PathVariable("uid") long uid, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		System.out.println(id+"--------------------------attempt-----");
		this.currentUser.takeChallenge(id, uid);
		return "redirect:/social/";
	}
}