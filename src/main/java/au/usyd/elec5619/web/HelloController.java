package au.usyd.elec5619.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.usyd.elec5619.domain.User;
public class HelloController implements Controller {
	protected final Log logger = LogFactory.getLog(getClass());
	@Override
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {
		User user = (User) arg0.getAttribute("user");
		if (null == user)
			return new ModelAndView("redirect:/");
		String now = (new Date()).toString();
		logger.info("Returning hello view with " + now);
		return new ModelAndView("user", "now", now);
	}
}