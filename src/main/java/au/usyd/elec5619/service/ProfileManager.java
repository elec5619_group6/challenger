package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Achievement;
import au.usyd.elec5619.domain.AchievementAttempt;
import au.usyd.elec5619.domain.User;

/**
 * User profile manager
 * @author John
 *
 */
@Repository
public class ProfileManager {

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<User> users;
	
	public void addUser(User newUser) {
		if (null != newUser) {
			users.add(newUser);
		}
	}
	
	@Transactional
	public void createUserAchievementList(User user) {
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("FROM " + AchievementAttempt.class.getName() + " WHERE user_id = :id");
		query.setParameter("id", user.getUserId());
		System.out.println("User exist: " + !query.list().isEmpty());
		
		if (query.list().isEmpty()) {
			System.out.println("Create attempt list for the user. ");
			List<Achievement> achList = session.createQuery("FROM " + Achievement.class.getName()).list();
			System.out.println("Get achievement List. ");
			
			for (Achievement ach : achList) {
				System.out.println("Achievement id " + ach.getAchId());
				session.save(new AchievementAttempt(user.getUserId(), ach.getAchId()));
			}
		} else {
			System.out.println("User has created attempt list before. ");
		}
	}
	
	public User getUser(long id) {
		System.out.println("Getting users");
		for (User user : users) {
			System.out.println("Getting User " + user.getUserId());
			if (id == user.getUserId())
				return user;
		}
		return null;
	}
	
	@Transactional
	public void findAll() {
		Session session = sessionFactory.getCurrentSession();
	    List<User> users = session.createQuery("FROM " + User.class.getName()).list();
	    for (User user : users) {
			System.out.println("Storing User " + user.getUserId());
			if (!this.users.contains(user)) {
				this.users.add(user);
			}
		}
	}
	
	/* Constructor */
	public ProfileManager() {
		users = new ArrayList<User>();
	}

	public ArrayList<User> getUsers() {
		return users;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
