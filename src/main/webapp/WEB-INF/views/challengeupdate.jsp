<%@ include file="/WEB-INF/views/head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Challenge</title>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Challenges</li>
							<li><a href="../challenges">Back to my challenges</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="jumbotron">
					<div class="column">
						<form action="../updatechallenge/${challenge.userChallengeId}" method="post">
							<div>
								<h3>Challenge Update</h3>
								<br />
								<h4>Title: ${challenge.title}</h4>
								<h4>Description: ${challenge.description}</h4>
								<c:if test="${0 != challenge.duration}">
									<div class="progress">
										<div id="progress" name="${challenge.progress}"></div>
										<div id="duration" name="${challenge.duration}"></div>
										<input id="progressoutput" name="progressoutput" type="hidden"
											value="${challenge.progress}" /> <input id="durationoutput"
											name="durationoutput" type="hidden"
											value="${challenge.duration}" />

										<div id="progressbar" class="progress-bar" role="progressbar"
											aria-valuenow="${challenge.progress}" aria-valuemin="0"
											aria-valuemax="${challenge.duration}"
											style="width:${challenge.percentage}%;">
											${challenge.progress}/${challenge.duration}</div>
										<script type="text/javascript">
											var progress = parseInt(document
													.getElementById("progress")
													.getAttribute("name"));
											var duration = parseInt(document
													.getElementById("duration")
													.getAttribute("name"));
											var progressbar = document
													.getElementById("progressbar");
											var progressout = document
													.getElementById("progressoutput");

											function increase() {
												progress = progress + 1;
												if (progress >= duration)
													progress = duration;
												progressbar.setAttribute("style",
														"width:" + (progress * 100 / duration) + "%;");
												progressbar.setAttribute("aria-valuenow", progress);
												progressbar.innerHTML = progress + "/" + duration;
												progressout.setAttribute( "value", progress);
												console.log(progressbar);
											}
										</script>
									</div>
									<c:if test="${0 == challenge.status}">
										<button type="button" class="btn btn-default addButton"
											onclick="increase()">
											<i class="glyphicon glyphicon-plus-sign"></i> Increase
										</button>
									</c:if>
								</c:if>

								<c:if test="${0 == challenge.status}">
									<button type="submit" class="btn btn-default">
										<i class="glyphicon glyphicon-ok-sign"></i> Update
									</button>
								</c:if>
								
								<a href="../challenges" type="submit" class="btn btn-default">
									<i class="glyphicon glyphicon-circle-arrow-left"></i> <c:choose>
										<c:when test="${0 == challenge.status}">Cancel</c:when>
										<c:otherwise>Back</c:otherwise>
									</c:choose>
								</a>
							</div>
						</form>
					</div>
				</div>

			</div>
			<!--/span-->

		</div>
		<!--/row-->
	</div>
</body>
</html>