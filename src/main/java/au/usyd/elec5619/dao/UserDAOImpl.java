package au.usyd.elec5619.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	protected final Log logger = LogFactory.getLog(getClass());
//	
//	@Autowired
//	public UserDAOImpl(@Qualifier("mySessionFactory") SessionFactory sessionFactory) {
//		this.sessionFactory = sessionFactory;
//	}
	
	@Override
	public User getUser(long id) {
        List<User> userList = new ArrayList<User>();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM User u WHERE u.userId = :userId");
        query.setParameter("userId", id);
        logger.info("executing query " + query.toString());
        userList = query.list();
        if (userList.size() > 0)
            return userList.get(0);
        else
            return null; 
	}
	
	@Override
    public User getUser(String userEmail) {
		System.out.println("executing query with email " + userEmail);
		if (userEmail == null || userEmail.trim().equals("")) {
			return null;
		}
        List<User> userList = new ArrayList<User>();
        Query query = sessionFactory.getCurrentSession().createQuery("FROM User u WHERE u.email = :email");
        query.setParameter("email", userEmail);
        System.out.println("executing query 2 " + query.toString() + query.getQueryString());
        for (String par : query.getNamedParameters()) {
        	System.out.println(par);
        }
        userList = query.list();
        if (userList.size() > 0)
            return userList.get(0);
        else
            return null;   
    }
    
	@Override
	@Transactional
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	@Override
	@Transactional
	public void addUser(String email, String password) {
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		user.setUserName(email.substring(0, email.indexOf("@")));
		
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	@Transactional
	public void updateUser(User user) throws Exception {
		if (user.getUserId() == 0) {
			throw new Exception("Updating user: userId not specified");
		} else {
			User origUser = getUser(user.getUserId());
			String origEmail = origUser.getEmail();
			String newEmail = user.getEmail();
			if (!origEmail.equals(newEmail)) {
				if (getUser(newEmail) != null) {
					throw new Exception("User email already exists");
				}
			}
			Session currentSession = this.sessionFactory.getCurrentSession();
			currentSession.merge(user);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsers() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
	}

	@Override
	public void deleteUser(String email) throws Exception {
		User user = getUser(email);
		if (user != null) {
			this.sessionFactory.getCurrentSession().delete(user);
		} else {
			throw new Exception("User: " + email + " does not exist");
		}
	}

	@Override
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


}
