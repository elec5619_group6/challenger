<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>User</title>
</head>

<body>
	<div class="container">
		<div class="starter-template">
			<h1>
				Welcome!
				<c:out value="${user.firstName} ${user.lastName}" />
			</h1>
			<p class="lead">This is just a prototype of the user home page</p>
		</div>

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4>User Profile</h4>
			</div>
			<a href="${user.userId}/statistics.htm">
			<div class="panel-body">
			
				<ul class="list-group">
					<li class="list-group-item">User name: <c:out
							value="${user.firstName} ${user.lastName}" /></li>
					<li class="list-group-item">User email: <c:out
							value="${user.email}" /></li>
				</ul>
			</div>
			</a>
			<!-- <div class="panel-footer">Not used.</div> -->
		</div>

		<div class="panel panel-info">
			<div class="panel-heading">
				<h4>Challenges</h4>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<a href="#" class="list-group-item">
						<h4 class="list-group-item-heading">Complete a web page</h4>
						<p class="list-group-item-text">Complete the first page for
							the Challenger project.</p>
					</a>
				</div>
			</div>
			<!-- <div class="panel-footer">Not used.</div> -->
		</div>

		<div class="panel panel-danger">
			<div class="panel-heading">
				<h4>Achievements</h4>
			</div>
			<div class="panel-body">
				<div class="list-group">
					<a href="${user.userId}/achievement.htm" class="list-group-item">
						<h4 class="list-group-item-heading">First blood!</h4>
						<p class="list-group-item-text">Complete the first page for
							the Challenger project.</p>
						<div class="progress">
							<div class="progress-bar" role="progressbar" aria-valuenow="60"
								aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
								100%</div>
						</div>
					</a>
				</div>
			</div>
			<!-- <div class="panel-footer">Not used.</div> -->
		</div>



	</div>
	<!-- /.container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

