package au.usyd.elec5619.domain;

/**
 * States for challenges and achievements
 * @author John
 */
public enum TaskState {

	INACTIVE,
	INPROGRESS,
	COMPLETED,
	/* add before invalid */
	INVALID
}
