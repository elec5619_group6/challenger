package au.usyd.elec5619.service;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.dao.UserDAO;
import au.usyd.elec5619.dao.UserDAOImpl;
import au.usyd.elec5619.domain.User;

public class UserManagerTest extends BaseConfiguration {

	private UserManager userManager;
	private UserDAO userDAO;
	private User user;
	
	private static final String TEST_EMAIL = "mary.email@test.com";
	private static final String TEST_PASSWORD = "pass123";
	private static final String TEST_USERNAME = "whatACoolUserName";
	private static final String TEST_FIRSTNAME = "Mary";
	private static final String TEST_PASSWORD_1 = "pass2";
	private static final String TEST_USERNAME_1 = "anotherCoolUserName";
	
	@Before
	public void setUp() throws Exception {
		userManager = new UserManagerImpl();
		userDAO = new UserDAOImpl();
		userDAO.setSessionFactory(this.getSessionFactory());
		userManager.setUserDAO(userDAO);
		
		// Create new User
		user = new User();
		user.setEmail(TEST_EMAIL);
		user.setPassword(TEST_PASSWORD);
		user.setUserName(TEST_USERNAME);
		
		userManager.addUser(user);
		user = userManager.getUser(TEST_EMAIL);
	}

	@After
	public void tearDown() throws Exception {
		if (userManager.hasUser(TEST_EMAIL)) {
			try {
				userManager.deleteUser(TEST_EMAIL);
			} catch (Exception e) { 
				throw new Exception("UserManagerTest teardown exception: " + e.getMessage());
			}
		}
	}
	
	@Test
	public void testCreateUserFromUserObject() throws Exception {
		// Delete existing test user
		if (userManager.hasUser(TEST_EMAIL)) {
			try {
				userManager.deleteUser(TEST_EMAIL);
			} catch (Exception e) {
				throw new Exception("Couldn't delete user: " + e.getMessage());
			}
		}
		assertFalse("User already exists and can't be deleted", userManager.hasUser(TEST_EMAIL));
		assertNull(userManager.getUser(TEST_EMAIL));
		
		// Persist User
		
		userManager.addUser(user);;
		
		// Test user is persisted
		assertTrue("User was not added", userManager.hasUser(TEST_EMAIL));
		User registeredUser = userManager.getUser(TEST_EMAIL);
		
		// Test added user is the correct one
		assertEquals("Added user was different from original user", TEST_EMAIL, registeredUser.getEmail());
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
	}
	
	@Test
	public void testCreateUserFromFields() throws Exception {
		// Delete existing test user
		if (userManager.hasUser(TEST_EMAIL)) {
			
			userManager.deleteUser(TEST_EMAIL);

		}
		assertFalse("User already exists and can't be deleted", userManager.hasUser(TEST_EMAIL));
		assertNull(userManager.getUser(TEST_EMAIL));
		
		// Persist User
		userManager.addUser(TEST_EMAIL, TEST_PASSWORD);;

		
		// Test user is persisted
		assertTrue("User was not added", userManager.hasUser(TEST_EMAIL));
		User registeredUser = userManager.getUser(TEST_EMAIL);
		
		// Test added user is the correct one
		assertEquals("Added user was different from original user", user.getEmail(), registeredUser.getEmail());
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
	}

	@Test
	public void testGetUser() {
		// Add test user if not there to begin with
		if (!userManager.hasUser(TEST_EMAIL)) {
			userManager.addUser(user);
		}
		assertEquals(true, userManager.hasUser(TEST_EMAIL));

		User receivedUser = userManager.getUser(TEST_EMAIL);
		
		// Test user is correct
		assertNotNull("Could not get user", receivedUser);
		assertEquals("User returned did not match query user email", TEST_EMAIL, receivedUser.getEmail());
	}

	@Test
	public void testGetUsers() {
		// Add test user if not there to begin with
		if (!userManager.hasUser(TEST_EMAIL)) {
			userManager.addUser(user);
		}
		assertEquals(true, userManager.hasUser(TEST_EMAIL));
		
		// Get a list of all the users
		List<User> allUsers = null;
		
		allUsers = userManager.getUsers();
		
		assertNotNull("List of users returned is null", allUsers); 
		assertThat("List of users returned is empty", 0, is(not(allUsers.size())));
		assertTrue("Added user was not returned in full user list", allUsers.contains(user));
	}
	
	@Test
	public void testDeleteUser() throws Exception {
		// Add test user if not there to begin with
		if (!userManager.hasUser(TEST_EMAIL)) {
			userManager.addUser(user);
		}
		assertEquals(true, userManager.hasUser(TEST_EMAIL));
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
		
		userManager.deleteUser(TEST_EMAIL);
		
		assertFalse("User with test email was not deleted", userManager.hasUser(TEST_EMAIL));
		assertNull("User was not deleted", userManager.getUser(TEST_EMAIL));
	}

	@Test
	public void testUpdateUserFromOriginal() throws Exception {
		// Add test user if not there to begin with
		if (!userManager.hasUser(TEST_EMAIL)) {
			userManager.addUser(user);
		}
		assertEquals(true, userManager.hasUser(TEST_EMAIL));
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
		
		user.setPassword(TEST_PASSWORD_1);
		user.setFirstName(TEST_FIRSTNAME);
		Date date = new Date();
		user.setDob(date);
		userManager.updateUser(user);
		
		User updatedUser = userManager.getUser(TEST_EMAIL);
		
		assertEquals(updatedUser.getUserId(), user.getUserId());
		assertEquals(updatedUser.getEmail(), TEST_EMAIL);
		assertEquals(updatedUser.getPassword(), TEST_PASSWORD_1);
		assertEquals(updatedUser.getUserName(), TEST_USERNAME);
		assertEquals(updatedUser.getFirstName(), TEST_FIRSTNAME);
		assertEquals(updatedUser.getDob(), date);
	}
	
	@Test
	public void testUpdateUserFromUnattached() throws Exception {
		// Add test user if not there to begin with
		if (!userManager.hasUser(TEST_EMAIL)) {
			userManager.addUser(user);
		}
		assertEquals(true, userManager.hasUser(TEST_EMAIL));
		assertNotNull("User with email is null", userManager.getUser(TEST_EMAIL));
		
		user = new User();
		user.setPassword(TEST_PASSWORD_1);
		user.setUserName(TEST_USERNAME_1);
		user.setFirstName(TEST_FIRSTNAME);
		Date date = new Date();
		user.setDob(date);
		try {
			userManager.updateUser(user);
			fail("Updated user with bad id when failure expected");
		} catch (Exception e) {
			return;
		}

	}
	
}
