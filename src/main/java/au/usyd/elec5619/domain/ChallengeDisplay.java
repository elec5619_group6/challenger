package au.usyd.elec5619.domain;

import java.util.Date;

public class ChallengeDisplay {

	private long userChallengeId;
	private long chaId;
	private long userId;
    private String title;
    private String description;
    private String creator;
    private int progress;
    private int duration;
    private Date postTime;
    private long timeStart;
    private long timeEnd;
    private int status;
    private double rating;
    private double difficulty;
    private int pictureId;
    private double percentage = 0;

	public ChallengeDisplay() {
		super();
	}

	public ChallengeDisplay(Challenge challenge, UserChallenge userChallenge) {
		super();
		this.setUserChallengeId(userChallenge.getUserChallengeId());
		this.setChaId(challenge.getChaId());
		this.setUserId(userChallenge.getUserId());
		this.setTitle(challenge.getTitle());
		this.setDescription(challenge.getDescription());
		this.setCreator(challenge.getCreator());
		this.setProgress(userChallenge.getProgress());
		this.setDuration(challenge.getDuration());
		this.setPostTime(challenge.getPostTime());
		this.setTimeStart(userChallenge.getTimeStart());
		this.setTimeEnd(userChallenge.getTimeEnd());
		this.setStatus(userChallenge.isStatus());
		this.setRating(challenge.getRating());
		this.setDifficulty(challenge.getDifficulty());
		this.setPictureId(challenge.getPictureId());
	}
	
	public long getUserChallengeId() {
		return userChallengeId;
	}
	public void setUserChallengeId(long userChallengeId) {
		this.userChallengeId = userChallengeId;
	}
	public long getChaId() {
		return chaId;
	}
	public void setChaId(long chaId) {
		this.chaId = chaId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
		if (0 != duration)
			this.percentage = (double)progress / (double)duration * 100;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
		if (0 != duration)
			this.percentage = (double)progress / (double)duration * 100;
	}
	public Date getPostTime() {
		return postTime;
	}
	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}
	public long getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(long timeStart) {
		this.timeStart = timeStart;
	}
	public long getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(long timeEnd) {
		this.timeEnd = timeEnd;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public double getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(double difficulty) {
		this.difficulty = difficulty;
	}
	public int getPictureId() {
		return pictureId;
	}
	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}

	@Override
	public boolean equals(Object obj) {
		return this.chaId == ((ChallengeDisplay)obj).getChaId();
	}

	public double getPercentage() {
		return percentage;
	}
	
}
