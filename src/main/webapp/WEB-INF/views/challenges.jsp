<%@ include file="/WEB-INF/views/head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Challenges</li>
							<li><a href="./challengelist">Choose challenge</a></li>
							<li><a href="./addchallenge">Create challenge</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h4>Challenges</h4>
					</div>
					<div class="panel-body">
						<c:if test="${not empty msg}">
							<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>${msg}</strong>
							</div>
						</c:if>
						<c:choose>
							<c:when test="${not empty challengelist}">
								<c:forEach items="${challengelist}" var="display">
									<a href="./challenges/${display.userChallengeId}" class="list-group-item clearfix">
										<span>
											<h4 class="list-group-item-heading">${display.title}</h4>
											<p class="list-group-item-text">${display.description}</p>
											<c:if test="${0 != display.duration}">
												<div class="progress">
													<div class="progress-bar" role="progressbar"
														aria-valuenow="${display.progress}" aria-valuemin="0" aria-valuemax="${challenge.duration}"
														style="width: ${display.percentage}%;">
														${display.progress}/${display.duration}</div>
												</div>
											</c:if>
										</span>
									</a>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<h4>You haven't start any challenges yet.</h4>
								<p>
									<a href="./challengelist" class="btn btn-primary btn-lg">Start
										right away �</a>
								</p>
							</c:otherwise>
						</c:choose>
					</div>
					<!-- <div class="panel-footer">Not used.</div> -->
				</div>
			</div>
			<!--/span-->

		</div>
		<!--/row-->
	</div>
</body>
</html>