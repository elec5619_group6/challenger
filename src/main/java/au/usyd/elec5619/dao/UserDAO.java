package au.usyd.elec5619.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.User;

public interface UserDAO {

	public User getUser(long id);
	
	public User getUser(String email);

	public void addUser(User user);
	
	public void addUser(String email, String password);
	
	public void updateUser(User user) throws Exception;

	public List<User> getUsers();

	public void deleteUser(String email) throws Exception;

	public void setSessionFactory(SessionFactory sessionFactory);



}
