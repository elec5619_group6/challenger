<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>BarChart</title>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/select/bootstrap-select.min.css" />
<script src="${pageContext.request.contextPath}/resources/select/bootstrap-select.min.js"></script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	
	// Load the Visualization API and the piechart package.
	google.load('visualization', '1.0', {
		'packages' : [ 'corechart' ]
	});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(init);

	var countData = [];
	var timeData = [];
	var totalCountData = [];
	var totalTimeData = [];
	
	function init() {
		setOption(0);
		drawChart();
	}
	
	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	function drawChart() {

		for (i=0; i<countData.length; i++) {
			console.log(timeData[i]);
			console.log(countData[i]);
		}
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('date', 'Topping');
		data.addColumn('number', 'Challenges');
		for (i=0; i<countData.length; i++) {
			data.addRow([new Date(timeData[i]), parseInt(countData[i])]);
		}
		//data.addRows([ [ 'Mushrooms', 3 ], [ 'Onions', 1 ], [ 'Olives', 1 ], [ 'Zucchini', 1 ], [ 'Pepperoni', 2 ] ]);

		// Date format
		var formatter_medium = new google.visualization.DateFormat({formatType: 'medium'});
		formatter_medium.format(data, 0);
		
		// Set chart options
		var options = {
			'width' : 600,
			'height' : 500,
			vAxis: {minValue: 0}
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	}
	
	function addAttribute(count, time) {
		totalCountData[totalCountData.length] = count;
		totalTimeData[totalTimeData.length] = time;
	}
	
	function setOption(value) {
		if ((0 == value) || (value >= totalCountData.length)) {
			for (i=0; i<totalCountData.length; i++) {
				timeData[i] = totalTimeData[i];
				countData[i] = totalCountData[i];
			}
		} else {
			for (i=0; i<value; i++) {
				timeData[i] = totalTimeData[i + totalTimeData.length - value];
				countData[i] = totalCountData[i + totalCountData.length - value];
			}
		}
		drawChart();
	}
	
	$(document).ready(function(e) {
        $('.selectpicker').selectpicker();
    });

</script>

</head>

<body>
<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="sidebar-nav-fixed">
					<div class="well">
						<ul class="nav ">
							<li class="nav-header">Graphs</li>
							<li><a href="#">Challenge completion</a></li>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/sidebar-nav-fixed -->
			</div>
			<!--/span-->
			<div class="col-md-8">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4>Challenge completion history on each day</h4>
						<select class="selectpicker show-tick" data-style="btn-success"
						onChange="setOption(parseInt(this.value))">
							<option value="0">Default</option>
							<option value="3">Recent 3 days</option>
							<option value="7">Recent 7 days</option>
						</select>
					</div>
					<div class="panel-body">
						<input type="hidden" id="list" value="${display}" />
						<c:forEach items="${display}" var="display">
							<script type="text/javascript">
								var i = document.createElement('div');
								i.id = "count";
								i.name = "${display.count}";
								document.body.appendChild(i);

								var j = document.createElement('div');
								j.id = "time";
								j.name = "${display.finishTime}";
								document.body.appendChild(j);

								console.log(i.name);
								console.log(j.name);
								console.log("input");

								addAttribute(i.name, j.name);

								document.body.removeChild(i);
								document.body.removeChild(j);
							</script>
						</c:forEach>

						<!--Div that will hold the bar chart-->
						<div id="chart_div">test</div>

					</div>
					<!-- <div class="panel-footer">Not used.</div> -->
				</div>
			</div>
			<!--/span-->
		</div>
		<!--/row-->
	</div>
</body>
</html>