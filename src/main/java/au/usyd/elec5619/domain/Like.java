package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.likechallenge")
public class Like implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="like_id")
	private long likeId;
	
	@Column(name="userchallenge_id")
	private long userChallengeId;
	
	@Column(name="user_id")
	private long userId;
	
	public Like(long userChallengeId,long userId)
	{
		this.userChallengeId = userChallengeId;
		this.userId = userId;
	}

	public long getLikeId() {
		return likeId;
	}

	public void setLikeId(long likeId) {
		this.likeId = likeId;
	}

	public long getUserChallengeId() {
		return userChallengeId;
	}

	public void setUserChallengeId(long userChallengeId) {
		this.userChallengeId = userChallengeId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Like()
	{}
}
