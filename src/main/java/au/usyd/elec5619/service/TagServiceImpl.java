package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.usyd.elec5619.dao.TagDAO;
import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;

@Service
public class TagServiceImpl implements TagService {
	
	@Autowired
	TagDAO tagDAO;

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<Tag> userTags;
	
	@Override
	public ArrayList<Tag> getTagsByUserId(long userId) {
		return tagDAO.getTagsByUserId(userId);
	}
	
	@Override
	public void addTag(String tagName) {
		tagDAO.addTag(tagName);
	}
	
	@Override
	public Tag getTagById(long tagId) {
		return tagDAO.getTagById(tagId);
	}
	
	@Override
	public void addTagForUser(String tagName, long userId) {
		tagDAO.addTagForUser(tagName, userId);
	}
	
	@Override
	public void addTagForChallenge(String tagName, long challengeId) {
		tagDAO.addTagForChallenge(tagName, challengeId);
	}

	@Override
	public void persistUser(User user) {
		sessionFactory.getCurrentSession().persist(user);
	}
	
	@Override
	public User findUserById(String id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public ArrayList<Tag> getUserTags() {
		return userTags;
	}

	@Override
	public void setUserTags(ArrayList<Tag> userTags) {
		this.userTags = userTags;
	}
	
	@Override
	public Object getTagsStringByUserId(long userId) {
		List<Tag> tagList = getTagsByUserId(userId);
		String tagString = "";
		if (tagList != null) {
			for (Tag t : tagList) {
				tagString += t.getTagName() + ",";
			}
			if (tagString.endsWith(",")) {
				tagString = tagString.substring(0, tagString.length()-1);
			}
		}
		return tagString;
	}

	@Override
	public void addTagsForUser(String tags, long userId) {
		String tagString = tags;
		while (true) {
			int pos = tagString.indexOf(',');
			if (pos < 0) {
				addTagForUser(tagString, userId);
				break;
			}
			if (pos == tagString.length()-1){
				addTagForUser(tagString.substring(0, tagString.length()-1), userId);
				break;
			}
			addTagForUser(tagString.substring(0, pos), userId);
			tagString = tagString.substring(pos + 1);
		}		
		
	}

	@Override
	public void removeAllTagsForUser(long userId) {
		tagDAO.removeAllTagsForUser(userId);
	}
	
}
