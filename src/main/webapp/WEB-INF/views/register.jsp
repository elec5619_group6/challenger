<%@ include file="/WEB-INF/views/head.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html lang="en">
<head>
<title>Register</title>

<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.css" />
<script
	src="${pageContext.request.contextPath}/resources/tagsinput/bootstrap-tagsinput.js"></script>

<style type="text/css">
.bootstrap-tagsinput {
	width: 100%;
}

.label {
	line-height: 2 !important;
}

.error {
	color: red;
}

.center-block() {
	display: block;
	margin-left: auto;
	margin-right:auto;
}

.container {
	width: 940px;
	.center-block();
}
</style>
</head>
<body>


	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-6 col-md-offset-3 jumbotron">
				<form:form action="register" commandName="userRegister"
					method="post" class="form-horizontal">
					<div class="form-group">
						<h2 class="text-center">Register an Account</h2>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Email:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="email" path="email" class="form-control"
								required="true" autofocus="true" />
							<form:errors path="email" cssClass="error" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Password:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="password" path="password" class="form-control"
								required="true" />
							<form:errors path="password" cssClass="error" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Confirm Password:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="password" path="confirmPassword" class="form-control"
								required="true" />
							<form:errors path="password" cssClass="error" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">First Name:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="firstName" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Last Name:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="lastName" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">User Name:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="userName" class="form-control"
								required="true" />
							<form:errors path="userName" cssClass="error" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Phone:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="phone" class="form-control" />
							<form:errors path="phone" cssClass="error" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Gender:</label>
						<div class="col-sm-9 col-md-9">
							<label> <form:radiobutton path="gender" value="true"
									checked="true" /> Male
							</label> <label> <form:radiobutton path="gender" value="false" />
								Female
							</label>
							<form:errors path="gender" cssClass="error" />
						</div>
					</div>
<!-- 					<div class="form-group"> -->
<!-- 						<label class="col-sm-3 control-label">Date of Birth:</label> -->

<!-- 						<div class='col-sm-9'> -->
<!-- 							<div class='input-group date' id='datetimepicker5'> -->
<%-- 								<form:input path="${userRegister.dob}" type='text' --%>
<%-- 									class="form-control" data-date-format="YYYY/MM/DD" /> --%>
<!-- 								<span class="input-group-addon"> <span -->
<!-- 									class="glyphicon glyphicon-calendar"></span> -->
<!-- 								</span> -->
<!-- 							</div> -->
<!-- 							<script type="text/javascript"> -->
<!-- //  								$(function() { -->
<!-- //  								    $('#sandbox-container input').datepicker({ -->
<!-- //  								    }); -->
<!-- //  								}); -->
<!-- 							</script> -->
<!-- 						</div> -->
						<!-- 				<div class="col-sm-9"> -->
						<!-- 					<select name="year" id="year"> -->
						<!-- 					</select> <select name="month" id="month"> -->
						<!-- 						<option value=01>JAN</option> -->
						<!-- 						<option value=02>FEB</option> -->
						<!-- 						<option value=03>MAR</option> -->
						<!-- 						<option value=04>APR</option> -->
						<!-- 						<option value=05>MAY</option> -->
						<!-- 						<option value=06>JUN</option> -->
						<!-- 						<option value=07>JUL</option> -->
						<!-- 						<option value=08>AUG</option> -->
						<!-- 						<option value=09>SEP</option> -->
						<!-- 						<option value=10>OCT</option> -->
						<!-- 						<option value=11>NOV</option> -->
						<!-- 						<option value=12>DEC</option> -->
						<!-- 					</select> <select name="day" id="day"> -->
						<!-- 					</select> -->
						<!-- 				</div> -->
<!-- 					</div> -->
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">Country:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="country" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-md-3 control-label">City:</label>
						<div class="col-sm-9 col-md-9">
							<form:input type="text" path="city" class="form-control" />
						</div>
					</div>
					<!-- 			<div class="form-group"> -->
					<!-- 				<label class="col-sm-2 control-label">Interests:</label> -->
					<!-- 				<div class="col-sm-5"> -->
					<%-- 					<form:input type="text" path="interests" class="col-sm-5" --%>
					<%-- 						value="Building website" data-role="tagsinput" /> --%>
					<!-- 				</div> -->
					<!-- 			</div> -->
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3">
							<button type="submit" class="btn btn-success btn-block">Register</button>
						</div>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form:form>
				 
				<div class="col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-3">
					Already have an account?
					<a class="btn btn-primary" href="login">Login</a>
				</div>

			</div>
		</div>
	</div>
	<!-- /container -->


</body>
</html>