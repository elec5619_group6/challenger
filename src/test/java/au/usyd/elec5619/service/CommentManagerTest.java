package au.usyd.elec5619.service;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.Comment;

public class CommentManagerTest extends BaseConfiguration
{
	private CommentManager commentManager;
	private ArrayList<Comment> commentList;
	private Comment comment1;
	private Comment comment2;
	private int origSize;
	
	private static long USER_ID = 999;
	private static long USER_CHALLENGE_ID_1 = 1001;
	
	private static int COMMENT_COUNT = 2;
	
	@Before
	public void setUp() throws Exception
	{
		commentManager = new CommentManager();
		commentManager.setSessionFactory(this.getSessionFactory());
		commentList = commentManager.getCommentByChallengeId(USER_CHALLENGE_ID_1);
		comment1 = new Comment(USER_CHALLENGE_ID_1,USER_ID,"test",1234);
		comment2 = new Comment(USER_CHALLENGE_ID_1,USER_ID,"test",1235);
		origSize = commentList.size();
	}
	
	@Test
	public void testCommentManager()
	{
		commentManager.addComment(comment1);
		commentManager.addComment(comment2);
		assertEquals(origSize+COMMENT_COUNT,commentManager.getCommentByChallengeId(USER_CHALLENGE_ID_1).size());
		
		commentList = commentManager.getCommentByChallengeId(USER_CHALLENGE_ID_1);
		assertEquals(COMMENT_COUNT,commentList.size()-origSize);
	}
	
}
