package au.usyd.elec5619.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Achievement;
import au.usyd.elec5619.domain.AchievementAttempt;
import au.usyd.elec5619.domain.AchievementDisplay;
import au.usyd.elec5619.domain.AchievementType;
import au.usyd.elec5619.domain.TaskState;
import au.usyd.elec5619.domain.UserStatistics;
import au.usyd.elec5619.web.AchievementController;

/**
 * AchievementManager
 * @author John
 *
 */
@Repository
public class AchievementAttemptManager {
	
	private static final Logger logger = LoggerFactory.getLogger(AchievementAttemptManager.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<AchievementAttempt> attemptList;
	private ArrayList<Achievement> achList;
	private ArrayList<AchievementDisplay> achDisplay;
	private UserStatistics userStat;
	
	@Transactional
	public void getAchievementList(long userid) {
		Session session = sessionFactory.getCurrentSession();
		Connection conn = session.connection();
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("SELECT * "
					+ "  FROM challenger.achievement_attempt NATURAL JOIN challenger.achievement ORDER BY percentage DESC");
			ResultSet resultSet = stmt.executeQuery();
			achDisplay.clear();
			
			while (resultSet.next()) {
				logger.info(resultSet.getString("attempt_id")
						+ " - " + resultSet.getString("title")
						+ " - "  + resultSet.getString("description")
						+ " - "+ resultSet.getDouble("percentage")
						+ " - "   + resultSet.getInt("progress"));
				
				achDisplay.add(new AchievementDisplay(resultSet.getLong("achievement_id"),
						resultSet.getString("title"),
						resultSet.getString("description"),
						resultSet.getDouble("percentage") * 100,
						resultSet.getInt("progress"),
						resultSet.getInt("quantity"),
						resultSet.getInt("type"),
						resultSet.getInt("state"),
						resultSet.getDate("completed_time")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * true: new achievement unlock*/
	@Transactional
	public void getUserStatistics(long userId) {
		Session session = sessionFactory.getCurrentSession();
		
		Query query = session.createQuery("FROM " + UserStatistics.class.getName() + " WHERE user_id = :id");
		query.setParameter("id", userId);
		
		if (query.list().isEmpty()) {
			logger.info("Creating statistics table for the user. ");
			userStat = new UserStatistics(userId, new Date());
			session.save(userStat);
		} else {
			logger.info("Getting user statistics. ");
			userStat = (UserStatistics) query.list().get(0);
		}
	}
	
	@Transactional
	public boolean updateUserLoginRecord(long userId) {
		boolean achUnlock = false;
		Session session = sessionFactory.getCurrentSession();
		
		/* Update user statistics */
		getUserStatistics(userId);
		userStat.setLastLoginTime(new Date());
		userStat.incTotalLoginNum();
		session.save(userStat);
		
		/* Update achievement */
		return updateAchievement(AchievementType.LOGIN_NUMBER);
	}
	
	@Transactional
	public void startNewChallenge(long userId) {
		Session session = sessionFactory.getCurrentSession();
		
		getUserStatistics(userId);
		userStat.incIncmpChg();
		session.save(userStat);
	}
	
	@Transactional
	public boolean finishChallenge(long userId) {
		Session session = sessionFactory.getCurrentSession();
		
		/* update challenge statistics */
		getUserStatistics(userId);
		userStat.incCmpChg();
		session.save(userStat);
		
		/* update achievements */
		return updateAchievement(AchievementType.CHG_NUMBER);
	}
	
	@Transactional
	public boolean updateChallengeCreationAchievement() {
		return updateAchievement(AchievementType.CREATE_CHG_NUMBER);
	}
	
	private boolean updateAchievement(AchievementType type) {
		boolean achUnlock = false;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM " + AchievementAttempt.class.getName() + " as achAtpt WHERE achAtpt.achId IN"
				+ " (SELECT ach.achId FROM " + Achievement.class.getName() + " as ach WHERE ach.category =:type)");
		query.setParameter("type", type);
		
		for (AchievementAttempt achAtmpt : (List<AchievementAttempt>)query.list()) {
			Achievement achievement = (Achievement) session.get(Achievement.class, achAtmpt.getAchId());
			logger.info("Updating user achievement: " + achAtmpt.getAchId());
			if (TaskState.COMPLETED != achAtmpt.getState()) {
				achAtmpt.incProgress();
				achAtmpt.setPercentage((double)achAtmpt.getProgress() / (double)achievement.getQuantity());
				if (achievement.getQuantity() == achAtmpt.getProgress()) {
					achAtmpt.setState(TaskState.COMPLETED);
					achUnlock = true;
				}
				session.save(achAtmpt);
			}
		}
		return achUnlock;
	}

	public AchievementAttemptManager() {
		this.attemptList = new ArrayList<AchievementAttempt>();
		this.achList = new ArrayList<Achievement>();
		this.achDisplay = new ArrayList<AchievementDisplay>();
	}


	public ArrayList<AchievementAttempt> getAttemptList() {
		return attemptList;
	}

	public ArrayList<Achievement> getAchList() {
		return achList;
	}

	public ArrayList<AchievementDisplay> getAchDisplay() {
		return achDisplay;
	}

	public UserStatistics getUserStat() {
		return userStat;
	}
	
}