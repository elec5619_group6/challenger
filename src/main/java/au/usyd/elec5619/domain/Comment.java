package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="challenger.comment")
public class Comment implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="comment_id")
	private long commentId;
	
	@Column(name="userchallenge_id")
	private long userChallengeId;
	
	@Column(name="commenter_id")
	private long commenterId;
	
	@Column(name="content")
	private String content;
	
	@Column(name="comment_time")
	private long commentTime;

	public long getCommentId() {
		return commentId;
	}

	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}

	public long getUserChallengeId() {
		return userChallengeId;
	}

	public void setUserChallengeId(long userChallengeId) {
		this.userChallengeId = userChallengeId;
	}

	public long getCommenterId() {
		return commenterId;
	}

	public void setCommenterId(long commenterId) {
		this.commenterId = commenterId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getCommentTime() {
		return commentTime;
	}

	public void setCommentTime(long commentTime) {
		this.commentTime = commentTime;
	}
	
	public Comment(long userChallengeId,long commenterId,String content,long time)
	{
		this.userChallengeId = userChallengeId;
		this.commenterId = commenterId;
		this.setCommentTime(time);
		this.content= content;
	}
	
	public Comment()
	{}

	
}
