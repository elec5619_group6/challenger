<%@ include file="/WEB-INF/views/head.jsp" %>

<html lang="en">
<head>
</head>
<body>
	<c:if test="${user == null}">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive center-block"
						src="${pageContext.request.contextPath}/resources/1.jpg"
						alt="First slide" />
					<div class="container">
						<div class="carousel-caption">
							<h1>Challenger</h1>
							<h3>Make a difference every day!</h3>
							<p>
								<a class="btn btn-primary btn-lg" role="button" href="login">Login</a>
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="img-responsive center-block"
						src="${pageContext.request.contextPath}/resources/2.jpg"
						alt="Second slide" />
					<div class="container">
						<div class="carousel-caption">
							<h1>Join us right now</h1>
							<h3></h3>
							<p>
								<a class="btn btn-primary btn-lg" role="button" href="register">Sign
									up for free!</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"><span
				class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		<!-- /.carousel -->
	</c:if>
	<div class="container">

		<div class="starter-template">
			<c:choose>
				<c:when test="${user == null}">
					<!-- <h1>Welcome, Guest!</h1>
					<div class="btn-group">
						<a href="login"><button type="button" class="btn btn-default">Login</button></a>
					</div>

					<div class="btn-group">
						<a href="register"><button type="button"
								class="btn btn-default">Register</button></a>
					</div> -->
				</c:when>
				<c:otherwise>
					<h1>
						Welcome,
						<c:out value="${user.firstName}" />
						!
					</h1>
					<c:if test="${not empty msg}">
						<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert">
								<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
							</button>
							<strong>${msg}</strong>
						</div>
					</c:if>
					<div class="panel panel-info">
					<div class="panel-heading">
						<h4>Challenges</h4>
					</div>

					<div class="panel-body">
						<c:if test="${not empty chgtitle}">
							<div class="alert alert-info alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert">
									<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
								</button>
								<strong>${chgtitle}</strong>
							</div>
						</c:if>
						<div class="list-group">
							<c:forEach items="${challengelist}" var="display">

								<a href="./challengedetail/${display.chaId}"
									class="list-group-item clearfix"> <span>
										<h4 class="list-group-item-heading">${display.title}</h4>
										<p class="list-group-item-text">${display.description}</p>
								</span> <span class="pull-right"> <input type="button"
										class="btn btn-md btn-primary" value="Detail" />
								</span>
								</a>

							</c:forEach>
						</div>

						<!-- <div class="panel-footer">Not used.</div> -->
					</div>
				</div>
				</c:otherwise>
			</c:choose>

		</div>

	</div>

	<!-- Bootstrap core JavaScript
	    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../../js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
