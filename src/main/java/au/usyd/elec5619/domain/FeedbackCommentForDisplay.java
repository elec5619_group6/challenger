package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

public class FeedbackCommentForDisplay implements Serializable 
{
	
	private String userName;

	private String comment;
	private long feedback_id;

	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public FeedbackCommentForDisplay(String username,String comment,long feedbackid,Date date)
	{
		this.userName = username;
		this.comment = comment;
		this.feedback_id = feedbackid;
		this.date = date;
	}

	public String getUserName() {
		return userName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getFeedback_id() {
		return feedback_id;
	}

	public void setFeedback_id(long feedback_id) {
		this.feedback_id = feedback_id;
	}
	
}