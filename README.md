# README #
## Challenger Project ##
This project aims to bulld a website that allows users to create, select and start challenges that make positive changes to daily life. Users can not only upload records for challenge completion, but also invite their friends to join them.      
### *This project is in honor of Robin Williams.* ###

## Website ##
[Challenger](http://54.69.143.93:8080/elec5619/)

## Presentation video ##
[Presentation](http://youtu.be/gGt5mqtxilE)

## Database setting ##
Settings for running challenger master branch on your own machine.         
######
* SQL file for all tables creation: \src\main\resources\META-INF\databaseschema.sql       
1. Run this .sql file to create all tables in your local database server.       
2. This file also includes some of the records in the table. If there is any missing, please add it to this file.        
* Database username and password setting          
1. Please change the username and password to match your local server. These properties are stored here: \src\main\resources\database.properties       
2. Make sure the JDBC URL is right to your server.