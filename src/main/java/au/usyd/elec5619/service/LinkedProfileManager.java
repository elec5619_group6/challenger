//package au.usyd.elec5619.service;
//
//import java.util.List;
//
//import org.hibernate.SessionFactory;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import au.usyd.elec5619.dao.LinkedProfileDAO;
//import au.usyd.elec5619.domain.LinkedProfile;
//import au.usyd.elec5619.service.Registration;
//
//@Service("LinkedProfileManager")
//@Transactional
//public interface LinkedProfileManager {
//	
//	public LinkedProfile getLinkedProfile(long id);
//	
//	public LinkedProfile getLinkedProfile(String email);
//	
//	public LinkedProfile getLinkedProfile(String email, String password);
//
//	public void addLinkedProfile(LinkedProfile LinkedProfile);
//	
//	public void addLinkedProfile(String email, String password);
//	
//	public void updateLinkedProfile(LinkedProfile LinkedProfile) throws Exception;
//
//	public List<LinkedProfile> getLinkedProfiles();
//
//	public boolean hasLinkedProfile(long LinkedProfileId);
//	
//	public boolean hasLinkedProfile(String email);
//
//	public LinkedProfile registerLinkedProfile(Registration registration);
//
//	public void deleteLinkedProfile(String email) throws Exception;
//
//	public void setLinkedProfileDAO(LinkedProfileDAO LinkedProfileDAO);
//
//	public Registration getUpdateObjectFromLinkedProfile(LinkedProfile self);
//
//	public LinkedProfile updateLinkedProfile(Registration LinkedProfileUpdate) throws Exception;
//
//}
