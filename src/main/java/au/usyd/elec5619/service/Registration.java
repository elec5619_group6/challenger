package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.*;
import org.springframework.format.annotation.DateTimeFormat;

import au.usyd.elec5619.validator.PasswordMatch;

@PasswordMatch(message="Passwords must match", first="password", second="confirmPassword")
public class Registration implements Serializable {

	private long userId;
	
	@NotBlank(message = "Email must not be empty")
	@Email(message = "Must be a valid email")
	private String email;
	
	@NotBlank(message = "Password must not be empty")
    private String password;
	
	@NotBlank(message = "Confirm password must not be empty")
    private String confirmPassword;
	
    private String firstName;
	
    private String lastName;
	
    @Digits(fraction = 0, integer = 14, message="Phone must be a number")
    @Size(min=9, max=14, message="Phone must be between 9 to 14 digits")
	private String phone;
	
    @NotNull
    private boolean gender;
	
    @DateTimeFormat(pattern="dd-MMM-yyyy")
    @Past(message="Date of birth must be a past date")
    private Date dob;
	
	@NotBlank(message="Username must not be empty")
    private String userName;
	
	
    private String country;
	
	
    private String city;
    

	public Registration() {}
	
	public Registration(String email, String password, boolean rememberMe) {
		super();
		this.email = email;
		this.password = password;
	}

	/* Functions */
	@Override
	public boolean equals(Object obj) {
		return email == ((Registration)obj).getEmail();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
}
