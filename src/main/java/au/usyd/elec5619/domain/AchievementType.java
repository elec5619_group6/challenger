package au.usyd.elec5619.domain;

/**
 * Achievement categories
 * @author John
 *
 */
public enum AchievementType {

	CHG_NUMBER,
	LOGIN_NUMBER,
	CREATE_CHG_NUMBER
	/* add here */
}
