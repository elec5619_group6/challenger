package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.*;

public class FeedbackCommentManagerTest extends BaseConfiguration
{
	private FeedbackCommentMananger feedbackCommentManager;
	private ArrayList<FeedbackCommentForDisplay> commentList;
	private FeedbackComment comment1;
	private FeedbackComment comment2;
	private int originalSize = 0;
	private static int COMMENT_COUNT = 2;
	
	@Before
	public void setUp() throws Exception
	{
		
		
		feedbackCommentManager = new FeedbackCommentMananger();
		feedbackCommentManager.setSessionFactory(this.getSessionFactory());
		commentList = feedbackCommentManager.getCommDisplay();
		originalSize = commentList.size();
		System.out.println(originalSize+"2222222222222222222");
		
	}
	@Test
	public void testCommentManager()
	{
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Feedback feedback = new Feedback(CHG_TITLE,3,4,1,1);
		this.getSession().save(feedback);
		query = this.getSession().createQuery("FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", CHG_TITLE);
		feedback = (Feedback)query.list().get(0);

		
		//testfeedbackcomment();
		
		query = this.getSession().createQuery("DELETE FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment",CHG_TITLE);
		query.executeUpdate();
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		
	}
	private void testfeedbackcomment(){
		FeedbackComment feedbackcomment = new FeedbackComment();
		feedbackcomment.setFeedback_id(74);
		feedbackcomment.setUser_id(1);
		feedbackcomment.setComment("test");
		feedbackcomment.setDate(new Date());		
		FeedbackComment feedbackcomment2 = new FeedbackComment();
		feedbackcomment2.setFeedback_id(74);
		feedbackcomment2.setUser_id(1);
		feedbackcomment2.setComment("test");
		feedbackcomment2.setDate(new Date());
		feedbackCommentManager.addComment(feedbackcomment);
		feedbackCommentManager.addComment(feedbackcomment2);
		
		/**
		 * I do not know why feedbackCommentManager.getCommDisplay().size() return 0
		 */
		System.out.println(feedbackCommentManager.getCommDisplay().size()+"33333333333333333333333333333");
		assertEquals(originalSize+COMMENT_COUNT,feedbackCommentManager.getComment().size());
		
		commentList = feedbackCommentManager.getCommDisplay();
		assertEquals(COMMENT_COUNT,commentList.size()-originalSize);
	}
	
}