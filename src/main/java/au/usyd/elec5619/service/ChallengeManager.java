package au.usyd.elec5619.service;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.ChallengeForComment;
import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.FeedbackForDisplay;
import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.TagChallenge;

@Repository
public class ChallengeManager {
	
	private static final Logger logger = LoggerFactory.getLogger(ChallengeManager.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Transactional
	public ChallengeForComment findChallengeContent(long id, long userId, long userChallengeId,
			int numOfLike, boolean liked, boolean attemped) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		//System.out.println("------------------"+id);
		Query query = currentSession.createQuery("FROM Challenge where challenge_id=:id");
		query.setLong("id", id);
		
		Challenge challenge = (Challenge) query.uniqueResult();
		//System.out.println("---cccccccccc---------------"+challenge.getTitle());
	    return new ChallengeForComment(challenge,userId,userChallengeId,numOfLike,liked,attemped);
		
	}
	
	@Transactional
	public Challenge findRelatedChallenge(long id) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM Challenge where Challenge_id=:id");
		query.setLong("id", id);
		
		Challenge challenge = (Challenge) query.uniqueResult();
	    return challenge;		
	}
	
	@Transactional
	public Challenge getChallengeById(long challengeId) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		Challenge challenge = (Challenge) currentSession.get(Challenge.class, challengeId);		
		return challenge;
	}
	
	@Transactional
	public Challenge getChallengeByTitle(String title) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM " + Challenge.class.getName() + " WHERE title =:title");
		query.setParameter("title", title);
		if (query.list().isEmpty()) {
			logger.info("Challenge with title: " + title + " not found. ");
			return null;
		} else {
			logger.info("Challenge found. ");
			return (Challenge) query.list().get(0);
		}
	}
	
	/* Added by John */
	@Transactional
	public void addChallenge(Challenge challenge, ArrayList<Tag> tags) {
		Session currentSession = sessionFactory.getCurrentSession();
		
		Query query;

		query = currentSession.createQuery("FROM " + Challenge.class.getName() + " WHERE title =:title");
		query.setParameter("title", challenge.getTitle());
		if (query.list().isEmpty()) {
			logger.info("Creating new challenge. ");
			currentSession.save(challenge);
		} else {
			logger.info("Challenge exist. ");
		}
		
		for (Tag tag : tags) {
			query = currentSession.createQuery("FROM " + Tag.class.getName() + " WHERE tag_name =:name");
			query.setParameter("name", tag.getTagName());
			logger.info("Searching for tag: " + tag.getTagName());
			
			if (query.list().isEmpty()) {
				currentSession.save(tag);
				
				query = currentSession.createQuery("FROM " + Tag.class.getName() + " WHERE tag_name = :name");
				query.setParameter("name", tag.getTagName());
			}
			
			long tagId = ((Tag) query.list().get(0)).getId();
			
			query = currentSession.createQuery("FROM " + Challenge.class.getName() + " WHERE title =:title");
			query.setParameter("title", challenge.getTitle());
			currentSession.save(new TagChallenge(((Challenge) query.list().get(0)).getChaId(), tagId));
		}
	}
	
	@Transactional
	public void modifyChallenge(Challenge newChallenge) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("UPDATE " + Challenge.class.getName() 
				+ " SET title =:title, description =:description, creator =:creator,"
				+ " duration =:duration, post_time =:post_time, rating =:rating,"
				+ " difficulty =:difficulty WHERE challenge_id=:id");
		
		query.setParameter("title", newChallenge.getTitle());
		query.setParameter("description", newChallenge.getDescription());
		query.setParameter("creator", newChallenge.getCreator());
		query.setParameter("duration", newChallenge.getDuration());
		query.setParameter("post_time", newChallenge.getPostTime());
		query.setParameter("rating", newChallenge.getRating());
		query.setParameter("difficulty", newChallenge.getDifficulty());
		query.setParameter("challenge_id", newChallenge.getChaId());
		query.executeUpdate();
	}
	
	@Transactional
	public ArrayList<Challenge> getChallengeList() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM " + Challenge.class.getName() + " ORDER BY post_time DESC");
		return (ArrayList<Challenge>) query.list();
	}
	
	public ChallengeManager() {
		super();
	}
}
