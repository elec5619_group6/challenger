package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.tag_challenge")
public class TagChallenge {

	@Id
	@GeneratedValue
	@Column(name="idtag_challenge")
	private long id;
	
	@Column(name="challenge_id")
	private long challengeId;
	
	@Column(name="tag_id")
	private long tagId;

	public TagChallenge() {
		super();
	}

	public TagChallenge(long challengeId, long tagId) {
		super();
		this.challengeId = challengeId;
		this.tagId = tagId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}

	public long getTagId() {
		return tagId;
	}

	public void setTagId(long tagId) {
		this.tagId = tagId;
	}
	
}
