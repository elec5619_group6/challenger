package au.usyd.elec5619.domain;

import java.util.Date;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.service.FeedbackCommentMananger;

public class FeedbackCommentTest extends BaseConfiguration {
	private FeedbackCommentMananger feedbackCommentManager;
	private int INTEREST_SCORE = 3;
	private int DIFFCUITY_SCORE = 4;
	private String FEEDBACK_COMMENT = "ffff";
	@Test
	public void persistFeedback()
	{
		/**
		 * Create temporary required data in database
		 */
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		challenge = (Challenge)query.list().get(0);
		
		Feedback feedback = new Feedback(CHG_TITLE,INTEREST_SCORE,DIFFCUITY_SCORE,user.getUserId(),challenge.getChaId());
		this.getSession().save(feedback);
		query = this.getSession().createQuery("FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", CHG_TITLE);
		feedback = (Feedback)query.list().get(0);
		/**
		 * create object, prepare data
		 */
		FeedbackComment feedbackComment = new FeedbackComment();
		feedbackComment.setComment(FEEDBACK_COMMENT);
		feedbackComment.setDate(new Date());
		feedbackComment.setFeedback_id(feedback.getFdId());
		feedbackComment.setUser_id(feedback.getUser_id());
		/**
		 * 	transaction data to database
		 */
	
		this.getSession().save(feedbackComment);
		/**
		 * Get data from database
		 */
		query = this.getSession().createQuery("FROM " + FeedbackComment.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FEEDBACK_COMMENT);
		if (query.list().isEmpty()) {
			assertNotNull("challenge list is empty", null);
			return;
		}
		feedbackComment = (FeedbackComment)query.list().get(0);
		
		/**
		 * Test whether data were consistent
		 */
		assertEquals(FEEDBACK_COMMENT, feedbackComment.getComment());
		assertEquals(feedback.getFdId(), feedbackComment.getFeedback_id());
		assertEquals(feedback.getUser_id(), feedbackComment.getUser_id());
		/**
		 * Delete test row
		 */
		query = this.getSession().createQuery("DELETE FROM " + FeedbackComment.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FEEDBACK_COMMENT);
		query.executeUpdate();
		
		query = this.getSession().createQuery("DELETE FROM " + Feedback.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment",CHG_TITLE);
		query.executeUpdate();
		
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		/**
		 * Check whether test row exist in database
		 */
		query = this.getSession().createQuery("FROM " + FeedbackComment.class.getName() + " WHERE comment = :comment");
		query.setParameter("comment", FEEDBACK_COMMENT);
		
		assertTrue(query.list().isEmpty());
		
	}
}
