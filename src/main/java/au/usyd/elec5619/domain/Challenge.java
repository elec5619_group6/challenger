package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="challenger.challenge")
public class Challenge implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="challenge_id")
	private long chaId;
	
	@Column(name="title")
    private String title;
	
	@Column(name="description")
    private String description;
	
	@Column(name="creator")
    private String creator;
	
	@Column(name="duration")
    private int duration;
	
	@Column(name="post_time")
    private Date postTime;
	
	@Column(name="rating")
    private double rating;
	
	@Column(name="difficulty")
    private double difficulty;
	
	@Column(name="picture_id")
    private int pictureId;
	

	@Column(name="video_link")
    private String icon;
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Challenge() {
		super();
	}
	
	public Challenge(String title, String description, String creator,
			int duration, Date postTime) {
		super();
		this.title = title;
		this.description = description;
		this.creator = creator;
		this.duration = duration;
		this.postTime = postTime;
	}
	
	public Challenge(String title, String description, String creator,
			int duration)
	{
		this.title = title;
		this.description = description;
		this.creator = creator;
		this.duration = duration;
	}
	
	public String getCreator() {
		return creator;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}


	public long getChaId() {
		return chaId;
	}


	public void setChaId(long chaId) {
		this.chaId = chaId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public Date getPostTime() {
		return postTime;
	}


	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}


	public double getRating() {
		return rating;
	}


	public void setRating(double rating) {
		this.rating = rating;
	}


	public double getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(double difficulty) {
		this.difficulty = difficulty;
	}


	public int getPictureId() {
		return pictureId;
	}


	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}

}
