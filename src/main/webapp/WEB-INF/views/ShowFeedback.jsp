<%@ include file="/WEB-INF/views/head.jsp" %>
<!-- jQuery files -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery/jRating.jquery.js"></script>
<html>
<head><title>User reviews</title></head>
<body>
<h1>User reviews</h1>

<c:forEach items="${feedbacks}" var="feedback">
    	<div class="container">
    	<div class="panel panel-primary">
    	<div class="panel-heading">
    	<h4>Title:&nbsp<c:out value="${feedback.challengeName}"/></h4>
    	<h4>User:&nbsp<c:out value="${feedback.userName}"/></h4>
    	<p>Interest: ${feedback.interestScore} &nbsp&nbsp Difficulty:${feedback.difficultyScore}</p>
    	</div>
    	<div class="panel-body">
    	<c:out value="${feedback.review}"/><br><br>

    	<c:if test="${feedback.videoLink!=''}">
    	<img src="${feedback.videoLink}" width="128" height="128">
    	</c:if>
    	</div>
    	
    	<div class="panel-footer">	
    			<c:forEach items="${comment}" var="comm">
    			<style type="text/css">
   					 .x-fieldset{border:2px solid #B5B8C8;padding:10px;margin-bottom:10px;display:block;background :#FFEC8B;
 	margin:auto;
	}
  				</style>   			 				
    				<c:if test="${feedback.feedbackId==comm.feedback_id}">
    				<fieldset class="x-fieldset">
    				<legend>${comm.date} by ${comm.userName} </legend>
					<label><c:out value="${comm.comment}"/></label>			
					</fieldset>
					</c:if>				
				</c:forEach>
				<form action="showfeedback/${feedback.feedbackId}&${User.userId}" method="post">					
				Comment content: <input type="text" name="comment"/>
				<input type="submit" value="Comment"/>
				</form>				
		</div>
    	</div>
    	</div>
    	<br>
    	<br>
    </c:forEach>
<%--  <c:forEach items="${model.feedbacks}" var="prod"> --%>
<!--     	<div class="container"> -->
    
<!-- 		<div class="panel panel-primary"> -->
<!-- 			<div class="panel-heading"> -->
<%-- 				<h4>User:&nbsp<c:out value="${User.Email}"/></h4> --%>
<!-- 			</div> -->
<!-- 			<div class="panel-body"> -->
<%--     	<i><c:out value="${prod.description}"/></i> --%>
<!-- 			</div> -->
			
<!-- 		</div> -->
<!--     </div>   	 -->
<!--     	<br> -->
<%--     </c:forEach> --%>
</body>
</html>