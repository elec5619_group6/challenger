package au.usyd.elec5619.domain;

import org.hibernate.Query;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;

public class ChallengeAttemptTest extends BaseConfiguration {

	@Test
	public void persistChallengeAttempt() {
		
		User user = new User(EMAIL, PASSWORD, USERNAME);
		this.getSession().save(user);
		Query query = this.getSession().createQuery("FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		user = (User) query.list().get(0);
		
		Challenge challenge = new Challenge(CHG_TITLE, CHG_DESCRIPTION, USERNAME, CHG_DURATION, CHG_POSTTIME);
		this.getSession().save(challenge);
		query = this.getSession().createQuery("FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		challenge = (Challenge) query.list().get(0);
		
		UserChallenge userChg = new UserChallenge();
		userChg.setUserId(user.getUserId());
		userChg.setChallengeId(challenge.getChaId());
		userChg.setTimeStart(TIME);
		userChg.setStatus(0);
		this.getSession().save(userChg);
		query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		if (query.list().isEmpty()) {
			assertNotNull("achievement attempt list is empty", null);
			return;
		}
		userChg = (UserChallenge) query.list().get(0);
		assertEquals(user.getUserId(), userChg.getUserId());
		assertEquals(challenge.getChaId(), userChg.getChallengeId());
		assertEquals(TIME, userChg.getTimeStart());
		assertEquals(0, userChg.isStatus());
		
		query = this.getSession().createQuery("DELETE FROM " + UserChallenge.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		query.executeUpdate();
		
		query = this.getSession().createQuery("FROM " + UserChallenge.class.getName() + " WHERE user_id = :user_id");
		query.setParameter("user_id", user.getUserId());
		
		assertTrue(query.list().isEmpty());
		
		query = this.getSession().createQuery("DELETE FROM " + User.class.getName() + " WHERE email = :email");
		query.setParameter("email", EMAIL);
		query.executeUpdate();
		query = this.getSession().createQuery("DELETE FROM " + Challenge.class.getName() + " WHERE title = :title");
		query.setParameter("title", CHG_TITLE);
		query.executeUpdate();
	}
}
