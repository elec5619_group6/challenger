package au.usyd.elec5619.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
//

@Entity
@Table(name="challenger.relationship")
public class Relationship implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="relationship_id")
	private long relationshipId;
	
	@Column(name="user_id")
	private long userId;
	
	@Column(name="friend_id")
	private long friendId;
	
	
	public Relationship()
	{
	}
	
	public Relationship(long userId,long friendId)
	{
		this.userId = userId;
		this.friendId = friendId;
	}


	public long getRelationshipId() {
		return relationshipId;
	}


	public void setRelationshipId(long relationshipId) {
		this.relationshipId = relationshipId;
	}


	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getFriendId() {
		return friendId;
	}


	public void setFriendId(long friendId) {
		this.friendId = friendId;
	}
	
	
	
}
