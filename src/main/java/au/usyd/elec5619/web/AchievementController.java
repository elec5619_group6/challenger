package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.AchievementAttemptManager;
import au.usyd.elec5619.service.AchievementManager;
import au.usyd.elec5619.service.ProfileManager;

/**
 * Achievement relative pages
 * @author John
 *
 */
@Controller
public class AchievementController {
	
	private static final Logger logger = LoggerFactory.getLogger(AchievementController.class);

	@Resource(name="achAmptManager")
	private AchievementAttemptManager achAmptManager;
	
	@Resource(name="achManager")
	private AchievementManager achManager;
	
	@Resource(name="profileManager")
	ProfileManager profileManager;
	
	@RequestMapping(value="/achievements", method=RequestMethod.GET)
	public String getAchievementList(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		logger.info("Accessing all achievements for the user " + user.getUserId());
		profileManager.createUserAchievementList(user);
		achAmptManager.getAchievementList(user.getUserId());
		
		model.addAttribute("attemptList", achAmptManager.getAttemptList());
		model.addAttribute("achList", achAmptManager.getAchList());
		model.addAttribute("display", achAmptManager.getAchDisplay());
		
		logger.info("Get attempt list complete");
		return "achievement";
	}

	@RequestMapping(value="/statistics", method=RequestMethod.GET)
	public String getUserStatistics(Model model, HttpSession session) {
		User user = (User) session.getAttribute("user");
		if (null == user)
			return "redirect:/";
		
		logger.info("Accessing user " + user.getUserId() + " statistic table");
		achAmptManager.getUserStatistics(user.getUserId());
		
		model.addAttribute("statistics", achAmptManager.getUserStat());
		return "statistics";
	}
}
