package au.usyd.elec5619.service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import au.usyd.elec5619.domain.Achievement;
import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.Feedback;
import au.usyd.elec5619.domain.UserChallenge;

//public interface FeedbackManager extends Serializable
//{
//	public List<Feedback> getFeedbacks();
//
//}

@Repository
public class FeedbackManager {

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<Feedback> feedbacklist;
	
	@Transactional
	public List<Feedback> findAll() {
		Session session = sessionFactory.getCurrentSession();
	    List<Feedback> feedbackList = session.createQuery("FROM Feedback").list();
	    return feedbackList;
	}
	@Transactional
	public List<Feedback> getRelatedFeedback (long challengeId) {
		
		Session currentSession = sessionFactory.getCurrentSession();
		Query query = currentSession.createQuery("FROM Feedback WHERE challenge_id = :Id");
		query.setParameter("Id", challengeId);		
		ArrayList<Feedback> myList = (ArrayList<Feedback>)query.list();
		return myList;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
//	@Transactional
//	public Feedback getRelatedFeedback(long id){
//				
//			Session currentSession = sessionFactory.getCurrentSession();
//			Query query = currentSession.createQuery("FROM Feedback where Challenge_id=:id");
//			query.setLong("id", id);
//			
//			Feedback feedback = (Feedback) query.uniqueResult();
//		    return feedback;		
//
//	}
//	@Transactional
//	public ArrayList<String> getChallengeName(long id)
//	{
//		Session currentSession = sessionFactory.getCurrentSession();
//		ArrayList<String> challengeName = new ArrayList<String>();
//		Query query = currentSession.createQuery("FROM Challenge where challenge_id =:id");
//		query.setLong("id", id);
//		ArrayList<UserChallenge> myList = (ArrayList<UserChallenge>)query.unit
//		for(UserChallenge u : myList)
//		{
//			myListId.add(u.getChallengeId());
//		}
//		return myListId;
//	@Transactional
//	public FeedbackForDisplay findFeedbackContent(long id,long userId,long userChallengeId,int numOfLike,boolean liked,boolean attemped) 
//	{
//		Session currentSession = sessionFactory.getCurrentSession();
//		//System.out.println("------------------"+id);
//		Query query = currentSession.createQuery("FROM Challenge where challenge_id=:id");
//		query.setLong("id", id);
//		
//		Feedback feedback = (Feedback) query.uniqueResult();
//	    return new FeedbackForDisplay(feedback,userId,userChallengeId,numOfLike,liked,attemped);
//	    //FeedbackForDisplay(Feedback feedback,String cn,String un,String r,String c,int i,int d)
//		
//	}
	@Transactional
	public void addFeedback(Feedback feedback) {
		this.sessionFactory.getCurrentSession().save(feedback);
	}

	public Feedback getFeedbackById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Feedback feedback = (Feedback) currentSession.get(Feedback.class,id);
		return feedback;
	}
	
	public void updateFeedback(Feedback feedback) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(feedback);
	}

	public void deleteFeedback(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Feedback feedback = (Feedback) currentSession.get(Feedback.class,id);
		currentSession.delete(feedback);
	}
}
