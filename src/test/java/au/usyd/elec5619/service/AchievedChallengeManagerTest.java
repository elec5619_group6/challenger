package au.usyd.elec5619.service;

import java.util.ArrayList;

import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;

import au.usyd.elec5619.base.BaseConfiguration;
import au.usyd.elec5619.domain.UserChallenge;
import au.usyd.elec5619.domain.Like;

public class AchievedChallengeManagerTest extends BaseConfiguration {
	
	private UserChallenge userChallenge;
	private AchievedChallengeManager achievedChallengeManager;
	private ArrayList<UserChallenge> achievedList;
	private int USER_CHALLENGE_COUNT = 1;
	private int USER_CHALLENGE_LIKE_COUNT = 1;
	private Like like1;
	private static long USER_ID_FOR_ACHIEVED = 999;
	private static long USER_CHALLENGE_ID_FOR_ACHIEVED = 1000;
	private int origSize;
	private int origListSize;
	
	@Before
	public void setUp()
	{
		achievedChallengeManager = new AchievedChallengeManager();
		achievedChallengeManager.setSessionFactory(this.getSessionFactory());
		userChallenge = new UserChallenge(USER_ID_FOR_ACHIEVED,1);
		achievedList = new ArrayList<UserChallenge>();
		like1 = new Like();
		like1.setUserChallengeId(USER_CHALLENGE_ID_FOR_ACHIEVED);
		like1.setUserId(USER_ID_FOR_ACHIEVED);
		
		Query query = this.getSession().createQuery("FROM " + Like.class.getName());
		origListSize = query.list().size();
		 
	}
	
	@Test
	public void testManager()
	{
		this.getSession().save(userChallenge);
		achievedList = achievedChallengeManager.findAll(USER_ID_FOR_ACHIEVED);
		assertEquals(USER_CHALLENGE_COUNT,achievedList.size());
		Query query = this.getSession().createQuery("DELETE FROM " + UserChallenge.class.getName() + " WHERE userchallengeId = :userChallengeId");
		query.setParameter("userChallengeId", achievedList.get(0).getUserChallengeId());
		query.executeUpdate();
	
		
		int likeListSize = achievedChallengeManager.getLikeList().size();
		assertEquals(origListSize,likeListSize);
		
//		this.getSession().save(like1);
//		
//		query = this.getSession().createQuery("FROM " + Like.class.getName() + " WHERE userchallenge_id = :userchallenge_id AND user_id =:user_id");
//		query.setParameter("userchallenge_id", USER_CHALLENGE_ID_FOR_ACHIEVED);
//		query.setParameter("user_id", USER_ID_FOR_ACHIEVED);
//		origSize = query.list().size();
//		System.out.println(like1.getUserChallengeId()+"-"+like1.getUserId());
//		achievedChallengeManager.increaseLike(new Like(3,2));
//		query = this.getSession().createQuery("FROM " + Like.class.getName() + " WHERE userchallenge_id = :userchallenge_id AND user_id =:user_id");
//		query.setParameter("userchallenge_id", USER_CHALLENGE_ID_FOR_ACHIEVED);
//		query.setParameter("user_id", USER_ID_FOR_ACHIEVED);
//		int newSize = query.list().size();
//		assertEquals(origSize+1,newSize);
//		
//		achievedChallengeManager.disLike(USER_CHALLENGE_ID_FOR_ACHIEVED, USER_ID_FOR_ACHIEVED);
//		assertEquals(origListSize,achievedList.size());
	}
	
}
