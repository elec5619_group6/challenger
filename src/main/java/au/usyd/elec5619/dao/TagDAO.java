package au.usyd.elec5619.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import au.usyd.elec5619.domain.Tag;
import au.usyd.elec5619.domain.User;

public interface TagDAO {
	
	public ArrayList<Tag> getTagsByUserId(long userId);
	
	public Tag addTag(String tagName);

	public void persistUser(User user);
	
	public User findUserById(String id);

	void addTagForUser(String tagName, long userId);

	Tag getTagById(long tagId);

	void addTagForChallenge(String tagName, long challengeId);

	public void removeAllTagsForUser(long userId);
}
