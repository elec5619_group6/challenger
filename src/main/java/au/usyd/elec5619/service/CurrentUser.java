package au.usyd.elec5619.service;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.AchievementDisplay;
import au.usyd.elec5619.domain.Challenge;
import au.usyd.elec5619.domain.ChallengeDisplay;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.domain.UserChallenge;

@Repository
public class CurrentUser {
	
	private static final Logger logger = LoggerFactory.getLogger(CurrentUser.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	private ArrayList<ChallengeDisplay> challengeList;
	
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public User getUserById(long userId)
	{
		Session currentSession = sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class,userId);
		return user;
	}
	
	@Transactional
	public void takeChallenge(long challengeId,long userId)
	{
		UserChallenge attemp = new UserChallenge();
		attemp.setChallengeId(challengeId);
		attemp.setUserId(userId);
		attemp.setTimeStart(System.currentTimeMillis());
		this.sessionFactory.getCurrentSession().save(attemp);
	}
	
	@Transactional
	public ArrayList<Long> getMyChallenge(long id)
	{
		Session currentSession = sessionFactory.getCurrentSession();
		ArrayList<Long> myListId = new ArrayList<Long>();
		Query query = currentSession.createQuery("FROM UserChallenge where user_id =:id");
		query.setLong("id", id);
		ArrayList<UserChallenge> myList = (ArrayList<UserChallenge>)query.list();
		for(UserChallenge u : myList)
		{
			myListId.add(u.getChallengeId());
		}
		return myListId;
	}
	
	/* add by John */
	@Transactional
	public ArrayList<ChallengeDisplay> getUserChallenges(long userId) {
		Session currentSession = sessionFactory.getCurrentSession();
		Connection conn = currentSession.connection();
		PreparedStatement stmt;
		try {
			stmt = conn.prepareStatement("SELECT * "
					+ "  FROM challenger.challenge NATURAL JOIN challenger.userchallenge WHERE user_id = ?");
			stmt.setLong(1, userId);
			ResultSet resultSet = stmt.executeQuery();
			challengeList.clear();
			
			while (resultSet.next()) {
				ChallengeDisplay challenge = new ChallengeDisplay();
				challenge.setChaId(resultSet.getLong("challenge_id"));
				if (!challengeList.contains(challenge)) {
					challenge.setUserId(userId);
					challenge.setUserChallengeId(resultSet.getLong("userchallengeId"));
					challenge.setTitle(resultSet.getString("title"));
					challenge.setDescription(resultSet.getString("description"));
					challenge.setProgress(resultSet.getInt("progress"));
					challenge.setDuration(resultSet.getInt("duration"));
					challenge.setTimeStart(resultSet.getLong("time_start"));
					challenge.setTimeEnd(resultSet.getLong("time_end"));
					challenge.setStatus(resultSet.getInt("status"));
					challenge.setRating(resultSet.getInt("rating"));
					challenge.setDifficulty(resultSet.getInt("difficulty"));
					challenge.setPictureId(resultSet.getInt("picture_id"));
					challenge.setPostTime(resultSet.getDate("post_time"));
					challenge.setCreator(resultSet.getString("creator"));
					
					logger.info(resultSet.getString("challenge_id")
							+ " - " + resultSet.getString("title")
							+ " - "  + resultSet.getString("description")
							+ " - "+ resultSet.getDate("post_time")
							+ " - "   + resultSet.getInt("status"));

					challengeList.add(challenge);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return challengeList;
	}
	
	@Transactional
	public void updateUserChallenge(UserChallenge userChallenge) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query query;
		if (1 == userChallenge.isStatus()) {
			query = currentSession.createQuery("UPDATE " + UserChallenge.class.getName() 
					+ " SET time_end =:time_end, progress =:progress, status =:status"
					+ " WHERE userchallengeId =:userchallengeId");
			
			query.setParameter("time_end", userChallenge.getTimeEnd());
			query.setParameter("progress", userChallenge.getProgress());
			query.setParameter("status", userChallenge.isStatus());
			query.setParameter("userchallengeId", userChallenge.getUserChallengeId());
		} else {
			query = currentSession.createQuery("UPDATE " + UserChallenge.class.getName() 
					+ " SET progress =:progress WHERE userchallengeId =:userchallengeId");
			
			query.setParameter("progress", userChallenge.getProgress());
			query.setParameter("userchallengeId", userChallenge.getUserChallengeId());
		}
		query.executeUpdate();
	}
	
	public CurrentUser() {
		super();
		challengeList = new ArrayList<ChallengeDisplay>();
	}

	public ArrayList<ChallengeDisplay> getChallengeList() {
		return challengeList;
	}
	
	
}
